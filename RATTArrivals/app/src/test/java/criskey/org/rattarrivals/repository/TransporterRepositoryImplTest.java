package criskey.org.rattarrivals.repository;

import android.content.Context;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import criskey.org.rattarrivals.ArrivalsApplication;
import criskey.org.rattarrivals.BuildConfig;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.model.TransporterType;

/**
 * Created by criskey on 4/1/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 16, application = ArrivalsApplication.class)
public class TransporterRepositoryImplTest extends TestCase {

    @Test
    public void testSavingData() {
        Context context = RuntimeEnvironment.application.getApplicationContext();

        PreferencesRepository preferencesRepository = new PreferencesRepositoryImpl(context);
        TransporterRepository transporterRepository = new TransporterRepositoryImpl(context);

        assertTrue("Database is not initialized", !preferencesRepository.hasData());

        List<Transporter> transporterList = new ArrayList<>();
        Transporter _40 = new Transporter(TransporterType.BUS, "param40", 40, true);
        transporterList.add(_40);
        Transporter _33 = new Transporter(TransporterType.BUS, "param33", 33, false);
        transporterList.add(_33);
        Transporter _46 = new Transporter(TransporterType.BUS, "param46", 46, false);
        transporterList.add(_46);

        Transporter _1 = new Transporter(TransporterType.TRAM, "param1", 1, true);
        transporterList.add(_1);
        Transporter _2 = new Transporter(TransporterType.TRAM, "param2", 2, false);
        transporterList.add(_2);
        transporterList.add(new Transporter(TransporterType.TRAM, "param4", 4, false));
        transporterList.add(new Transporter(TransporterType.TRAM, "param5", 5, true));

        transporterList.add(new Transporter(TransporterType.TROLLEY, "param14", 14, true));
        transporterList.add(new Transporter(TransporterType.TROLLEY, "param11", 11, false));
        transporterList.add(new Transporter(TransporterType.TROLLEY, "param13", 13, true));

        transporterRepository.createMany(transporterList);
        preferencesRepository.validateData(true);

        assertTrue("Database is initialized", preferencesRepository.hasData());

        List<Transporter> loadedTransporterList = transporterRepository.findAll();

        assertNotNull(loadedTransporterList);
        assertFalse(loadedTransporterList.isEmpty());
        assertEquals(transporterList.size(), loadedTransporterList.size());

        loadedTransporterList = transporterRepository.getTransportersByType(TransporterType.BUS);
        assertEquals(3, loadedTransporterList.size());

        loadedTransporterList = transporterRepository.getTransportersByType(TransporterType.TRAM);
        assertEquals(4, loadedTransporterList.size());

        loadedTransporterList = transporterRepository.getTransportersByFavorite();
        assertEquals(5, loadedTransporterList.size());

        transporterRepository.changeFavorite(_40);
        loadedTransporterList = transporterRepository.getTransportersByFavorite();
        assertEquals(4, loadedTransporterList.size());


        // testing history;
        transporterRepository.addToHistory(_33);
        transporterRepository.addToHistory(_40);
        transporterRepository.addToHistory(_1);
        assertEquals(3, transporterRepository.getTransporterHistory().size());
        assertEquals(_1, transporterRepository.getTransporterHistory().get(0));

        transporterRepository.commitHistory();

        transporterRepository = new TransporterRepositoryImpl(context);
        assertEquals(3, transporterRepository.getTransporterHistory().size());
        assertEquals(_1, transporterRepository.getTransporterHistory().get(0));
        transporterRepository.addToHistory(_46);
        assertEquals(_46, transporterRepository.getTransporterHistory().get(0));
        transporterRepository.addToHistory(_1);
        assertEquals(_1, transporterRepository.getTransporterHistory().get(0));

        criskey.org.rattarrivals.util.Logger.log(this,""+transporterRepository.getTransporterHistory());

        assertEquals(4, transporterRepository.getTransporterHistory().size());

        transporterRepository.commitHistory();
        transporterRepository = new TransporterRepositoryImpl(context);

        assertEquals(_1, transporterRepository.getTransporterHistory().get(0));

        transporterRepository.deleteAll();
        preferencesRepository.validateData(false);
        assertFalse("Database has no data", preferencesRepository.hasData());

    }

}