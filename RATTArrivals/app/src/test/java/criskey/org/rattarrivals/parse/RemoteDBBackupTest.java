package criskey.org.rattarrivals.parse;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import criskey.org.rattarrivals.BuildConfig;
import criskey.org.rattarrivals.TestUtils;
import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.util.Logger;

import static org.junit.Assert.*;

/**
 * Created by criskey on 20/6/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 16, application = TestUtils.TestApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RemoteDBBackupTest {


    private static List<Station> stations;
    private static RemoteDBBackup rmtDb;

    private static Station createdStation;

    @BeforeClass
    public static void initTests() {
        stations = new ArrayList<>();
        rmtDb = new RemoteDBBackup();
        createdStation = createStation(null);
    }

    @NonNull
    public static Station createStation(Location location) {
        Station station = new Station();
        station.setId(UUID.randomUUID().toString());
        station.setName(UUID.randomUUID().toString());
        Location loc = (location == null) ? new Location(1.224, 2.333) : location;
        station.setLocation(loc);
        return station;
    }

    @Test
    public void a_addStation() {

        rmtDb.execute(new RemoteDBBackup.Query() {
            @Override
            public void exec() throws IOException, JSONException {
                assertTrue("Db be empty", rmtDb.isDatabaseEmpty());
                rmtDb.upsertLocation(createdStation);
                assertFalse("Db should not be empty", rmtDb.isDatabaseEmpty());
                JSONObject data = rmtDb.getLocation(createdStation.getId());
                assertEquals("Station id should match", createdStation.getId(), data.get("station_id"));
            }

            @Override
            public void onError(Exception e) {
                fail(e.getMessage());
            }

        });

    }

    @Test
    public void b_updateLocation() {
        createdStation.setLocation(new Location(666.1, 7777.1));
        rmtDb.execute(new RemoteDBBackup.Query() {
            @Override
            public void exec() throws IOException, JSONException {
                rmtDb.upsertLocation(createdStation);
                JSONObject data = rmtDb.getLocation(createdStation.getId());
                Location loc = createdStation.getLocation();
                assertEquals("Station id should match", createdStation.getId(), data.get("station_id"));
                assertEquals("Station latitude should match", loc.getLatitude(), data.get("latitude"));
                assertEquals("Station longitude should match", loc.getLongitude(), data.get("longitude"));
            }

            @Override
            public void onError(Exception e) {
                fail(e.getMessage());
            }
        });
    }

    @Test
    public void c_addBulkStations() {
        for (int i = 0; i < 10; i++) {
            stations.add(createStation(null));
        }
        rmtDb.execute(new RemoteDBBackup.Query() {
            @Override
            public void exec() throws IOException, JSONException {
                rmtDb.insertBulkLocations(stations);
                Station randomStation = stations.get(3);
                JSONObject data = rmtDb.getLocation(randomStation.getId());
                assertEquals("Station id should match", randomStation.getId(), data.get("station_id"));
            }

            @Override
            public void onError(Exception e) {
                fail(e.getMessage());
            }
        });
    }

    @Test(expected = JSONException.class)
    public void d_getBadLocation() throws IOException, JSONException {
        try {
            rmtDb.getLocation("-1").toString();
        } finally {
            try {
                rmtDb.close();
            } catch (IOException e) {
                e.printStackTrace();
                fail();
            }
        }
    }

//    public void execute(Query query) {
//        try {
//            query.exec();
//            assertTrue(true);
//        } catch (Exception e) {
//            e.printStackTrace();
//            fail(e.getMessage());
//        } finally {
//            try {
//                rmtDb.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//                fail();
//            }
//        }
//    }
//
//
//    public interface Query {
//        public void exec() throws IOException, JSONException;
//    }

}