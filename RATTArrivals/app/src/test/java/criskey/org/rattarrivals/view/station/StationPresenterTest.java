package criskey.org.rattarrivals.view.station;

import junit.framework.TestCase;

import org.junit.Test;
import org.mockito.Mockito;

import rx.Observable;
import rx.functions.Action1;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by criskey on 15/2/2016.
 */
public class StationPresenterTest extends TestCase {

    @Test
    public void testPresenter() {

        Observable<Integer> observable = null;
        for (int i = 0; i < 4; i++) {
            if (observable == null)
                observable = Observable.just(i);
            else
                observable = observable.concatWith(Observable.just(i));
        }


        observable
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        criskey.org.rattarrivals.util.Logger.log(this,""+integer);
                    }
                });

    }

}