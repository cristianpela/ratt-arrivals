package criskey.org.rattarrivals.parse;

import junit.framework.TestCase;

import org.junit.Test;

import criskey.org.rattarrivals.model.TransporterType;

import static org.junit.Assert.assertEquals;

/**
 * Created by criskey on 4/1/2016.
 */
public class TransporterTypeTest extends TestCase {

    @Test
    public void testType(){
        String alias = "BUS";
        TransporterType busType = TransporterType.valueOf(alias);
        assertEquals(TransporterType.BUS, busType);
    }

    @Test
    public void test_if_transporter_type_is_correct_created() {
        String rawUri = "http://86.122.170.105:61978/html/timpi/tram.php";
        TransporterType tram = TransporterType.TRAM;
        assertEquals(rawUri, tram.getUri().toString());
    }

}