package criskey.org.rattarrivals.view;

import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import criskey.org.rattarrivals.BuildConfig;
import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.ArrivalsApplication;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.model.TransporterType;

/**
 * Created by criskey on 8/1/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 16, application = ArrivalsApplication.class)
public class MainDisplayFragmentTest extends TestCase {

    @Test
    public void testIfFragmentIsCreated() throws Exception {
        Transporter transporter = new Transporter(TransporterType.BUS, "Linia 40", 40, true);
        MainDisplayFragment fragment = MainDisplayFragment.createFragment(transporter);
        SupportFragmentTestUtil.startFragment(fragment, AppCompatActivity.class);

        TextView textLine = (TextView) fragment.getView().findViewById(R.id.textSelectedLine);
        assertNotNull("Text View must not be null ", textLine);
        assertNotNull("Transporter should not be null", fragment.getPresenter().getTransporter());

        assertEquals("Transporters must be equal", transporter, fragment.getPresenter().getTransporter());

    }
}