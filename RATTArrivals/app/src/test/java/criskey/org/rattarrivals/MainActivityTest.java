package criskey.org.rattarrivals;

import android.content.pm.ActivityInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;
import org.robolectric.util.ActivityController;

import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.model.TransporterType;
import criskey.org.rattarrivals.repository.PreferencesRepository;
import criskey.org.rattarrivals.repository.Repository;
import criskey.org.rattarrivals.repository.RepositoryManager;
import criskey.org.rattarrivals.repository.TransporterRepository;
import criskey.org.rattarrivals.repository.TransporterRepositoryImpl;
import criskey.org.rattarrivals.view.DisplayFilter;
import criskey.org.rattarrivals.view.MainDisplayFragment;
import criskey.org.rattarrivals.view.MainListFragment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by criskey on 6/1/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 16, application = ArrivalsApplication.class)
public class MainActivityTest extends TestCase {

    public static final int PORTRAIT = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    public static final int LANDSCAPE = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;

    private ActivityController<MainActivity> activityController;
    private MainActivity mainActivity;
    private ArrivalsApplication application;

    @Before
    public void setup() {
        application = (ArrivalsApplication) RuntimeEnvironment.application;
        //hack the repository api: this will prevent sync with remote server
        getRepository().validateData(true);
        activityController = Robolectric.buildActivity(MainActivity.class).setup();
        mainActivity = activityController.get();
    }

    @After
    public void clean() {
        if (mainActivity != null) {
            mainActivity.onBackPressed();
            activityController.pause().stop().destroy();
        }
    }

    @Ignore
    @Test
    public void testMenu() {
        Toolbar toolbar = (Toolbar) mainActivity.findViewById(R.id.toolbar);
        assertNotNull(toolbar);
        getMenuItemAndTestIt(mainActivity, toolbar, R.id.action_menu_bus);
        getMenuItemAndTestIt(mainActivity, toolbar, R.id.action_menu_tram);
        getMenuItemAndTestIt(mainActivity, toolbar, R.id.action_menu_trolley);
        getMenuItemAndTestIt(mainActivity, toolbar, R.id.action_menu_favorite);
    }

    private void getMenuItemAndTestIt(MainActivity mainActivity, Toolbar toolbar, int id) {
        MenuItem menuItem = toolbar.getMenu().findItem(id);
        assertNotNull(menuItem);
        assertTrue(menuItem.isVisible());
        mainActivity.onOptionsItemSelected(menuItem);
    }

    @Ignore
    @Test
    public void testingApplicationPresenterCache() {
        //stop/destroy the activity [done by rotation, config changes]
        activityController.pause().stop().destroy();
        assertEquals(2, application.getPresenterManager().size());
        // criskey.org.rattarrivals.util.Logger.log(this,application.getPresenterManager());

        //recreate activity
        activityController = Robolectric.buildActivity(MainActivity.class).setup();
        mainActivity = activityController.get();

        //destroy the activity [done by pressing the back button]
        mainActivity.onBackPressed();
        activityController.pause().stop().destroy();
        assertEquals(0, application.getPresenterManager().size());

        // criskey.org.rattarrivals.util.Logger.log(this,application.getPresenterManager());
        mainActivity = null;// just a flag for @After method
    }

    @Ignore
    @Test
    public void testNumberOfFragmentsInPortraitMode(){
        setScreenOrientation(PORTRAIT);
       // assertNotNull(mainActivity.getMainListFragment());
       // assertNull(mainActivity.getMainDisplayFragment(null));
    }

    @Ignore
    @Test
    public void testNumberOfFragmentsInLandscapeMode(){
        setScreenOrientation(LANDSCAPE);
      //  assertNotNull("List Fragment must not be null", mainActivity.getMainListFragment());
       // assertNotNull("Display Fragment must not be null", mainActivity.getMainDisplayFragment(null));
    }


    //TODO take a look at
    // https://chromium.googlesource.com/external/robolectric/+/animation-realism/robolectric/src/test/java/org/robolectric/shadows/LayoutInflaterTest.java
    public void setScreenOrientation(int mode){
        application.getResources().getConfiguration().orientation = mode;
        //Shadows.shadowOf(application).getResources().getConfiguration().orientation = mode;
        //Shadows.shadowOf(mainActivity).setRequestedOrientation(mode);
    }

    @Ignore
    @Test
    public void testLastDisplayFilterWhenLaunching() throws Exception {
        getRepository().setLastDisplayFilter(DisplayFilter.ALL.toString());
        String all = application.getResources().getString(R.string.all);
        assertEquals("Subtitle must be all", all, mainActivity.getSupportActionBar().getSubtitle());
    }

    @Ignore
    @Test
    public void testFragmentIntegration() throws Exception {
        getRepository().setLastDisplayFilter(DisplayFilter.ALL.toString());

       /* MainListFragment mainListFragment = mainActivity.getMainListFragment();
        assertNotNull(mainListFragment);
        assertTrue("Main List Fragment must be visibile", mainListFragment.isVisible());

        Transporter line40 = new Transporter(TransporterType.BUS, "Linia 40", 40, true);
        mainActivity.onTransporterClickFromFragment(line40);

        FragmentManager supportFragmentManager = mainActivity.getSupportFragmentManager();
        assertEquals(1, supportFragmentManager.getBackStackEntryCount());
        assertEquals(MainActivity.MAIN_DISPLAY_BACK_ENTRY, supportFragmentManager.getBackStackEntryAt(0).getName());

        // pressing a menu filter while arrivals are displayed should pop the backstack
        Toolbar toolbar = (Toolbar) mainActivity.findViewById(R.id.toolbar);
        MenuItem menuItem = toolbar.getMenu().findItem(R.id.action_menu_bus);
        mainActivity.onOptionsItemSelected(menuItem);

        assertEquals("Backstack should be empty", 0, supportFragmentManager.getBackStackEntryCount());*/
    }

    @Ignore
    @Test
    @Config(qualifiers = "land")
    public void testFragmentIntegrationInLandscape() throws Exception {
        getRepository().setLastDisplayFilter(DisplayFilter.ALL.toString());

        assertTrue("Configuration must be in landscape", mainActivity.isInLandscape());

        Transporter line40 = new Transporter(TransporterType.BUS, "Linia 40", "", 40, true);
        mainActivity.onTransporterClickFromFragment(line40);

        FragmentManager supportFragmentManager = mainActivity.getSupportFragmentManager();
        assertEquals(0, supportFragmentManager.getBackStackEntryCount());
     /*   MainDisplayFragment mainDisplayFragment = mainActivity.getMainDisplayFragment(null);
        assertNotNull(mainDisplayFragment);
        TextView textLine =(TextView) mainDisplayFragment.getView().findViewById(R.id.textSelectedLine);
        assertNotNull(textLine);
        assertEquals("Line text must match", "Line 40", textLine.getText().toString());*/
    }

    protected PreferencesRepository getRepository() {
        return (PreferencesRepository)application.getRepository(RepositoryManager.PREFERENCES_REPOSITORY);
    }


    @Test
    public void testAttachDetachFragment(){
        Fragment fragment = new Fragment();
        SupportFragmentTestUtil.startFragment(fragment, AppCompatActivity.class);
    }

}