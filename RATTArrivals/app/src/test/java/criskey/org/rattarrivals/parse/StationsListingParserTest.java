package criskey.org.rattarrivals.parse;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.List;

import criskey.org.rattarrivals.TestUtils;
import criskey.org.rattarrivals.model.Station;
import rx.functions.Action1;

/**
 * Created by criskey on 12/2/2016.
 */
public class StationsListingParserTest {

    private static final String TRANSP_ID = "1552";
    private static final String FIRST_STATION_ID = "5283";

    @Test
    public void testingStationsParse() {
        StationsListingParser stationsListingParser = new StationsListingParser(new TestUtils.FileDataDownloader("stations.html"));
        List<Station> stationList = stationsListingParser.getStations();
        coreTest(stationList);

        Station firstStation = stationList.get(0);
        StationParser stationParser = new StationParser(new TestUtils.FileDataDownloader("single_station_5283.html"), firstStation);
        firstStation = stationParser.getStation();
        assertEquals(1, firstStation.getTransporters().size());
        assertEquals("Transporter id must match for this station", TRANSP_ID, firstStation.getTransporters().get(0).getId());

        ArrivalTimeParser arrivalTimeParser = new ArrivalTimeParser(
                new TestUtils.FileDataDownloader("single_station_arrival_1552_5283.html"),
                firstStation,
                firstStation.getTransporters().get(0)
        );
        firstStation.getArrivals().add(arrivalTimeParser.getArrivalTime(null));
        assertEquals("15:08", firstStation.getArrivals().get(0).getTime());

    }

    @Test
    @Ignore
    public void testingRxStationParse() {
        StationsListingParser stationsListingParser = new StationsListingParser(new TestUtils.FileDataDownloader("stations.html"));
        ObservableNetworkDataDownloader.getObservableStationData(stationsListingParser)
                .subscribe(new Action1<List<Station>>() {
                    @Override
                    public void call(List<Station> stations) {
                        coreTest(stations);
                    }
                });
    }

    @Test
    @Ignore
    public void testingRxNetworkStationParse() {
        StationsListingParser stationsListingParser = new StationsListingParser();
        ObservableNetworkDataDownloader.getObservableStationData(stationsListingParser)
                .subscribe(new Action1<List<Station>>() {
                    @Override
                    public void call(List<Station> stations) {
                        coreTest(stations);
                    }
                });
    }

    private void coreTest(List<Station> stationList) {
        assertTrue(!stationList.isEmpty());
        Station firstStation = stationList.get(0);
        assertEquals("Station id doesn't match", FIRST_STATION_ID, firstStation.getId());
        assertEquals("Station name doesn't match", "Mures", firstStation.getName());
        Station lastStation = stationList.get(stationList.size() - 1);
        assertEquals("Station id doesn't match", "6861", lastStation.getId());
        assertEquals("Station name doesn't match", "str. Corbului", lastStation.getName());
    }
}