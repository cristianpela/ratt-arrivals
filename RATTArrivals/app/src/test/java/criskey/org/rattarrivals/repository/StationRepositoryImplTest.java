package criskey.org.rattarrivals.repository;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import criskey.org.rattarrivals.ArrivalsApplication;
import criskey.org.rattarrivals.BuildConfig;
import criskey.org.rattarrivals.TestUtils;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.model.TransporterType;
import criskey.org.rattarrivals.parse.StationsListingParser;

/**
 * Created by criskey on 12/2/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 16, application = ArrivalsApplication.class)
public class StationRepositoryImplTest extends TestCase {


    /**Use case scenario
     * 1. User clicks on station
     * 2. App query after the name of clicked station. It could return more than one station. For
     * each (if is the case) follow the next steps:
     * 3. App check if station is joined
     * 3.1 If not, a network call to get transporters that arrive in that station [StationParser invoked]
     * 3.2 Create a join with each transporter(id) received over network
     * 4. If so, query all the joins with that station. Each join contain a transporter. which
     * will be added to station's transporters Station#getTransporters().add(...)
     * 5. For each station transporter, call network for arrival time [ArrivalTimeParser invoked]
     */
    @Test
    public void fullTestRepository(){
        StationRepository stationRepository = new StationRepositoryImpl(RuntimeEnvironment.application);

        StationsListingParser stationsListingParser = new StationsListingParser(new TestUtils.FileDataDownloader("stations.html"));
        List<Station> netStations = stationsListingParser.getStations(); // stations from "network"
        stationRepository.createMany(netStations);

        List<Station> dbStations = stationRepository.findAll();// stations from db;
        assertEquals(netStations.size(), dbStations.size());


        //insert some transporters
        TransporterRepository transporterRepository = new TransporterSQLRepository(RuntimeEnvironment.application);
        List<Transporter> transporters = transporters();
        transporterRepository.createMany(transporters);

        Transporter _40 = transporters.get(0);
        Transporter _33 = transporters.get(1);

        Station firstStation = dbStations.get(0);
        assertFalse("Should be not joined", stationRepository.isJoined(firstStation));
        //pretend that step 3.1 from use case was done;

        //create a join (3.2)
        stationRepository.createJoinTransporterStation(firstStation, _40);
        stationRepository.createJoinTransporterStation(firstStation, _33);


        firstStation = stationRepository.findJoinedStation(firstStation);
        assertEquals(2, firstStation.getTransporters().size());


        //test LIKE
        stationRepository.updateAlias(firstStation, "My station");
        firstStation = stationRepository.findOne(firstStation.getId());
        assertEquals("My station", firstStation.getAlias());

        //like by alias
        List<Station> likeStations = stationRepository.findAllStationsLike("My");
        assertEquals(1, likeStations.size());
        assertEquals(firstStation, likeStations.get(0));

        //like by name
        likeStations = stationRepository.findAllStationsLike(firstStation.getName());
        assertEquals(1, likeStations.size());
        assertEquals(firstStation, likeStations.get(0));


        stationRepository.updateAlias(dbStations.get(1), "Aliazzz");
        stationRepository.updateAlias(dbStations.get(2), "Aliazzz");
        stationRepository.updateAlias(dbStations.get(3), "Aliazzz");


        List<String> aliases = stationRepository.findAllAliases();
        assertEquals(2, aliases.size());
        assertTrue(aliases.contains("Aliazzz"));
        assertTrue(aliases.contains("My station"));

        //delete alias
        stationRepository.updateAlias(firstStation, null);
        firstStation = stationRepository.findOne(firstStation.getId());
        assertNull(firstStation.getAlias());
    }


    private List<Transporter> transporters(){
        List<Transporter> transporterList = new ArrayList<>();
        Transporter _40 = new Transporter(TransporterType.BUS, "param40", 40, true);
        transporterList.add(_40);
        Transporter _33 = new Transporter(TransporterType.BUS, "param33", 33, false);
        transporterList.add(_33);
        Transporter _46 = new Transporter(TransporterType.BUS, "param46", 46, false);
        transporterList.add(_46);

        Transporter _1 = new Transporter(TransporterType.TRAM, "param1", 1, true);
        transporterList.add(_1);
        Transporter _2 = new Transporter(TransporterType.TRAM, "param2", 2, false);
        transporterList.add(_2);
        transporterList.add(new Transporter(TransporterType.TRAM, "param4", 4, false));
        transporterList.add(new Transporter(TransporterType.TRAM, "param5", 5, true));

        transporterList.add(new Transporter(TransporterType.TROLLEY, "param14", 14, true));
        transporterList.add(new Transporter(TransporterType.TROLLEY, "param11", 11, false));
        transporterList.add(new Transporter(TransporterType.TROLLEY, "param13", 13, true));
        return  transporterList;
    }
}