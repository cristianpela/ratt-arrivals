package criskey.org.rattarrivals.view;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import criskey.org.rattarrivals.ArrivalsApplication;
import criskey.org.rattarrivals.BuildConfig;
import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.model.TransporterType;
import criskey.org.rattarrivals.repository.PreferencesRepository;
import criskey.org.rattarrivals.repository.RepositoryManager;
import criskey.org.rattarrivals.repository.TransporterRepository;

/**
 * Created by criskey on 6/1/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 16, application = ArrivalsApplication.class)
public class MainListFragmentTest extends TestCase {


    private TransporterRepository transRepository;
    private PreferencesRepository prefsRepository;
    private ArrivalsApplication application;
    private MainListFragment mainListFragment;
    private ListView transportersListView;

    @Before
    public void setUp() {
        application = (ArrivalsApplication) RuntimeEnvironment.application;
        application.setUpDatabaseForTesting();
        transRepository = (TransporterRepository) application.getRepository(RepositoryManager.TRANSPORTER_REPOSITORY);
        prefsRepository = (PreferencesRepository) application.getRepository(RepositoryManager.PREFERENCES_REPOSITORY);
        application.setUpDatabaseForTesting();
        mainListFragment = new MainListFragment();
        SupportFragmentTestUtil.startFragment(mainListFragment, AppCompatActivity.class);
        transportersListView = (ListView) mainListFragment.getView().findViewById(R.id.listTransporters);
        Shadows.shadowOf(transportersListView).populateItems();
    }

    @After
    public void tearDown() {
        transRepository.deleteAll();
        application.getPresenterManager().removeCachedPresenters();
    }

    @Ignore
    @Test
    public void testIfDataIsDisplayedOnFragment() {

        int count = transportersListView.getAdapter().getCount();
        assertEquals(transRepository.findAll().size(), count);

        View firstEntryView = transportersListView.getChildAt(0);
        assertNotNull(firstEntryView);
        assertTrue(firstEntryView instanceof RelativeLayout);


        TextView textLine = (TextView) firstEntryView.findViewById(R.id.textLine);
        assertNotNull(textLine);
        ImageButton imageButtonFavorite = (ImageButton) firstEntryView.findViewById(R.id.imageButtonFavorite);
        assertNotNull(imageButtonFavorite);
    }

    @Test
    public void testSimulatingAddingOrRemovingAFavoriteByClick() throws Exception {
        View firstEntryView = transportersListView.getChildAt(0);
        ImageButton imageButtonFavorite = (ImageButton) firstEntryView.findViewById(R.id.imageButtonFavorite);
        //simulating a click for add or remove a favorite
        int dbFavoriteCount = transRepository.getTransportersByFavorite().size();
        imageButtonFavorite.performClick();
        int newDbFavoriteCount = transRepository.getTransportersByFavorite().size();
        assertTrue("Number of favorite must be with 1 lower or higher than before; comparing: (" +
                dbFavoriteCount + " " + newDbFavoriteCount + ")", Math.abs(newDbFavoriteCount - dbFavoriteCount) == 1.0);
    }

    @Ignore
    @Test
    public void testIfListIsProperlyChangedWhenAFilterIsApplied() throws Exception {
        assertTrue(true);
        testTheTransporterFilters(DisplayFilter.BUS, TransporterType.BUS);
        testTheTransporterFilters(DisplayFilter.TRAM, TransporterType.TRAM);
        testTheTransporterFilters(DisplayFilter.TROLLEY, TransporterType.TROLLEY);

        testTheFavoriteFilter();

    }

    private void testTheFavoriteFilter() {
        mainListFragment.setSelectedDisplayFilter(DisplayFilter.FAVORITE);
        int adapterCount = transportersListView.getAdapter().getCount();
        int dbCount = transRepository.getTransportersByFavorite().size();
        assertEquals(DisplayFilter.FAVORITE + " in db and adapter must match", dbCount, adapterCount);
    }

    private void testTheTransporterFilters(DisplayFilter displayFilter, TransporterType transporterType) {
        mainListFragment.setSelectedDisplayFilter(displayFilter);
        int adapterCount = transportersListView.getAdapter().getCount();
        int dbCount = transRepository.getTransportersByType(transporterType).size();
        assertEquals(TransporterType.BUS + " in db and adapter must match", dbCount, adapterCount);

        String displayFilterAsString = displayFilter.toString();
        assertEquals("Matching last display filter failed", displayFilterAsString, prefsRepository.getLastDisplayFilter());
    }
}
