package criskey.org.rattarrivals;

import android.app.Application;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.EnumMap;

import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.model.TransporterType;
import criskey.org.rattarrivals.parse.AbstractNetworkDataDownloader;
import criskey.org.rattarrivals.util.Logger;

/**
 * Created by criskey on 14/1/2016.
 */
public class TestUtils {

    public static class TestApplication extends Application {}
    public static File baseDir = new File("D:\\Programming\\Workspace\\Sources\\bitbucket\\rattarrivals\\RATTArrivals\\app\\src\\test\\java\\criskey\\org\\rattarrivals\\parse\\");


    public static EnumMap<TransporterType, String> transporterTypeMap = new EnumMap<TransporterType, String>(TransporterType.class);

    {
        TestUtils.transporterTypeMap.put(TransporterType.TRAM, "tram.html");
        TestUtils.transporterTypeMap.put(TransporterType.TROLLEY, "trol.html");
        TestUtils.transporterTypeMap.put(TransporterType.BUS, "bus.html");
    }

    public static File _40ArrivalsFile = new File(baseDir, "_40arrivals.html");

    public static Transporter line40 = new Transporter(TransporterType.BUS, "Linia 40", "", 886, true);


    public static class FileDataDownloader implements AbstractNetworkDataDownloader {

        private File file;

        public FileDataDownloader(File file) {
            this.file = file;
        }

        public FileDataDownloader(String fileName){
            file = new File(baseDir, fileName);
        }


        @Override
        public Document load() {
            try {
                return Jsoup.parse(file, "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static <E> void printCollection(Collection<E> collection){
        for(E element : collection){
            Logger.log(element.getClass(), element.toString());
        }
    }
}
