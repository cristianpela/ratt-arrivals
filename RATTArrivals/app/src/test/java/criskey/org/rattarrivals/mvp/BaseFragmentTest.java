package criskey.org.rattarrivals.mvp;

import android.os.Bundle;

import junit.framework.TestCase;

/**
 * Created by criskey on 4/1/2016.
 */
//@RunWith(RobolectricGradleTestRunner.class)
//@Config(constants = BuildConfig.class, sdk = 16, application = ArrivalsApplication.class)
public class BaseFragmentTest extends TestCase {

   // @Test
   // @Config(qualifiers = "en-port")
    public void testIfPresenterManagerWasCalled(){
       /* ArrivalsApplication app = (ArrivalsApplication) RuntimeEnvironment.application;
        app.getResources().getConfiguration().orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        MockFragment mockFragment = new MockFragment();
       // SupportFragmentTestUtil.startFragment(mockFragment, MainActivity.class);
        FragmentController<MockFragment> fragmentController = FragmentController.of(mockFragment).create().resume();
        fragmentController.pause().stop().destroy();
        //assertNotNull(mockFragment.getPresenterManager());
        criskey.org.rattarrivals.util.Logger.log(this,app.getPresenterManager());*/


    }

    public static interface MockView extends IView{

    }

    public static class MockPresenter extends BasePresenter<MockView>{
        @Override
        public void onBind() {

        }
    }

    public static class MockFragment extends BaseFragment<MockPresenter> implements MockView{

        @Override
        protected void onArgumentsAttached(Bundle args) {

        }

        @Override
        public void onPresenterAttached() {

        }
    }
}