package criskey.org.rattarrivals.parse;

import android.app.Application;

import com.google.android.gms.maps.model.LatLng;

import junit.framework.TestCase;

import org.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import criskey.org.rattarrivals.ArrivalsApplication;
import criskey.org.rattarrivals.BuildConfig;
import criskey.org.rattarrivals.TestUtils;
import criskey.org.rattarrivals.util.Logger;

import static criskey.org.rattarrivals.TestUtils.printCollection;
import static org.junit.Assert.*;

/**
 * Created by criskey on 24/3/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 16, application = TestUtils.TestApplication.class)
public class WalkingDirectionsParserTest {


   /* @Test
    public void testParserFromLocalJson() throws Exception {
        Logger.log("TESTING LOCAL JSON");
        WalkingDirectionsParser parser = new WalkingDirectionsParser(new TestUtils.FileDataDownloader("walkdirections.json"));
        List<String> directions = parser.getWalkingDirections().getDirections();
        assertEquals(2, directions.size());
        printCollection(directions);

        List<LatLng> pathNodes = parser.getWalkingDirections().getPathNodes();
        printCollection(pathNodes);
    }

    @Test
    public void testParserFromRemoteJson() throws Exception {
        Logger.log("TESTING REMOTE JSON");
        LatLng from = new LatLng(45.774201, 21.244120),
                to = new LatLng(45.766782, 21.244833); // gara de est
        String lang = "ro";
        WalkingDirectionsParser parser = new WalkingDirectionsParser(from, to, lang);
        List<String> directions = parser.getWalkingDirections().getDirections();
        printCollection(directions);

        List<LatLng> pathNodes = parser.getWalkingDirections().getPathNodes();
        printCollection(pathNodes);
    }*/
}