package criskey.org.rattarrivals.parse;

import android.net.Uri;

import junit.framework.TestCase;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import criskey.org.rattarrivals.ArrivalsApplication;
import criskey.org.rattarrivals.BuildConfig;
import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.TestUtils;
import criskey.org.rattarrivals.model.Route;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;

/**
 * Created by criskey on 14/1/2016.
 */

public class RouteParserTest extends TestCase {


//    @Ignore
//    @Test
//    public void testResourceRawUri() throws Exception {
//
//        ArrivalsApplication application = (ArrivalsApplication) RuntimeEnvironment.application;
//        InputStream in = application.getResources().openRawResource(R.raw._40arrivals);
//        assertNotNull(in);
//        AbstractNetworkDataDownloader resDowloader = new ResourceStreamDataDownloader(in);
//        assertNotNull(resDowloader.load());
//    }

    @Test
    public void testRouteParsing() throws Exception {

        TestUtils.FileDataDownloader fileDataDownloader = new TestUtils.FileDataDownloader(TestUtils._40ArrivalsFile);
        assertNotNull(fileDataDownloader);
        RouteParser routeParser = new RouteParser(fileDataDownloader, TestUtils.line40);
        assertNotNull(routeParser);

//        List<String> stationNames = routeParser.getStationNamesBothWay();
//        assertEquals("Statia", stationNames.get(0));
//        assertEquals("T.Grozavescu 1a", stationNames.get(1));
//        assertEquals("T.Grozavescu 1a", stationNames.get(stationNames.size() - 1));
//
//        List<String> arrivalTimes = routeParser.getArrivalTimesBothWay();
//        assertTrue("Station and arrival lists must have same size", arrivalTimes.size() == stationNames.size());
//
//        assertEquals("Sosire", arrivalTimes.get(0));
//        assertEquals("2 min.", arrivalTimes.get(1));
//        assertEquals("2 min.", arrivalTimes.get(arrivalTimes.size() - 1));
//
//        Route route = routeParser.getRoute();
//        assertEquals("T.Grozavescu 1a", route.getFromName());
//        assertEquals("Stuparilor 1a", route.getToName());
//
//        criskey.org.rattarrivals.util.Logger.log(this,""+route.getFromToStations());
//        criskey.org.rattarrivals.util.Logger.log(this,"##########");
//        criskey.org.rattarrivals.util.Logger.log(this,""+route.getToFromStations());
        Route route = routeParser.getRoute();
        System.out.println(route);

    }

//    @Ignore
//    @Test
//    public void testDebugRouteParser(){
//        ArrivalsApplication application = (ArrivalsApplication) RuntimeEnvironment.application;
//        InputStream in = application.getResources().openRawResource(R.raw._40arrivals);
//        RouteParser routeParser = new RouteParser.DebugRouteParser(in, TestUtils.line40);
//        Route route = routeParser.getRoute();
//        criskey.org.rattarrivals.util.Logger.log(this,""+route.getFromToStations());
//        criskey.org.rattarrivals.util.Logger.log(this,"##########");
//        criskey.org.rattarrivals.util.Logger.log(this,""+route.getToFromStations());
//    }
//
//    @Ignore
//    @Test
//    public void testRouteParsingRx() throws Exception {
//        Observable.create(new Observable.OnSubscribe<Route>() {
//            @Override
//            public void call(Subscriber<? super Route> subscriber) {
//                TestUtils.FileDataDownloader fileDataDownloader = new TestUtils.FileDataDownloader(TestUtils._40ArrivalsFile);
//                if (!subscriber.isUnsubscribed()) {
//                    try {
//                        RouteParser routeParser = new RouteParser(fileDataDownloader, TestUtils.line40);
//                        subscriber.onNext(routeParser.getRoute());
//                    } catch (ParsingException e) {
//                        Observable.error(e);
//                    }
//                }
//                subscriber.onCompleted();
//            }
//        }).subscribe(new Action1<Route>() {
//            @Override
//            public void call(Route route) {
//                criskey.org.rattarrivals.util.Logger.log(this,""+route.getFromToStations());
//                criskey.org.rattarrivals.util.Logger.log(this,"##########");
//                criskey.org.rattarrivals.util.Logger.log(this,""+route.getToFromStations());
//            }
//        });
//    }

}