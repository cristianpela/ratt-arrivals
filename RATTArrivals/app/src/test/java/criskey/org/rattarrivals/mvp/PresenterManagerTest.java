package criskey.org.rattarrivals.mvp;

import android.support.annotation.StringRes;

import junit.framework.TestCase;

import org.junit.Test;

import criskey.org.rattarrivals.TestUtils;

/**
 * Created by criskey on 28/12/2015.
 */
public class PresenterManagerTest extends TestCase {



    @Test
    public void test_manager() {

        PresenterManager<MockPresenter> presenterManager = PresenterManager.getInstance(TestUtils.baseDir);
        MockPresenter presenter = presenterManager.getPresenter(MockPresenter.class);
        assertNotNull(presenter);
        assertTrue(presenter instanceof MockPresenter);

        //
        presenter.bindView(new MockView());
        assertNotNull(presenter.getView());

        //cache presenter
        presenterManager.putPresenter(presenter);
        assertEquals(1, presenterManager.size());

        presenter.value = 1337;

        //unbind view
        presenter.unbindView();
        assertNull(presenter.getView());


        //
        presenterManager.removePresenter(presenter);
        assertEquals(0, presenterManager.size());


        presenter = presenterManager.getPresenter(MockPresenter.class);
        presenter.bindView(new MockView());
        assertEquals(1337, presenter.value);
    }


    public static class MockView implements IView {
        @Override
        public String toString() {
            return "MockView{}";
        }

        @Override
        public void onShowInfo(String infoMessage) {

        }

        @Override
        public void onShowInfo(@StringRes int resId) {

        }


        @Override
        public void onPresenterAttached() {

        }
    }

    public static class MockPresenter extends BasePresenter<MockView> {

        private static final long serialVersionUID = 1L;

        int value = 1;

        @Override
        public void onBind() {

        }

    }


}