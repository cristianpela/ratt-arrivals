package criskey.org.rattarrivals.parse;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.io.File;
import java.util.List;

import criskey.org.rattarrivals.ArrivalsApplication;
import criskey.org.rattarrivals.BuildConfig;
import criskey.org.rattarrivals.TestUtils;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.model.TransporterType;

/**
 * Created by criskey on 4/1/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 16, application = ArrivalsApplication.class)
public class AbstractNetworkDataLoaderTest extends TestCase {


    @Test
    public void testing_data_parser() throws Exception {
        TransporterType transporterType = TransporterType.TRAM;
        File file = new File(TestUtils.baseDir, TestUtils.transporterTypeMap.get(transporterType));
        AbstractNetworkDataDownloader loader = new TestUtils.FileDataDownloader(file);
        TransporterParser parser = new TransporterParser(loader, transporterType);
        List<Transporter> transporters = parser.getTransporters();
        assertEquals(11, transporters.size());
    }


    @Test
    public void testArrivalURL() {
        String url = "http://86.122.170.105:61978/html/timpi/sens0.php?param1=1006";
        Transporter transporter = new Transporter(TransporterType.TROLLEY, "Linia 14", "", 1006, false);
        assertEquals(url, transporter.getArrivalLink());
    }


}