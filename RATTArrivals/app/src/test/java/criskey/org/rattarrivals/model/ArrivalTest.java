package criskey.org.rattarrivals.model;

import junit.framework.TestCase;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by criskey on 11/4/2016.
 */
public class ArrivalTest {


    @Test
    public void testingPoolingMechanism() {
        // obtain an instance and configs the pool capacity;
        Arrival arrival1 = Arrival.Pool.obtain(3);
        fillArrival(arrival1,"time1");

        Arrival arrival2 = Arrival.Pool.obtain();
        fillArrival(arrival2,"time2");


        Arrival arrival3 = Arrival.Pool.obtain();
        fillArrival(arrival3,"time3");


        Arrival arrival4 = Arrival.Pool.obtain();
        assertEquals(arrival3.getTime(), arrival4.getTime());

        arrival4 = Arrival.Pool.obtain();
        arrival4 = Arrival.Pool.obtain();
        assertEquals(arrival1.getTime(), arrival4.getTime());

        //should be a fresh instance
        arrival4 = Arrival.Pool.obtain();
        assertNull(arrival4.getTime());

        arrival4 = Arrival.Pool.obtain();
        assertNull(arrival4.getTime());
    }

    private void fillArrival(Arrival arrival, String time) {
        arrival.setTime(time);
        arrival.setTransporter(new Transporter(TransporterType.BUS, "line2", "", 1, false));
    }
}