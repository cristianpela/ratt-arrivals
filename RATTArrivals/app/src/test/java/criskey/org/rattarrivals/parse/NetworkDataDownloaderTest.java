package criskey.org.rattarrivals.parse;

import android.support.annotation.NonNull;

import org.jsoup.nodes.Document;
import org.junit.Test;

import criskey.org.rattarrivals.TestUtils;
import criskey.org.rattarrivals.model.Route;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.Exceptions;
import rx.schedulers.Schedulers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by criskey on 17/1/2016.
 */
public class NetworkDataDownloaderTest {


    @Test(expected = ParsingException.class)
    public void testError() throws Exception {
        RouteParser routeParser = buildMockParser();
        routeParser.getRoute();
    }

    @NonNull
    private RouteParser buildMockParser() {
        NetworkDataDownloader mockNetworkDataDownloader = mock(NetworkDataDownloader.class);
        when(mockNetworkDataDownloader.load()).thenReturn(Document.createShell("http://localhost:8880/bogus"));
        return new RouteParser(mockNetworkDataDownloader, TestUtils.line40);
    }

    @Test
    public void testRx(){
        Observable
                .create(new Observable.OnSubscribe<Route>() {
                    @Override
                    public void call(Subscriber<? super Route> subscriber) {
                        if (!subscriber.isUnsubscribed()) {
                            try {
                                RouteParser routeParser = new RouteParser(new NetworkDataDownloader("http://errnoserverrr.ro/bogus"), TestUtils.line40);
                                subscriber.onNext(routeParser.getRoute());
                                subscriber.onCompleted();
                            } catch (ParsingException ex) {
                                Exceptions.propagate(ex);
                            }
                        }

                    }
                })
                .subscribe(new Subscriber<Route>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        criskey.org.rattarrivals.util.Logger.log(this,e.getMessage());
                    }

                    @Override
                    public void onNext(Route route) {

                    }
                });
    }
}