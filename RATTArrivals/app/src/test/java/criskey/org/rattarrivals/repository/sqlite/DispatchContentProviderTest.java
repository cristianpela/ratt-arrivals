package criskey.org.rattarrivals.repository.sqlite;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.location.Address;
import android.net.Uri;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

import junit.framework.TestCase;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.maps.ShadowGeocoder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import criskey.org.rattarrivals.ArrivalsApplication;
import criskey.org.rattarrivals.BuildConfig;
import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.model.TransporterType;

/**
 * Created by criskey on 8/2/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 16, application = ArrivalsApplication.class)
public class DispatchContentProviderTest extends TestCase {

    ContentResolver contentResolver = RuntimeEnvironment.application.getContentResolver();

    @Test
    public void testProvider() {


        //adding and query a transporter
        Transporter t1 = new Transporter(TransporterType.TRAM, "line 1", 1, true);
        Transporter t2 = new Transporter(TransporterType.TRAM, "line 2", 2, true);
        Transporter t3 = new Transporter(TransporterType.TRAM, "line 3", 3, false);
        Transporter b40 = new Transporter(TransporterType.BUS, "line 40", 40, true);
        Transporter b13 = new Transporter(TransporterType.BUS, "line 13", 13, true);
        Transporter b33 = new Transporter(TransporterType.BUS, "line 33", 33, false);
        Transporter tr14 = new Transporter(TransporterType.TROLLEY, "line 14", 14, true);
        Transporter tr11 = new Transporter(TransporterType.TROLLEY, "line 11", 11, false);
        Transporter tr17 = new Transporter(TransporterType.TROLLEY, "line 17", 17, false);

        contentResolver.bulkInsert(Contract.TRANSPORTER_URI,
                new ContentValues[]{
                        transporter2CV(t1),
                        transporter2CV(t2),
                        transporter2CV(t3),
                        transporter2CV(b40),
                        transporter2CV(b13),
                        transporter2CV(b33),
                        transporter2CV(tr14),
                        transporter2CV(tr11),
                        transporter2CV(tr17)
                });

        List<Transporter> transporters = getTransporters(Contract.TRANSPORTER_URI, null, null);
        assertEquals(9, transporters.size());
        Transporter t1Get = transporters.get(0);
        assertNotNull(t1Get);
        assertEquals(t1Get, t1);

        // get favorite transporters
        transporters = getTransporters(Contract.TRANSPORTER_URI, Contract.Transporter.FAVORITE+"=?", new String[]{"1"});
        assertEquals(5, transporters.size());
        criskey.org.rattarrivals.util.Logger.log(this,""+transporters);

        // change favorite for t1
        Uri singleUri = DispatchContentProvider.buildSingleItemUri(Contract.TRANSPORTER_URI, ""+t1.getId());
        ContentValues cv = new ContentValues();
        cv.put(Contract.Transporter.FAVORITE, !t1.isFavorite());
        contentResolver.update(singleUri, cv, null, null);
        // get favorite transporters
        transporters = getTransporters(Contract.TRANSPORTER_URI, Contract.Transporter.FAVORITE+"=?", new String[]{"1"});
        assertEquals(4, transporters.size());
        criskey.org.rattarrivals.util.Logger.log(this,""+transporters);

        //get trams
        transporters = getTransporters(Contract.TRANSPORTER_URI,
                Contract.Transporter.TYPE+"=?", new String[]{TransporterType.TRAM.toString()});
        assertEquals(3, transporters.size());
        criskey.org.rattarrivals.util.Logger.log(this,""+transporters);

        //get buses
        transporters = getTransporters(Contract.TRANSPORTER_URI,
                Contract.Transporter.TYPE+"=?", new String[]{TransporterType.BUS.toString()});
        assertEquals(3, transporters.size());
        criskey.org.rattarrivals.util.Logger.log(this,""+transporters);

        //get trolleys
        transporters = getTransporters(Contract.TRANSPORTER_URI,
                Contract.Transporter.TYPE+"=?", new String[]{TransporterType.TROLLEY.toString()});
        assertEquals(3, transporters.size());
        criskey.org.rattarrivals.util.Logger.log(this,""+transporters);

        //adding and query a station
        Station s1 = new Station("station_1", "1123");
        s1.setAlias("Station Alias");
        contentResolver.insert(Contract.STATION_URI, station2CV(s1));

        Cursor cursor = query(contentResolver, Contract.STATION_URI, null, null);
        assertNotNull(cursor);
        Station s1Get = null;
        while (cursor.moveToNext()) {
            s1Get = cursor2Station(cursor, null, false);
        }
        cursor.close();
        assertNotNull(s1Get);
        assertEquals(s1.getId(), s1Get.getId());

        //insert and query a join
        contentResolver.insert(Contract.JOIN_URI, createJoin(s1, t1));
        contentResolver.insert(Contract.JOIN_URI, createJoin(s1, t2));
        cursor = query(contentResolver, DispatchContentProvider.buildSingleItemUri(Contract.JOIN_URI, s1.getId()), null, null);
        assertNotNull(cursor);


        Station currentStation = null;
        while (cursor.moveToNext()) {
            Station station = cursor2Station(cursor, currentStation, true);
            if (!station.equals(currentStation))
                currentStation = station;
            currentStation.getTransporters().add(cursor2Transporer(cursor));
        }
        cursor.close();
        assertNotNull(currentStation);
        assertEquals(currentStation, s1);
        assertEquals(currentStation.getTransporters().get(0), t1);
        assertEquals(currentStation.getTransporters().get(1), t2);

        //update s1 station location
        cv = new ContentValues();
        double latitude = 45.759780;
        cv.put(Contract.Station.LATITUDE, latitude);
        double longitude = 21.230020;
        cv.put(Contract.Station.LONGITUDE, longitude);
        contentResolver.update(DispatchContentProvider.buildSingleItemUri(Contract.STATION_URI, s1.getId()),
                cv, null, null);
        // update s1 station alias
        cv = new ContentValues();
        String newStationAlias = "New Alias";
        cv.put(Contract.Station.ALIAS, newStationAlias);
        contentResolver.update(DispatchContentProvider.buildSingleItemUri(Contract.STATION_URI, s1.getId()),
                cv, null, null);

        cursor = query(contentResolver, Contract.STATION_URI, null, null);
        assertNotNull(cursor);
        s1Get = null;
        if (cursor.moveToNext()) {
            s1Get = cursor2Station(cursor, null, false);
        }
        cursor.close();
        assertEquals(latitude, s1Get.getLocation().getLatitude());
        assertEquals(longitude,s1Get.getLocation().getLongitude());
        assertEquals(newStationAlias, s1Get.getAlias());
    }

    private List<Transporter> getTransporters(Uri transporterUri, String selection, String[] selectionArgs) {
        Cursor cursor = query(contentResolver, Contract.TRANSPORTER_URI, selection, selectionArgs);
        assertNotNull(cursor);
        List<Transporter> transporters = new ArrayList<>();
        while(cursor.moveToNext()) {
            transporters.add(cursor2Transporer(cursor));
        }
        cursor.close();
        return transporters;
    }

    @Test
    public void testSQLiteStatementInsertBuilder(){
        DispatchContentProvider  provider = Mockito.mock(DispatchContentProvider.class);

        ContentValues cv = new ContentValues();
        cv.put("a", 100);
        cv.put("b", "Name");
        cv.put("c", "Last Name");
        cv.put("d", 1.2);
        String insertStatement = "INSERT INTO SOME_TABLE(a, b, c, d) VALUES(?, ?, ?, ?);";

        Mockito.when(provider.createGenericPreparedSqlInsertScript("SOME_TABLE", cv.keySet())).thenCallRealMethod();
        assertEquals(insertStatement, provider.createGenericPreparedSqlInsertScript("SOME_TABLE", cv.keySet()));
        SQLiteStatement statement = Mockito.mock(SQLiteStatement.class);
        Mockito.when(provider.bindStatement(statement, cv)).thenCallRealMethod();
        provider.bindStatement(statement, cv);
        Mockito.verify(statement).bindLong(1, 100);
        Mockito.verify(statement).bindString(2, "Name");
        Mockito.verify(statement).bindString(3, "Last Name");
        Mockito.verify(statement).bindDouble(4, 1.2);
    }

    public ContentValues createJoin(Station station, Transporter transporter) {
        ContentValues cv = new ContentValues();
        cv.put(Contract.JoinStationTransporter.FK_STATION, station.getId());
        cv.put(Contract.JoinStationTransporter.FK_TRANSPORTER, transporter.getId());
        return cv;
    }

    public Cursor query(ContentResolver contentResolver, Uri uri, String selection, String[] selectionArgs) {
        return contentResolver.query(uri, null, selection, selectionArgs, null);
    }

    public ContentValues station2CV(Station station) {
        ContentValues cv = new ContentValues();
        cv.put(Contract.Station._ID, Integer.parseInt(station.getId()));
        cv.put(Contract.Station.NAME, station.getName());
        cv.put(Contract.Station.ALIAS, station.getAlias());
        return cv;
    }

    public Station cursor2Station(Cursor cursor, Station currentStation, boolean joined) {
        String stationId = Integer.toString(cursor.getInt(cursor.getColumnIndexOrThrow(Contract.Station._ID)));
        if (currentStation != null && stationId != currentStation.getId())
            return currentStation;
        Station station = new Station();
        station.setId(stationId);
        if(!joined) {
            station.setName(cursor.getString(cursor.getColumnIndexOrThrow(Contract.Station.NAME)));
            station.setAlias(cursor.getString(cursor.getColumnIndexOrThrow(Contract.Station.ALIAS)));
            double latitude = cursor.getDouble(cursor.getColumnIndexOrThrow(Contract.Station.LATITUDE));
            double longitude = cursor.getDouble(cursor.getColumnIndexOrThrow(Contract.Station.LONGITUDE));
            station.setLocation(new Location(latitude, longitude));
        }
        return station;
    }

    public ContentValues transporter2CV(Transporter transporter) {
        ContentValues cv = new ContentValues();
        cv.put(Contract.Transporter._ID, transporter.getId());
        cv.put(Contract.Transporter.NAME, transporter.getLine());
        cv.put(Contract.Transporter.TYPE, transporter.getTransporterType().toString());
        cv.put(Contract.Transporter.FAVORITE, transporter.isFavorite());
        return cv;
    }

    public Transporter cursor2Transporer(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndexOrThrow(Contract.Transporter._ID));
        String name = cursor.getString(cursor.getColumnIndexOrThrow(Contract.Transporter.NAME));
        TransporterType type = TransporterType.valueOf(
                cursor.getString(cursor.getColumnIndexOrThrow(Contract.Transporter.TYPE)));
        int fav = cursor.getInt(cursor.getColumnIndexOrThrow(Contract.Transporter.FAVORITE));
        return new Transporter(type, name, id, fav == 1);
    }

    @Test
    @Ignore
    public void testForFindingTheClosesStationInARadius(){
        ShadowGeocoder geocoder = new ShadowGeocoder();
        geocoder.setSimulatedLatLong(45.773763, 21.243884);
        LatLng knownOrigin = new LatLng(45.773763, 21.243884);
        String street = "Street Verde A97, Timisoara, Timis County, Romania";
        LatLng origin = null;
        try {
            Address address = geocoder.getFromLocationName(street, 1).get(0);
            origin = new LatLng(address.getLatitude(), address.getLongitude());
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertNotNull(origin);
        assertEquals(knownOrigin, origin);
        LatLng s40stuparilor = new LatLng(45.773040, 21.245803);
        LatLng sE2stuparilor = new LatLng(45.772797, 21.245910);
        LatLng s40holdelor = new LatLng(45.775292, 21.239045);
        LatLng s40garaest = new LatLng(45.767584, 21.244796);
        List<LatLng> stationCoordinates = new ArrayList<>();
        stationCoordinates.add(s40stuparilor);
        stationCoordinates.add(sE2stuparilor);
        stationCoordinates.add(s40holdelor);
        stationCoordinates.add(s40garaest);

        double radius = 200.0;//meters

        List<LatLng> closestDistances = new ArrayList<>();
        for(LatLng latLng: stationCoordinates){
            double distance = SphericalUtil.computeDistanceBetween(origin, latLng);
            criskey.org.rattarrivals.util.Logger.log(this,"Distance: " + distance);
            if(distance <= radius){
                closestDistances.add(latLng);
            }
        }
        criskey.org.rattarrivals.util.Logger.log(this,""+closestDistances);


    }
}