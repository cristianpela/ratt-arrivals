package criskey.org.rattarrivals.util;

import android.content.Context;
import android.util.SparseArray;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import criskey.org.rattarrivals.BuildConfig;
import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.ArrivalsApplication;
import criskey.org.rattarrivals.TestUtils;

/**
 * Created by criskey on 12/1/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 16, application = TestUtils.TestApplication.class)
public class UtilsTest extends TestCase {

    @Test
    public void testTranslation(){
        Context context = RuntimeEnvironment.application.getApplicationContext();
        String phrase = "Linia autobuz 40";
        SparseArray<String> resPairs = new SparseArray<>();
        resPairs.put(R.string.locale_line_text, "Linia");
        resPairs.put(R.string.bus, "autobuz");
        assertEquals("Line bus 40", Utils.insertTranslation(context, phrase, resPairs));

        phrase = "Linia 40";
        assertEquals("Line 40", Utils.insertOneTranslation(context, phrase, R.string.locale_line_text, "Linia"));
    }

    @Test
    public void testPrettifyStationName() throws Exception {
        String name = "Tv_Gara Nord 1";
        assertEquals("Gara Nord", Utils.prettifyStationName(name));
    }
}