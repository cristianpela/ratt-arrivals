package criskey.org.rattarrivals.repository;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;

import criskey.org.rattarrivals.ArrivalsApplication;
import criskey.org.rattarrivals.BuildConfig;

/**
 * Created by criskey on 29/1/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 16, application = ArrivalsApplication.class)
public class PreferencesRepositoryImplTest extends TestCase {

    @Test
    public void testPreferences() {
        ArrivalsApplication application = (ArrivalsApplication) RuntimeEnvironment.application;

        PreferencesRepository preferencesRepository = new PreferencesRepositoryImpl(application.getApplicationContext());
        assertFalse(preferencesRepository.hasData());
        preferencesRepository.validateData(true);
        assertTrue(preferencesRepository.hasData());

        int currentLang = 0;
        assertEquals("Should be english language 0", currentLang, preferencesRepository.getCurrentLanguage());

        preferencesRepository.setCurrentLanguage(1);
        currentLang = 1;
        assertEquals("Should be romanian language 1", currentLang, preferencesRepository.getCurrentLanguage());

    }
}