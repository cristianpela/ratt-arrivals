package criskey.org.rattarrivals;

import android.widget.TextView;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.util.ActivityController;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static org.junit.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */


public class JSoupUnitTest {

    static long lastRefreshDate = System.currentTimeMillis();

    @Test
    public void rx() throws Exception {
        final Observable<String> agoObservable = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(calculateAgo());
                    subscriber.onCompleted();
                }
            }
        }).take(5);

        Observable
                .interval(1, TimeUnit.SECONDS, Schedulers.newThread()).flatMap(new Func1<Long, Observable<String>>() {
            @Override
            public Observable<String> call(Long aLong) {
                return agoObservable;
            }
        })
                // .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String ago) {
                        criskey.org.rattarrivals.util.Logger.log(this,"Calculated " + ago);
                        // getView().onLastRefreshCheck(ago);
                    }
                });

        // Thread.sleep(100000);
    }

    private String calculateAgo() {
        long now = System.currentTimeMillis();
        return "Updated: " + TimeUnit.MILLISECONDS.toSeconds(now - lastRefreshDate) + " minutes";
    }

}