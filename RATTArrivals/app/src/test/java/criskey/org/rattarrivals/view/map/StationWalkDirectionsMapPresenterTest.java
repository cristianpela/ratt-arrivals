package criskey.org.rattarrivals.view.map;

import android.support.annotation.StringRes;

import com.google.android.gms.maps.model.LatLng;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import criskey.org.rattarrivals.BuildConfig;
import criskey.org.rattarrivals.TestUtils;
import criskey.org.rattarrivals.util.Logger;

import static criskey.org.rattarrivals.util.Logger.log;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by criskey on 24/3/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 16, application = TestUtils.TestApplication.class)
public class StationWalkDirectionsMapPresenterTest {

   /* private static final LatLng HOME = new LatLng(45.774201, 21.244120);
    private static final LatLng STATION = new LatLng(45.772817, 21.245901);

    double[] localNodes = new double[]{
            1.2, 1.3,
            1.5, 2.6,
            3.7, 4.6
    };

    private StationWalkDirectionsMapViewMock view;
    private StationWalkDirectionsMapPresenter presenter;

    @Before
    public void setup(){
        view = new StationWalkDirectionsMapViewMock();
        presenter = spy(new StationWalkDirectionsMapPresenter(localNodes));
        doReturn(view).when(presenter).getView();
    }

    @Ignore
    @Test
    public void testIfLocalSavePathIsProperDecoded() {
        presenter.onBind();

        assertEquals(localNodes.length, view.path.size() * 2); // group by 2
        assertEquals(new LatLng(1.2, 1.3), view.path.get(0));
        assertEquals(new LatLng(1.5, 2.6), view.path.get(1));
        assertEquals(new LatLng(3.7, 4.6), view.path.get(2));

        presenter.onUnbind();
        presenter.onBind();

        assertEquals(localNodes.length, view.path.size() * 2); // group by 2
        assertEquals(new LatLng(1.2, 1.3), view.path.get(0));
        assertEquals(new LatLng(1.5, 2.6), view.path.get(1));
        assertEquals(new LatLng(3.7, 4.6), view.path.get(2));
    }

    @Test
    public void integrationTestWithParser() throws InterruptedException {
        Logger.log("TESTING INTEGRATION REMOTE JSON");
        LatLng from = new LatLng(45.774201, 21.244120),
                to = new LatLng(45.766782, 21.244833); // gara de est
        presenter.onBind();
        presenter.obtainWalkingDirections(from, to);


        Thread.sleep(7000);

        presenter.onUnbind();
        presenter.onBind();

    }

    private class StationWalkDirectionsMapViewMock implements StationWalkDirectionsMapView {

        List<LatLng> path;
        List<String> directions;

        @Override
        public void onDecodedMapPath(List<LatLng> path) {
            this.path = path;
            TestUtils.printCollection(path);
        }

        @Override
        public void onWalkingDirections(List<String> directions) {
            this.directions = directions;
            TestUtils.printCollection(directions);
        }

        @Override
        public void onShowWait() {
            log("Wait");
        }

        @Override
        public void onHideWait() {

        }

        @Override
        public void onShowProgress(@StringRes int res) {

        }

        @Override
        public void onShowError(String errorMessage) {
            log(errorMessage);
        }

        @Override
        public void onShowInfo(String infoMessage) {

        }

        @Override
        public void onProgressInfo(String progMessage) {

        }

        @Override
        public void onPresenterAttached() {

        }
    }*/
}