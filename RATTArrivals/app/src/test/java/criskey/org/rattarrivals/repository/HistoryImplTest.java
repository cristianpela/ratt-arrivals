package criskey.org.rattarrivals.repository;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by criskey on 1/2/2016.
 */
public class HistoryImplTest {

    History<String> th;


    @Test
    public void testConstructHistoryWithSetLessThanDefaultCapacity() {
        String[] provided = {"1", "2", "3"};
        createHistory(provided);
        assertEquals(History.DEFAULT_CAPACITY, th.getCapacity());
        assertEquals(printedList(Arrays.asList(provided), true), printedHistory());
    }

    @Test
    public void testConstructHistoryWithSetBiggerThanDefaultCapacity() {
        String[] provided = new String[]{"1", "2", "3", "4", "5", "6"};
        createHistory(provided);
        assertEquals(provided.length, th.getCapacity());
        assertEquals(printedList(Arrays.asList(provided), true), printedHistory());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadCapacity() {
        th = new HistoryImpl<>();
        th.setCapacity(1);
    }

    @Test
    public void testHistoryOps() {
        th = new HistoryImpl<>();

        th.push("1");
        th.push("2");
        assertEquals(2, getHistorySize());
        assertEquals("2", getTop());
        th.push("3");
        th.push("4");
        th.push("5");
        // adding an entry over the capacity History.DEFAULT_CAPACITY
        th.push("6");
        assertEquals(th.getCapacity(), getHistorySize());
        assertFalse("Should not contain id 1 anymore", th.getEntries().contains("1"));
        assertEquals("65432", printedHistory());

        //pushing an already added entry will result moving that to the top
        th.push("3");
        assertEquals("3", getTop());
        //push same entry again right after, should do nothing
        th.push("3");
        assertEquals("3", getTop());

        // increasing the capacity
        th.setCapacity(6);
        th.push("7");
        assertEquals("7", getTop());
        assertEquals("736542", printedHistory());

        th.setCapacity(8);
        th.push("8");
        th.push("9");
        assertEquals("98736542", printedHistory());
        th.setCapacity(6);
        assertEquals("987365", printedHistory());

    }

    protected void createHistory(String[] provided) {
        Set<String> entries = new HashSet<>();
        entries.addAll(Arrays.asList(provided));
        th = new HistoryImpl<>(entries);
    }

    protected int getHistorySize() {
        return th.getEntries().size();
    }

    protected String getTop() {
        return th.getEntries().get(0);
    }

    protected String printedHistory() {
        return printedList(th.getEntries(), false);
    }

    protected String printedList(List<String> provided, boolean reversed) {
        if (reversed)
            Collections.reverse(provided);
        StringBuilder b = new StringBuilder();
        for (String e : provided) {
            b.append(e);
        }
        return b.toString();
    }

}