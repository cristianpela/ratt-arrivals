package criskey.org.rattarrivals.mvp;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import criskey.org.rattarrivals.ArrivalsApplication;

/**
 * Created by criskey on 5/1/2016.
 */
public abstract class BaseActivity<P extends Presenter> extends AppCompatActivity implements IView {

    boolean isDestroyedFromBackPressed;

    private VPBinder<? extends IView, P> mVPBinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVPBinder = new VPBinder<>(this);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0)
            isDestroyedFromBackPressed = true;
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        mVPBinder.bindView();
        onPresenterAttached();
        super.onStart();

    }

    @Override
    protected void onPause() {
        mVPBinder.unbindView();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (isDestroyedFromBackPressed) {
            mVPBinder.destroyPresenter();
            mVPBinder = null; // dereferencing to avoid leaks
        }
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Resources res = getResources();
        Configuration conf = new Configuration(res.getConfiguration());
        conf.locale = getPresenter().getCurrentLanguage();
        res.updateConfiguration(conf, res.getDisplayMetrics());
        super.onConfigurationChanged(newConfig);
    }

    public abstract void onPresenterAttached();


    @Override
    public void onShowInfo(String infoMessage) {
        Toast.makeText(this, infoMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShowInfo(int resId) {
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show();
    }

    public P getPresenter() {
        return mVPBinder.getPresenter();
    }


    private PresenterManager<P> getPresenterManager() {
        return ((ArrivalsApplication) getApplication()).getPresenterManager();
    }

}
