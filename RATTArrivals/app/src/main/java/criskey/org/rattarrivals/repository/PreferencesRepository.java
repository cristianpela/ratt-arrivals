package criskey.org.rattarrivals.repository;

import java.util.List;
import java.util.Locale;

/**
 * Created by criskey on 29/1/2016.
 */
public interface PreferencesRepository extends Repository<Void, Void>{

    /**
     * Checks if there is database initialized. If not, must proceed to download data over network
     * This will be checked when app starts.
     *
     * @return true if has data
     */
    public boolean hasData();

    /**
     * Marks that the database was successfully initialized or wiped.
     */
    public void validateData(boolean hasData);

    /**
     * Get last clicked display filter by user
     *
     * @return
     */
    public String getLastDisplayFilter();

    /**
     * Set last clicked display filter by user
     *
     * @param displayFilter
     */
    public void setLastDisplayFilter(String displayFilter);

    /**
     * Get current language code. Default will be {@link Locale#getDefault()}
     * @return
     */
    public int getCurrentLanguage();

    /**
     * Set current language
     * @param languageIndex
     */
    public void setCurrentLanguage(int languageIndex);

    /**
     *
     * @return Available languages
     */
    public String[] getAvailableLanguages();

    /**
     * makes the current language available to the ui
     */
    public void makeCurrentLanguageAvailable();

}
