package criskey.org.rattarrivals.parse;

import android.net.Uri;

import com.google.maps.android.PolyUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.util.MapUtil;

/**
 * Created by criskey on 24/3/2016.
 */
public class WalkingDirectionsParser extends JSONParser {

    private JSONObject route;

    private WalkingDirections walkingDirections;

    public WalkingDirectionsParser(AbstractNetworkDataDownloader downloader) throws ParsingException, JSONException {
        super(downloader);
        createWalkingDirections();
    }

    public WalkingDirectionsParser(Location from, Location to, String lang) throws ParsingException, JSONException {
        super(new NetworkDataDownloader(buildUrl(from, to, lang)));
        createWalkingDirections();
    }

    private static String buildUrl(Location from, Location to, String lang) {
        Uri uri = Uri.parse("http://maps.googleapis.com/maps/api/directions/json")
                .buildUpon()
                .appendQueryParameter("origin", from.toString())
                .appendQueryParameter("destination", to.toString())
                .appendQueryParameter("sensor", "false")
                .appendQueryParameter("avoid", "highways")
                .appendQueryParameter("mode", "walking")
                .appendQueryParameter("language", lang)
                .build();
        return uri.toString();
    }

    private void createWalkingDirections() throws JSONException {
        walkingDirections = new WalkingDirections();
        route = jsonDocument.getJSONArray("routes").getJSONObject(0);
        parsePathNodes();
        parseDirections();
    }

    private void parsePathNodes() throws JSONException {
        String encodedPoints = route.getJSONObject("overview_polyline").getString("points");
        walkingDirections.pathNodes = MapUtil.toLocationList(PolyUtil.decode(encodedPoints));
    }

    private void parseDirections() throws JSONException {
        JSONArray steps = route.getJSONArray("legs")
                .getJSONObject(0)
                .getJSONArray("steps");
        walkingDirections.directions  = new ArrayList<>();
        for (int i = 0; i < steps.length(); i++) {
            JSONObject obj = steps.getJSONObject(i);
            String walkingDirection = obj.getString("html_instructions");
            walkingDirections.directions.add(walkingDirection);
        }
    }

    public WalkingDirections getWalkingDirections(){
        return walkingDirections;
    }

    public static final class WalkingDirections {

        private List<String> directions;
        private List<Location> pathNodes;

        private WalkingDirections(){}

        public List<String> getDirections() {
            return Collections.unmodifiableList(directions);
        }

        public List<Location> getPathNodes() {
            return Collections.unmodifiableList(pathNodes);
        }
    }

}
