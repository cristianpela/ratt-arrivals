package criskey.org.rattarrivals.repository.sqlite;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.provider.BaseColumns;

import java.util.Set;

public class DispatchContentProvider extends ContentProvider {

    private SQLiteHelper dbHelper;

    public DispatchContentProvider() {
    }

    public static Uri buildSingleItemUri(Uri from, String id) {
        return from.buildUpon().appendPath(id).build();
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int rowsCount = -1;
        db.beginTransaction();
        try {
            rowsCount = db.delete(getTableName(uri), selection, selectionArgs);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return rowsCount;
    }

    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        String tableName = getTableName(uri);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            db.beginTransaction();
            db.insert(tableName, null, values);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return uri;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        int insertedRows = -1;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            insertedRows = (int) executeInsertStatement(db, uri, values);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return insertedRows;
    }

    public long executeInsertStatement(SQLiteDatabase db, Uri uri, ContentValues[] values) {
        String tableName = getTableName(uri);
        String sql = createGenericPreparedSqlInsertScript(tableName, values[0].keySet());
        SQLiteStatement statement = db.compileStatement(sql);
        long executed = -1;
        for (int i = 0; i < values.length; i++) {
            statement.clearBindings();
            bindStatement(statement, values[i]);
            executed = statement.executeInsert();
        }
        return executed;
    }

    public boolean bindStatement(SQLiteStatement statement, ContentValues cv) {
        Set<String> keys = cv.keySet();
        // in SQLiteStatement, column indexing is starting from 1
        int colIndex = 1;
        for (String key : keys) {
            Object value = cv.get(key);
            Class<?> classValue = value.getClass();
            if (classValue.equals(Integer.class) || classValue.equals(Long.class))
                statement.bindLong(colIndex, Long.parseLong(value.toString()));//hack. since I can't convert Integer to Long
            else if (classValue.equals(Double.class))
                statement.bindDouble(colIndex, (Double) value);
            else if (classValue.equals(String.class))
                statement.bindString(colIndex, (String) value);
            else if (classValue.equals(Boolean.class))
                statement.bindLong(colIndex, (Boolean) value ? 1 : 0);
            colIndex++;
        }
        return true;
    }

    public String createGenericPreparedSqlInsertScript(String tableName, Set<String> columns) {
        // should return something like: "INSERT INTO SOME_TABLE(a, b, c, d) VALUES(?, ?, ?, ?);"
        int length = columns.size();
        StringBuilder sqlBuilder = new StringBuilder("INSERT INTO " + tableName + "(");
        for (String column : columns) {
            sqlBuilder.append(column + ", ");
        }
        sqlBuilder.delete(sqlBuilder.length() - 2, sqlBuilder.length());// getting rid of last comma
        sqlBuilder.append(") VALUES(");
        for (int i = 0; i < length; i++) {
            sqlBuilder.append("?, ");
        }
        sqlBuilder.delete(sqlBuilder.length() - 2, sqlBuilder.length());// getting rid of last comma
        sqlBuilder.append(");");
        return sqlBuilder.toString();
    }

    @Override
    public boolean onCreate() {
        dbHelper = new SQLiteHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        if (Contract.matcher.match(uri) == Contract.SINGLE_JOIN_MATCH_ID) {
            return rawJoinQuery(db, uri);
        } else if (Contract.matcher.match(uri) == Contract.STATION_MATCH_DISTINCT_ID)
            return db.query(true, getTableName(uri), projection, selection, selectionArgs, null, null, null, null);

        return db.query(getTableName(uri), projection, selection, selectionArgs, null, null, null);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String id = uri.getLastPathSegment();
        String tableName = uri.getPathSegments().get(uri.getPathSegments().size() - 2);
        String columnId = (tableName.equals(Contract.Station.TITLE)) ?
                Contract.Station._ID : (tableName.equals(Contract.Transporter.TITLE)) ?
                Contract.Transporter._ID : BaseColumns._ID;
        return db.update(tableName, values, columnId + "=?", new String[]{id});
    }

    private String getTableName(Uri uri) {
        String tableName = null;
        int match = Contract.matcher.match(uri);
        switch (match) {
            case Contract.TRANSPORTER_MATCH_ID:
                tableName = Contract.Transporter.TITLE;
                break;
            case Contract.STATION_MATCH_ID:
                tableName = Contract.Station.TITLE;
                break;
            case Contract.STATION_MATCH_DISTINCT_ID:
                tableName = Contract.Station.TITLE;
                break;
            case Contract.JOIN_MATCH_ID:
                tableName = Contract.JoinStationTransporter.TITLE;
                break;
        }
        return tableName;
    }

    private Cursor rawJoinQuery(SQLiteDatabase db, Uri uri) {
        String sql = "SELECT t.*, s." + Contract.Station._ID + " FROM " + Contract.Transporter.TITLE + "  AS t, " +
                Contract.Station.TITLE + " AS s, " + Contract.JoinStationTransporter.TITLE + " AS j " +
                " WHERE t." + Contract.Transporter._ID + "=j." + Contract.JoinStationTransporter.FK_TRANSPORTER + " AND " +
                " s." + Contract.Station._ID + "=j." + Contract.JoinStationTransporter.FK_STATION + " AND " +
                " j." + Contract.JoinStationTransporter.FK_STATION + "=?";
        String[] args = new String[]{uri.getLastPathSegment()};
        return db.rawQuery(sql, args);
    }
}
