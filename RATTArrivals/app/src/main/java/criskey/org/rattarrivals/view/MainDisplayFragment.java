package criskey.org.rattarrivals.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.InputStream;
import java.util.List;

import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.mvp.BaseFragment;
import criskey.org.rattarrivals.util.Utils;
import criskey.org.rattarrivals.util.WaitNotifyContentDelegate;
import criskey.org.rattarrivals.view.map.StationMapsActivity;
import criskey.org.rattarrivals.view.station.StationActivity;

import static criskey.org.rattarrivals.util.WaitNotifyContentDelegate.Adapter;
import static criskey.org.rattarrivals.util.WaitNotifyContentDelegate.AdapterStrategy;
import static criskey.org.rattarrivals.util.WaitNotifyContentDelegate.ResIdConfig;

/**
 * Created by criskey on 8/1/2016.
 */
// TODO: 4/4/2016 Replace history drawer with a popup
public class MainDisplayFragment extends BaseFragment<MainDisplayFragmentPresenter> implements MainDisplayFragmentView,
        ListRouteStationRecyclerAdapter.OnStationClickCallback {

    public static final String FRAGMENT_TAG = MainDisplayFragment.class.getSimpleName();

    private static final String ARG_TRANSPORTER = "arg_transporter";
    private static final String ARG_HOST_CONTAINER = "arg_host";

    private TextView mTextCurrentLine;
    //   private TextView mTextLastRefresh;
    private TextView mTextFrom;
    private TextView mTextTo;

    private ListPopupWindow mPopupHistory;

    private ImageButton mImageButtonRefresh;
    private ImageButton mImageButtonChangeWay;
    private ImageButton mImageButtonHistoryPopup;
    private ImageButton mImageButtonRouteMap;

    private RecyclerView mListRouteStations;
    // private ListView mListHistory;

    private boolean mIsHistoryPoppedUp;

    private WaitNotifyContentDelegate mWaitNotifyViewDelegate;

    private OnTransporterItemClick mOnTransporterItemClickCallback;

    private OnWaitMainDisplayFragment mOnWaitMainDisplayFragmentCallback;

    public static final MainDisplayFragment createFragment(Transporter transporter) {
        MainDisplayFragment fragment = new MainDisplayFragment();
        Bundle args = new Bundle();
        //TODO modify this to parcelable when Transporter implements Parcelable or even better: pass an ID
        args.putSerializable(ARG_TRANSPORTER, transporter);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void onArgumentsAttached(Bundle args) {
        //this will be checked when we are in portrait mode
        //TODO modify this to parcelable when Transporter implements Parcelable or even better: pass an ID
        Transporter transporter = (Transporter) args.getSerializable(ARG_TRANSPORTER);
        setHostContainer(PORTRAIT_CONTAINER);
        setTransporter(transporter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mOnTransporterItemClickCallback = (OnTransporterItemClick) context;
        mOnWaitMainDisplayFragmentCallback = (OnWaitMainDisplayFragment) context;
    }

    @Override
    public void onDetach() {
        mOnTransporterItemClickCallback = null;
        mOnWaitMainDisplayFragmentCallback = null;
        super.onDetach();
    }

    @Override
    public void onPresenterAttached() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.display_transporter_layout, container, false);

        mTextCurrentLine = (TextView) view.findViewById(R.id.textSelectedLine);
        //  mTextLastRefresh = (TextView) view.findViewById(R.id.textLastRefresh);
        mTextFrom = (TextView) view.findViewById(R.id.textFrom);
        mTextTo = (TextView) view.findViewById(R.id.textTo);

        mImageButtonChangeWay = (ImageButton) view.findViewById(R.id.buttonChangeWay);
        mImageButtonChangeWay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPresenter().changeWay();
            }
        });

        mImageButtonRefresh = (ImageButton) view.findViewById(R.id.imageButtonRefresh);
        if (isLandscape()) {//we're in landscape
            mImageButtonRefresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    refresh();
                }
            });
        }


        mListRouteStations = (RecyclerView) view.findViewById(R.id.listRouteStations);
        mListRouteStations.setLayoutManager(new LinearLayoutManager(getActivity()));
        mListRouteStations.setAdapter(new ListRouteStationRecyclerAdapter(this));

        mImageButtonHistoryPopup = (ImageButton) view.findViewById(R.id.imageButtonHistoryPopup);
        mImageButtonHistoryPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsHistoryPoppedUp = !mIsHistoryPoppedUp;
                if (mIsHistoryPoppedUp) {
                    mPopupHistory.show();
                } else {
                    mPopupHistory.dismiss();
                }
            }
        });

        mImageButtonRouteMap = (ImageButton) view.findViewById(R.id.imageButtonRouteMap) ;
        if(isLandscape()) {
            mImageButtonRouteMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startMapRouteActivity();
                }
            });
        }

        mPopupHistory = new ListPopupWindow(getActivity());
        mPopupHistory.setAnchorView(mImageButtonHistoryPopup);
        mPopupHistory.setWidth(200);
        mPopupHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Transporter transporter = (Transporter) parent.getAdapter().getItem(position);
                getPresenter().setTransporter(transporter);
                mOnTransporterItemClickCallback.onTransporterClickFromFragment(transporter);
                mPopupHistory.dismiss();
            }
        });

        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (mPopupHistory != null && mPopupHistory.isShowing())
                    mPopupHistory.dismiss();
            }
        });

        return view;
    }

    public void refresh() {
        getPresenter().refresh(null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ResIdConfig config = new ResIdConfig.Builder()
                .mainContentLayoutId(R.id.displayContent).build();
        Adapter adapter = AdapterStrategy.fragmentAdapter(this,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getPresenter().retryOnError();
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mWaitNotifyViewDelegate.onHideWait();
                        getFragmentManager().popBackStack();
                    }
                }
        );
        mWaitNotifyViewDelegate = new WaitNotifyContentDelegate(adapter, config);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onNothingToShow() {
        //mWaitNotifyViewDelegate.onHideWait();
    }

    @Override
    public void onSettingCurrentLine(String line) {
        //TODO: Use string format
        if (isLandscape()) // we're in landscape
            mTextCurrentLine.setText(Utils.insertOneTranslation(getActivity(), line, R.string.locale_line_text, "Linia"));
    }

    @Override
    public void onLastRefreshCheck(String ago) {
        // mTextLastRefresh.setText(ago);
    }

    @Override
    public void onDisplayRoute(List<Station> stations, String from, String to) {
        mTextFrom.setText(from);
        mTextTo.setText(to);
        ((ListRouteStationRecyclerAdapter) mListRouteStations.getAdapter()).setStations(stations);
        if (!isLandscape())
            mOnWaitMainDisplayFragmentCallback.onFinishToLoadMainDisplayInPortrait(getRouteLineTitle());
    }

    @Override
    public void onShowWait() {
        if (!isLandscape())
            mOnWaitMainDisplayFragmentCallback.onWaitToLoadMainDisplayInPortrait();
        mWaitNotifyViewDelegate.onShowWait();
    }

    @Override
    public void onHideWait() {
        mWaitNotifyViewDelegate.onHideWait();
    }

    @Override
    public void onShowError(String errorMessage) {
        mWaitNotifyViewDelegate.onShowError(errorMessage);
    }

    @Override
    public void onShowProgress(@StringRes int res) {
        mWaitNotifyViewDelegate.onShowProgress(res);
    }

    @Override
    public void onShowHistory(List<Transporter> transporterHistory) {
        //  mListHistory.setAdapter(new ArrayAdapter<Transporter>(getContext(), android.R.layout.simple_list_item_1, transporterHistory));
        mPopupHistory.setAdapter(new ArrayAdapter<Transporter>(getContext(), android.R.layout.simple_list_item_1, transporterHistory));
    }

    @Override
    public void onDestroy() {
        // destroy might be called when fragment is detached so must null check mWaitNotifyViewDelegate
        if (isAdded())
            mWaitNotifyViewDelegate.destroy();
        super.onDestroy();
    }

    // this will be called in portrait from onCreate and in landscape from MainActivity
    public void setTransporter(Transporter transporter) {
        // debug/offline mode
        //getPresenter().setTransporter(transporter, getDebugRawResourceInputStream());
        // online mode
        getPresenter().setTransporter(transporter);
    }

    protected InputStream getDebugRawResourceInputStream() {
        return getResources().openRawResource(R.raw._40arrivals);
    }

    public boolean isEmpty() {
        return getPresenter().isEmpty();
    }

    /**
     * @return PORTRAIT_CONTAINER} or LANDSCAPE_CONTAINTER
     */
    public int getHostContainer() {
        return getPresenter().getHostContainer();
    }

    public void setHostContainer(@HostContainer int hostContainer) {
        getPresenter().setHostContainer(hostContainer);
    }

    @Override
    public void onStationClick(String stationName) {
        startActivity(StationActivity.createIntent(getActivity(), stationName));
    }

    public String getRouteLineTitle() {
        return getPresenter().getTransporter().getLine();
    }

    /**
     * if mImageButtonRefresh is not null, then clearly we are in landscape view
     *
     * @return
     */
    private boolean isLandscape() {
        return mImageButtonRefresh != null;
    }

    public void startMapRouteActivity() {
        Intent mapIntent = StationMapsActivity.createRouteLocationIntent(getActivity(), getPresenter().getWholeRouteWithLocations());
        startActivity(mapIntent);
    }


    public static interface OnWaitMainDisplayFragment {

        public void onWaitToLoadMainDisplayInPortrait();

        public void onFinishToLoadMainDisplayInPortrait(String linetTitle);

    }

}
