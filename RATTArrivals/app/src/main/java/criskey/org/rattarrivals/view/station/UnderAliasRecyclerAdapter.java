package criskey.org.rattarrivals.view.station;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.List;

import criskey.org.rattarrivals.R;

/**
 * Created by criskey on 25/2/2016.
 */
public class UnderAliasRecyclerAdapter extends RecyclerView.Adapter<UnderAliasRecyclerAdapter.UnderAliasHolder> {


    private WeakReference<Callback> callbackRef;

    private List<String> stationNames;

    public UnderAliasRecyclerAdapter(List<String> stationNames, Callback callback) {
        this.stationNames = stationNames;
        callbackRef = new WeakReference<Callback>(callback);
    }

    @Override
    public UnderAliasHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_display_station_under_alias, parent, false);
        return new UnderAliasHolder(view);
    }

    @Override
    public void onBindViewHolder(UnderAliasHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return stationNames.size();
    }

    public static interface Callback {

        public void removeAlias(int position);

        public void editAlias(int position);

    }

    public class UnderAliasHolder extends RecyclerView.ViewHolder {

        private TextView mTextStationName;
        private ImageButton mImageButtonEdit;
        private ImageButton mImageButtonRemove;

        public UnderAliasHolder(View itemView) {
            super(itemView);
            mTextStationName = (TextView) itemView.findViewById(R.id.textStationNameUnderAlias);
            mImageButtonEdit = (ImageButton) itemView.findViewById(R.id.imageButtonEditAlias);
            mImageButtonRemove = (ImageButton) itemView.findViewById(R.id.imageButtonRemoveAlias);
        }

        public void bind(final int position) {
            mTextStationName.setText(stationNames.get(position));
            if (callbackRef.get() == null)
                return;
            mImageButtonEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callbackRef.get().editAlias(position);
                }
            });
            mImageButtonRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callbackRef.get().removeAlias(position);
                }
            });
        }
    }
}
