package criskey.org.rattarrivals.view.station;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.mvp.BaseActivity;
import criskey.org.rattarrivals.util.Logger;

public class StationActivity extends BaseActivity<StationPresenter> implements StationView, StationNearFragment.Callback {

    private static final String ARG_STATION_NAME = "ARG_STATION_NAME";

    private FragmentManager fragmentManager;

    private boolean recreated;

    public static Intent createIntent(Context context, String stationName) {
        return new Intent(context, StationActivity.class)
                .putExtra(ARG_STATION_NAME, stationName)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station);
        Toolbar topToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(topToolbar);
        fragmentManager = getSupportFragmentManager();
        recreated = savedInstanceState != null;
        recreateFragments(savedInstanceState);
    }

    private void recreateFragments(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (fragmentManager.getBackStackEntryCount() > 0) {// we have a search around direction fragment on top
                showSearchStationsInRadius();
            } else {
                showArrivalsFragment(null);
            }
        }
    }

    @Override
    public void onPresenterAttached() {

    }

    @Override
    public void onFirstTimeIntent() {
        if (recreated) // if recreated from savedInstance do nothing
            return;

        Intent intent = getIntent();
        String stationNameFromIntent = intent.getStringExtra(ARG_STATION_NAME);
        if (stationNameFromIntent != null) {
            showArrivalsFragment(stationNameFromIntent);
            intent.removeExtra(ARG_STATION_NAME);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_station, menu);
        initSearchView(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_station_near: {
                showSearchStationsInRadius();
                break;
            }
        }
        return true;
    }

    private void initSearchView(Menu menu) {
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        SearchView.SearchAutoComplete autoCompleteTextView = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
        autoCompleteTextView.setDropDownBackgroundResource(R.color.colorDefaultBackground);
        final CursorAdapter searchAdapter = getPresenter().getSearchAdapter();
        searchView.setSuggestionsAdapter(searchAdapter);
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                String stationName = getPresenter().getSearchedStationNameOrAliasAt((Cursor) searchAdapter.getItem(position));
                showArrivalsFragment(stationName);
                searchAdapter.getCursor().close();
                searchView.clearFocus();
                searchItem.collapseActionView();
                return false;
            }
        });
    }

    private void showArrivalsFragment(String stationName) {
        StationArrivalsFragment arrivalsFragment = (StationArrivalsFragment) fragmentManager.findFragmentByTag(StationArrivalsFragment.TAG);
        boolean created = false;
        if (arrivalsFragment == null) {
            created = true;
            arrivalsFragment = StationArrivalsFragment.create(stationName);
        }
        fragmentManager.popBackStack();
        if (created) {
            fragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_in_right, R.anim.slide_in_left, R.anim.slide_in_right)
                    .replace(R.id.stationFragmentContainer, arrivalsFragment, StationArrivalsFragment.TAG)
                    .commit();
        } else if (stationName != null) {
            arrivalsFragment.searchArrivalsByStationNameOrAlias(stationName);
        }
    }

    private void showSearchStationsInRadius() {
        StationNearFragment stationNearFragment = (StationNearFragment) fragmentManager.findFragmentByTag(StationNearFragment.TAG);
        boolean created = false;
        if (stationNearFragment == null) {
            created = true;
            stationNearFragment = new StationNearFragment();
        }
        if (created)
            fragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_in_right, R.anim.slide_in_right, R.anim.slide_in_left)
                    .replace(R.id.stationFragmentContainer, stationNearFragment, StationNearFragment.TAG)
                    .addToBackStack(null)
                    .commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onNearStationClicked(String name) {
        showArrivalsFragment(name);
    }
}
