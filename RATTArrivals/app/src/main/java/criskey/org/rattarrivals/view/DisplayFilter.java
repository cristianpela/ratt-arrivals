package criskey.org.rattarrivals.view;

import criskey.org.rattarrivals.R;

/**
 * Created by criskey on 8/1/2016.
 */
public enum DisplayFilter {

    ALL(R.string.all), BUS(R.string.bus), FAVORITE(R.string.favorite), TRAM(R.string.tram), TROLLEY(R.string.trolley);

    int stringResId;
    DisplayFilter(int stringResId){
        this.stringResId = stringResId;
    }

    public int getDisplayResId(){
        return stringResId;
    }

}
