package criskey.org.rattarrivals.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.mvp.BaseFragment;
import criskey.org.rattarrivals.util.BreakPoint;
import criskey.org.rattarrivals.util.Logger;

/**
 * Created by criskey on 6/1/2016.
 */
//TODO go for RecycleView instead of ListView.
// TODO: 4/4/2016 change how to load data in recycler view. to prevent any "no adapter attached" errors
public class MainListFragment extends BaseFragment<MainListFragmentPresenter> implements MainListFragmentView, ListTransportersRecyclerAdapter.ListTransportersCallback {

    public static final String FRAGMENT_TAG = MainListFragment.class.getSimpleName();

    public static final String ARG_TRANSPORTER = "arg_transporter";

    private RecyclerView transportersListView;

    private OnTransporterItemClick mOnTransporterItemClickCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mOnTransporterItemClickCallback = (OnTransporterItemClick) context;
    }

    @Override
    public void onDetach() {
        mOnTransporterItemClickCallback = null;
        super.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_transporter_layout, container, false);
        // transportersListView = (ListView) view.findViewById(R.id.listTransporters);
        transportersListView = (RecyclerView) view.findViewById(R.id.listTransporters);
        transportersListView.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    @Override
    protected void onArgumentsAttached(Bundle args) {
    }

    @Override
    public void displayTransporters(List<Transporter> transporterList) {
        transportersListView.setAdapter(new ListTransportersRecyclerAdapter(this, transporterList));
    }

    @Override
    public void transporterClick(Transporter transporter) {
        mOnTransporterItemClickCallback.onTransporterClickFromFragment(transporter);
    }

    @Override
    public void changeFavorite(Transporter transporter) {
        getPresenter().changeFavorite(transporter);
    }

    @Override
    public void onPresenterAttached() {
        Logger.log(this, "Presenter has been attached");
    }

    public void setSelectedDisplayFilter(DisplayFilter displayFilter) {
        if (getPresenter() != null)// this is null when the app is recreated after a low memory kill
            getPresenter().setSelectedDisplayFilter(displayFilter);
    }

}
