package criskey.org.rattarrivals.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by criskey on 11/3/2016.
 */
public final class Logger {

    private static volatile SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH);

    private Logger() {
    }

    public static void log(String message) {
        failSafeInitSdf();
        String time = sdf.format(new Date());
        System.out.println("[RATTApp " + time + "]: " + message);
    }

    public static void log(String tag, String message) {
        log(" " + tag + ": " + message);
    }

    public static void log(Class<?> clazz, String message) {
        log(clazz.getSimpleName(), message);
    }

    public static void log(Object object, String message) {
        log(object.getClass(), message);
    }

    private static void failSafeInitSdf() {
        SimpleDateFormat tmp = null;
        if (tmp == null && sdf == null)
            synchronized (Logger.class) {
                tmp = sdf;
                if (tmp == null) {
                    tmp = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH);
                    sdf = tmp;
                }

            }
    }

}
