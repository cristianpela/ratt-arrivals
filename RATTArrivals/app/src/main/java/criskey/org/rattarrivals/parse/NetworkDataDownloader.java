package criskey.org.rattarrivals.parse;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by criskey on 9/1/2016.
 */
public class NetworkDataDownloader implements AbstractNetworkDataDownloader {

    private static final String AGENT = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0";
    private static final String REFERRER = "http://www.google.com";
    private static final int TIMEOUT = 12000;
    private static final int HTTP_OK = 200;

    private String url;

    public NetworkDataDownloader(String url) {
        this.url = url;
    }

    @Override
    public Document load() throws ParsingException{
        try {
            Connection.Response response = Jsoup.connect(url)
                    .ignoreContentType(true)
                    .userAgent(AGENT)
                    .referrer(REFERRER)
                    .timeout(TIMEOUT)
                    .followRedirects(true)
                    .execute();
            if(response.statusCode() != HTTP_OK)
                 throw new ParsingException("Http error: ["+response.statusCode()+"]"+ response.statusMessage());
            return response.parse();
        } catch (IOException e) {
            throw new ParsingException("IO error: " + e.toString());
           // e.printStackTrace();
        }
       // return null;
    }


}

