package criskey.org.rattarrivals.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.util.Utils;

/**
 * Created by criskey on 5/2/2016.
 */
public class ListRouteStationRecyclerAdapter extends RecyclerView.Adapter<ListRouteStationRecyclerAdapter.ListRouteStationHolder> {

    private List<Station> stations;

    private WeakReference<OnStationClickCallback> onStationClickCallbackRef;

    public ListRouteStationRecyclerAdapter(OnStationClickCallback onStationClickCallback) {
        stations = new ArrayList<>();
        onStationClickCallbackRef = new WeakReference<OnStationClickCallback>(onStationClickCallback);
    }

    @Override
    public ListRouteStationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_display_route_station, parent, false);
        return new ListRouteStationHolder(view, onStationClickCallbackRef.get());
    }

    @Override
    public void onBindViewHolder(ListRouteStationHolder holder, int position) {
        Station station = stations.get(position);
        holder.bind(station);
    }

    @Override
    public int getItemCount() {
        return stations.size();
    }

    public void setStations(List<Station> stations){
        this.stations = stations;
        notifyDataSetChanged();
    }

    public static interface OnStationClickCallback {
        public void onStationClick(String stationName);
    }

    static class ListRouteStationHolder extends RecyclerView.ViewHolder {

        private final TextView mTextArrivalTime;
        private final TextView mTextStationName;
        private OnStationClickCallback onStationClickCallback;

        public ListRouteStationHolder(View itemView, OnStationClickCallback onStationClickCallback) {
            super(itemView);
            mTextStationName = (TextView) itemView.findViewById(R.id.textStationName);
            mTextArrivalTime = (TextView) itemView.findViewById(R.id.textArrivalTime);
            this.onStationClickCallback = onStationClickCallback;
        }

        public void bind(final Station station) {
            mTextStationName.setText(Utils.prettifyStationName(station.getName()));
            mTextArrivalTime.setText(station.getArrivals().get(0).getTime());
            mTextStationName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onStationClickCallback.onStationClick(station.getName());
                }
            });
        }
    }

}
