package criskey.org.rattarrivals.view.station;

import java.util.List;

import criskey.org.rattarrivals.mvp.WaitView;

/**
 * Created by criskey on 15/3/2016.
 */
public interface StationNearView extends WaitView {

    public void onShowStationsInRadius(List<StationNearPresenter.DistanceStation> stations, String address);

    public void onShowRadius(int meters);

    public void onSliderInitialState(int current, int max);

}
