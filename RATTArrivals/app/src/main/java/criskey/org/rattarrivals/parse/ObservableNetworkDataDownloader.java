package criskey.org.rattarrivals.parse;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.model.TransporterType;
import criskey.org.rattarrivals.util.Logger;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.Exceptions;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by criskey on 9/1/2016.
 */
//TODO better abstraction should be general
public final class ObservableNetworkDataDownloader {

    public static Observable<List<? extends Serializable>> getObservableNetworkData() {
        Observable<List<Transporter>> tramObservable = observableFactory(transpNetworkDataLoaderFactory(TransporterType.TRAM));
        Observable<List<Transporter>> trolObservable = observableFactory(transpNetworkDataLoaderFactory(TransporterType.TROLLEY));
        Observable<List<Transporter>> busObservable = observableFactory(transpNetworkDataLoaderFactory(TransporterType.BUS));
        return Observable.concat(tramObservable, busObservable, trolObservable, getObservableStationData(null));
    }

    public static Subscription getConcurrentSubscription(Observable<Object> observable, Observer<Object> observer) {
        return observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }



    public static <T> Subscription runInParallel(final Callee<T> callee, final Observer<T> observer) {
        Observable<T> observable = Observable.create(new Observable.OnSubscribe<T>() {
            @Override
            public void call(Subscriber<? super T> subscriber) {
                T called = callee.called();
                if (!subscriber.isUnsubscribed())
                    subscriber.onNext(called);
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        return observable.subscribe(observer);
    }

    public static <T> Subscription runInParallel(final Callee<T> callee, Action1<T>... actions) {
        if (actions != null && actions.length > 3)
            throw new IllegalArgumentException("There must be at most three actions (onNext, onError, onComplete) or none!");

        Observable<T> observable = Observable.create(new Observable.OnSubscribe<T>() {
            @Override
            public void call(Subscriber<? super T> subscriber) {
                T called = callee.called();
                if (!subscriber.isUnsubscribed())
                    subscriber.onNext(called);
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        if (actions == null) {
            return observable.subscribe();
        }

        //TODO: adapt this to accept on error and on complete subcribers
        Subscription subscription = null;
        switch (actions.length) {
            case 1:
                subscription = observable.subscribe(actions[0]);
                break;// onNext
            //case 2: subscription =  observable.subscribe(actions[0], actions[1]); break;// onNext
        }
        return subscription;
    }

    public static <T> Subscription runInParallel(final Callee<T> callee,
                                                 final Action1<T> onNext,
                                                 final Action1<Throwable> onError,
                                                 final Action0 onComplete) {
        Observable<T> observable = Observable.create(new Observable.OnSubscribe<T>() {
            @Override
            public void call(Subscriber<? super T> subscriber) {
                T called = callee.called();
                if (!subscriber.isUnsubscribed())
                    subscriber.onNext(called);
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        Subscription subscription = null;
        if (onNext == null && onComplete == null && onError == null) {
            subscription = observable.subscribe();
        } else if (onNext != null && onError != null && onComplete == null) {
            subscription = observable.subscribe(onNext, onError);
        } else if (onNext != null && onError != null && onComplete != null) {
            subscription = observable.subscribe(onNext, onError, onComplete);
        }
        return subscription;
    }

    public static <T> Subscription runInParallel(final Callee<T> callee) {
        return runInParallel(callee, null, null, null);
    }

    @NonNull
    public static Subscription getGeocodeSubscription(final String street,
                                                      Action1<Location> onNext,
                                                      Action1<Throwable> onError) {
        Callee<Location> callee = new Callee<Location>() {
            @Override
            public Location called() {
                return getLocationService(street);
            }
        };
        return runInParallel(callee, onNext, onError, null);
    }

    @Nullable
    public static Location getLocationService(String street) {
        HttpURLConnection conn = null;
        InputStream is = null;
        try {
            String fullStreet = street.trim() + ", Timisoara, Timis County, Romania";
            URL url = new URL("http://maps.google.com/maps/api/geocode/json?address="
                    + URLEncoder.encode(fullStreet, "UTF-8") + "&ka&sensor=false");
            conn = (HttpURLConnection) url.openConnection();
            conn.connect();

            is = conn.getInputStream();
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            StringBuilder responseBuilder = new StringBuilder();
            String line;
            while ((line = streamReader.readLine()) != null)
                responseBuilder.append(line);

            Logger.log(ObservableNetworkDataDownloader.class, responseBuilder.toString());
            JSONObject jsonObject = new JSONObject(responseBuilder.toString());
            JSONObject firstResult = ((JSONArray) jsonObject.get("results")).getJSONObject(0);
            String formattedAddress = firstResult.getString("formatted_address");
            Logger.log(formattedAddress);
            if(formattedAddress.equals("Timișoara, Romania")) { // returns the city name instead of street address
                return Location.EMPTY_LOCATION;
            }
            JSONObject jsonLocationObject = firstResult
                    .getJSONObject("geometry").getJSONObject("location");
            return new Location(jsonLocationObject.getDouble("lat"), jsonLocationObject.getDouble("lng"));
        } catch (Exception ex) {
            Exceptions.propagate(ex);
        } finally {
            if (conn != null) {
                InputStream errStream = conn.getErrorStream();
                if (errStream != null) {
                    try {
                        conn.getErrorStream().close();
                    } catch (IOException ex) {
                        Exceptions.propagate(ex);
                    }
                }
                conn.disconnect();
            }
            if (is != null)
                try {
                    is.close();
                } catch (IOException ex) {
                    Exceptions.propagate(ex);
                }
        }
        return null; // should not reach this
    }


    public static Observable<List<Station>> getObservableStationData(final StationsListingParser stationsListingParser) {
        return Observable.create(new Observable.OnSubscribe<List<Station>>() {
            @Override
            public void call(Subscriber<? super List<Station>> subscriber) {
                try {
                    StationsListingParser sp = stationsListingParser;
                    if (sp == null) {
                        sp = new StationsListingParser();
                    }
                    subscriber.onNext(sp.getStations());
                    if (!subscriber.isUnsubscribed())
                        subscriber.onCompleted();
                } catch (ParsingException ex) {
                    Exceptions.propagate(ex);
                    ex.printStackTrace();
                }
            }
        });
    }

    private static Observable<List<Transporter>> observableFactory(final TransporterParser transporterParser) {
        return Observable.create(new Observable.OnSubscribe<List<Transporter>>() {
            @Override
            public void call(Subscriber<? super List<Transporter>> subscriber) {
                try {
                    subscriber.onNext(transporterParser.getTransporters());
                    if (!subscriber.isUnsubscribed())
                        subscriber.onCompleted();
                } catch (ParsingException ex) {
                    Exceptions.propagate(ex);
                    ex.printStackTrace();
                }
            }
        });
    }

    private static TransporterParser transpNetworkDataLoaderFactory(TransporterType transporterType) {
        return new TransporterParser(new NetworkDataDownloader(transporterType.getUri().toString()), transporterType);
    }

    public static interface Callee<T> {
        public T called();
    }

}
