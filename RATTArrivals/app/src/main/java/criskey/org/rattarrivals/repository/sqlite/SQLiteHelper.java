package criskey.org.rattarrivals.repository.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static criskey.org.rattarrivals.repository.sqlite.Contract.*;

/**
 * Created by criskey on 8/2/2016.
 */
public class SQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "ratt.db";
    private static final int DATABASE_VERSION = 3;

    private static final String CREATE_STATION_SCRIPT = "CREATE TABLE " + Station.TITLE
            + "(" + Station._ID + " INTEGER PRIMARY KEY, "
            + Station.NAME + " TEXT NOT NULL COLLATE NOCASE, "
            + Station.ALIAS + " TEXT COLLATE NOCASE, "
            + Station.LATITUDE + " REAL,"
            + Station.LONGITUDE + " REAL);";

    private static final String CREATE_TRANSPORTER_SCRIPT = "CREATE TABLE " + Transporter.TITLE
            + "(" + Transporter._ID + " INTEGER PRIMARY KEY, "
            + Transporter.NAME + " TEXT NOT NULL COLLATE NOCASE,"
            + Transporter.LINK + " TEXT NOT NULL COLLATE NOCASE,"
            + Transporter.TYPE + " TEXT NOT NULL,"
            + Transporter.FAVORITE + " INTEGER NOT NULL);";


    private static final String CREATE_TRANSPORTER_STATION_JOIN_SCRIPT = "CREATE TABLE " +
            JoinStationTransporter.TITLE + "( " + JoinStationTransporter._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + JoinStationTransporter.FK_STATION + " INTEGER NOT NULL, "
            + JoinStationTransporter.FK_TRANSPORTER + " INTEGER NOT NULL, "
            + "FOREIGN KEY(" + JoinStationTransporter.FK_TRANSPORTER + ") REFERENCES " + Transporter.TITLE + "(" + Transporter._ID + ") ON DELETE CASCADE, "
            + "FOREIGN KEY(" + JoinStationTransporter.FK_STATION + ") REFERENCES " + Station.TITLE + "(" + Station._ID + ") ON DELETE CASCADE); ";

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_STATION_SCRIPT);
        db.execSQL(CREATE_TRANSPORTER_SCRIPT);
        db.execSQL(CREATE_TRANSPORTER_STATION_JOIN_SCRIPT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Station.TITLE);
        db.execSQL("DROP TABLE IF EXISTS " + Transporter.TITLE);
        db.execSQL("DROP TABLE IF EXISTS " + JoinStationTransporter.TITLE);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Station.TITLE);
        db.execSQL("DROP TABLE IF EXISTS " + Transporter.TITLE);
        db.execSQL("DROP TABLE IF EXISTS " + JoinStationTransporter.TITLE);
        onCreate(db);
    }
}
