package criskey.org.rattarrivals.model;

import java.io.Serializable;

import criskey.org.rattarrivals.parse.NetworkUtils;

/**
 * Created by criskey on 26/12/2015.
 */
//TODO make this Parcelable
public class Transporter implements Serializable {

    private static final long serialVersionUID = -9104847845488394989L;


    private TransporterType transporterType;
    private String line;
    private long id;
    private boolean favorite;

    private String arrivalLink;

    public Transporter(TransporterType transporterType,  String line, String link, long id, boolean favorite) {
        this.transporterType = transporterType;
        this.line = line;
        this.id = id;
        this.favorite = favorite;
        this.arrivalLink = link;
    }
    public Transporter(TransporterType transporterType, String line, long id, boolean favorite) {
         this(transporterType, line, null, id, favorite);
    }

    public Transporter(long id){
        this.id = id;
    }

    public TransporterType getTransporterType() {
        return transporterType;
    }

    public String getArrivalLink() {
        return arrivalLink;
    }

    public String getLine() {
        return line;
    }

    public long getId() {
        return id;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public void setTransporterType(TransporterType transporterType) {
        this.transporterType = transporterType;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transporter that = (Transporter) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return line;
    }
}
