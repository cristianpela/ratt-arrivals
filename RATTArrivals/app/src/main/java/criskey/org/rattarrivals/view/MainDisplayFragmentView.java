package criskey.org.rattarrivals.view;

import java.util.List;

import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.mvp.IView;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.mvp.WaitView;

/**
 * Created by criskey on 8/1/2016.
 */
public interface MainDisplayFragmentView extends WaitView {

    public void onDisplayRoute(List<Station> stations, String from, String to);

    public void onSettingCurrentLine(String line);

    public void onLastRefreshCheck(String ago);

    public void onNothingToShow();

    public void onShowHistory(List<Transporter> transporterHistory);

    /**
     * tracks which container is hosting this view (fragment)
     */
    public static final int LANDSCAPE_CONTAINER = 0;

    public static final int PORTRAIT_CONTAINER = 1;

}
