package criskey.org.rattarrivals.parse;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.model.TransporterType;

/**
 * Created by criskey on 14/1/2016.
 */
public class TransporterParser {

    protected TransporterType transporterType;

    private AbstractNetworkDataDownloader dataDownloader;

    public TransporterParser(AbstractNetworkDataDownloader dataDownloader, TransporterType transporterType) {
        this.dataDownloader = dataDownloader;
        this.transporterType = transporterType;
    }

    public List<Transporter> getTransporters() throws ParsingException {
        List<Transporter> transporters = new ArrayList<>();
        Elements links = getLinks();
        for (Element link : links) {
            Transporter transporter = getTransporterFromLink(link);
            if (transporter != null)
                transporters.add(transporter);
        }
        return transporters;
    }

    private Transporter getTransporterFromLink(Element link) throws ParsingException {
        //<a href="sens0.php?param1=1106" target="dreapta1" title="Linia 1" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image9','','tram1-1.png',1)">
        String[] idSplit = link.attr("onclick").split("parent.dreapta1.location.href=");
        if (idSplit.length == 2) {
            final String trail = idSplit[1]
                    .replaceAll("'", "")
                    .replace(";", "");
            if (trail.contains("param1")) {
                String param = trail.split("=")[1];
                String line = link.attr("title");
                return (line.equals("")) ? null : new Transporter(transporterType, line,
                        NetworkUtils.getTransporterLink(trail)
                        , Long.parseLong(param), false);
            }else{
                return null;
            }
        } else {
            return null;
        }
    }

    private Elements getLinks() throws ParsingException {
        Document document = dataDownloader.load();
        return document.select("a[href]");
    }
}
