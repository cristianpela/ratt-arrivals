package criskey.org.rattarrivals.model;

import java.io.Serializable;

/**
 * Created by criskey on 28/3/2016.
 */
public class Location implements Serializable {

    public static final Location EMPTY_LOCATION = new Location(0.0, 0.0);

    private static final long serialVersionUID = -6462617303453760862L;

    private double longitude;

    private double latitude;

    public Location(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public boolean isEmpty() {
        return this.equals(EMPTY_LOCATION);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        if (Double.compare(location.longitude, longitude) != 0) return false;
        return Double.compare(location.latitude, latitude) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(longitude);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return latitude + "," + longitude;
    }
}
