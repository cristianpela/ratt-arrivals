package criskey.org.rattarrivals.parse;

import criskey.org.rattarrivals.model.Arrival;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.model.Transporter;

/**
 * Created by criskey on 12/2/2016.
 */
public class ArrivalTimeParser extends Parser {

    private Station station;
    private Transporter transporter;

    public ArrivalTimeParser(AbstractNetworkDataDownloader downloader,
                             Station station, Transporter transporter) throws ParsingException {
        super(downloader);
        this.station = station;
        this.transporter = transporter;
    }

    public ArrivalTimeParser(Station station, Transporter transporter) throws ParsingException {
        super(NetworkUtils.getArrivalsTimeLink(station.getId(), "" + transporter.getId()));
        this.station = station;
        this.transporter = transporter;
    }

    public Arrival getArrivalTime(Arrival existingArrival) throws ParsingException {
        String raw = document.html();
        if (raw.indexOf("Error") != -1 || raw.indexOf("alert") != -1) {
            //TODO: internationalize this message
            throw new ParsingException("Error connecting to database.<br>" +
                    " Incercati sa obtineti timpii, accesand ruta completa <a href='ratt://main-link/'>aici</a>.");
        }
        String startRef = "Sosire1:";
        int start = raw.indexOf(startRef);
        start += startRef.length();

        String endRef = "<br>";
        int end = raw.lastIndexOf(endRef);
        // end = endRef.length();

        String time = raw.substring(start, end).trim();

        Arrival arrival = existingArrival;
        if (arrival != null){
            arrival.setTime(time);
        }else
            arrival = new Arrival(time, transporter);

        return arrival;
    }
}
