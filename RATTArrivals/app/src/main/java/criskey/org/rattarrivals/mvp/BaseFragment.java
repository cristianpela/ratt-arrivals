package criskey.org.rattarrivals.mvp;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import static criskey.org.rattarrivals.util.Logger.*;

/**
 * Created by criskey on 4/1/2016.
 */
public abstract class BaseFragment<P extends Presenter> extends Fragment implements IView {

    private static String ARG_ARE_ARGUMENTS_CHECKED = "ARG_ARE_ARGUMENTS_CHECKED";

    private boolean areArgumentsChecked = false;

    private VPBinder<? extends IView, P> mVPBinder;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        loggedCreateViewPresenterDelegate("Attempt to create view-presenter-delegate onAttach",
                "View-presenter-delegate already created onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loggedCreateViewPresenterDelegate("Attempt to create view-presenter-delegate onCreate",
                "View-presenter-delegate already created onCreate");
        if (savedInstanceState != null) {
            areArgumentsChecked = savedInstanceState.getBoolean(ARG_ARE_ARGUMENTS_CHECKED);
        }
    }

    @Override
    public void onResume() {
        loggedCreateViewPresenterDelegate("Attempt to create view-presenter-delegate onPause",
                "View-presenter-delegate already created onResume");
        bindPresenter();
        super.onResume();
    }

    private void loggedCreateViewPresenterDelegate(String message, String elseMessage) {
        if (mVPBinder == null) {
            log(this, message);
            createViewPresenterDelegate();
        } else if (elseMessage != null)
            log(this, elseMessage);
    }

    private void bindPresenter() {
        if (mVPBinder == null)
            log(this, " on bind the view-presenter-delegate is null. Return");
        else
            mVPBinder.bindView();
        Bundle args = getArguments();
        if (args != null && !areArgumentsChecked) {
            onArgumentsAttached(args);
            //ensure that callback is called only on fragment creation - as new instance
            areArgumentsChecked = true;
        }
        onPresenterAttached();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(ARG_ARE_ARGUMENTS_CHECKED, areArgumentsChecked);
    }

    protected abstract void onArgumentsAttached(Bundle args);

    @Override
    public void onPause() {
        if (mVPBinder == null)
            log(this, " on unbind the view-presenter-delegate is null");
        else
            mVPBinder.unbindView();
        super.onPause();
    }

    @Override
    public void onDetach() {
        log(this, " detaching the fragment");
        super.onDetach();
    }

    @Override
    public void onShowInfo(String infoMessage) {
        Toast.makeText(getActivity(), infoMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShowInfo(int resId) {
        Toast.makeText(getActivity(), resId, Toast.LENGTH_SHORT).show();
    }

    public P getPresenter() {
        return mVPBinder.getPresenter();
    }

    public abstract void onPresenterAttached();

    @Override
    public void onDestroy() {
        mVPBinder = null; // dereferencing to avoid leaks
        super.onDestroy();
    }

    private void createViewPresenterDelegate() {
        Context context = getActivity();
        if (context == null) {
            log(this, "fragment activity is null");
        } else {
            mVPBinder = new VPBinder<>(this);
            log(this, " view-presenter-delegate was created!");
        }
    }
}
