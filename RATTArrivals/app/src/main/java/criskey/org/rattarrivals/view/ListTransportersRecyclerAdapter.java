package criskey.org.rattarrivals.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.List;

import criskey.org.rattarrivals.MainActivity;
import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.util.Utils;

/**
 * Created by criskey on 4/2/2016.
 */
public class ListTransportersRecyclerAdapter extends RecyclerView.Adapter<ListTransportersRecyclerAdapter.TransportersHolder> {

    private WeakReference<ListTransportersCallback> onTransporterItemClickRef;
    private List<Transporter> transporterList;

    public ListTransportersRecyclerAdapter(ListTransportersCallback onTransporterItemClick, List<Transporter> transporterList) {
        this.onTransporterItemClickRef = new WeakReference<ListTransportersCallback>(onTransporterItemClick);
        this.transporterList = transporterList;
    }

    @Override
    public TransportersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_transporter_layout, parent, false);
        return new TransportersHolder(view);
    }

    @Override
    public void onBindViewHolder(TransportersHolder holder, int position) {
        final Transporter transporter = transporterList.get(position);
        holder.bind(transporter, onTransporterItemClickRef.get());
    }

    @Override
    public int getItemCount() {
        return transporterList.size();
    }

    static interface ListTransportersCallback {
        public void transporterClick(Transporter transporter);

        public void changeFavorite(Transporter transporter);
    }

    static class TransportersHolder extends RecyclerView.ViewHolder {

        ImageView imageTransporterType;
        ImageButton imageButtonFavorite;
        TextView textLine;

        public TransportersHolder(View itemView) {
            super(itemView);
            imageTransporterType = (ImageView) itemView.findViewById(R.id.imageTransporter);
            textLine = (TextView) itemView.findViewById(R.id.textLine);
            imageButtonFavorite = (ImageButton) itemView.findViewById(R.id.imageButtonFavorite);
        }

        public void bind(final Transporter transporter, final ListTransportersCallback onTransporterItemClick) {
            imageTransporterType.setImageResource(transporter.getTransporterType().getImageResId());
            imageButtonFavorite.setImageResource(getFavoriteResId(transporter));
            imageButtonFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ImageButton button = (ImageButton) v;
                    onTransporterItemClick.changeFavorite(transporter);
                    button.setImageResource(getFavoriteResId(transporter));
                }
            });
            String localizedLineText = Utils.insertOneTranslation(textLine.getContext(), transporter.getLine(), R.string.locale_line_text, "Linia");
            textLine.setText(transporter.getLine());
            textLine.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onTransporterItemClick.transporterClick(transporter);
                }
            });
        }

        private int getFavoriteResId(Transporter transporter) {
            return transporter.isFavorite() ? R.drawable.ic_star_black_24dp : R.drawable.ic_star_white_24dp;
        }

    }
}
