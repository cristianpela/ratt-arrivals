package criskey.org.rattarrivals.util;

/**
 * Simply serves as a debug point. Just put this in your code and add breakpoint near it.
 * Created by criskey on 29/3/2016.
 */
public final class BreakPoint {

    private BreakPoint(){}

    public static boolean breakInTheNameOfLove(){
        return false;
    }
}
