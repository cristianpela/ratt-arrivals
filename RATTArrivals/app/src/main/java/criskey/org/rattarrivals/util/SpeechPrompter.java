package criskey.org.rattarrivals.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import java.util.ArrayList;

import criskey.org.rattarrivals.R;

/**
 * Created by criskey on 19/3/2016.
 */
public final class SpeechPrompter {

    public static final int SPEECH_REQUEST_CODE = 773;

    private SpeechPrompter(){}

    public static void promptSpeechInputFromFragment(final Fragment fragment, int promptTitle) {
        promptSpeech(new StartActivityForResultAdapter() {
            @Override
            public void startActivityForResult(Intent intent, int requestCode) throws ActivityNotFoundException {
                fragment.startActivityForResult(intent, requestCode);
            }

            @Override
            public Context getContext() {
                return fragment.getActivity();
            }
        }, promptTitle);
    }

    public static void promptSpeechInputFromActivity(final Activity activity, int promptTitle) {
        promptSpeech(new StartActivityForResultAdapter() {
            @Override
            public void startActivityForResult(Intent intent, int requestCode) throws ActivityNotFoundException {
                activity.startActivityForResult(intent, requestCode);
            }

            @Override
            public Context getContext() {
                return activity;
            }
        }, promptTitle);
    }

    private static void promptSpeech(StartActivityForResultAdapter safrAdapter, @StringRes int promptTitle) {
        Context context = safrAdapter.getContext();
        Resources resources = context.getResources();

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
                .putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
                .putExtra(RecognizerIntent.EXTRA_LANGUAGE, Utils.LOCALE_RO)
                .putExtra(RecognizerIntent.EXTRA_PROMPT, resources.getString(promptTitle));
        try {
            safrAdapter.startActivityForResult(intent, SPEECH_REQUEST_CODE);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(context,
                    resources.getString(R.string.search_street_voice_app_error),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Only call this inside onActivityResult of activity or fragment and if the result code is
     * Activity.RESULT_OK and request code is SPEECH_REQUEST_CODE
     *
     * @param data intent with the processed speech
     * @return a list with matching words
     */
    @NonNull
    public static ArrayList<String> getConvertedSpeech(@NonNull Intent data) {
        return data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
    }

    public static String getConvertedSpeechTopResult(Intent data){
        return getConvertedSpeech(data).get(0);
    }

    private static interface StartActivityForResultAdapter {
        void startActivityForResult(Intent intent, int requestCode) throws ActivityNotFoundException;

        Context getContext();
    }

}
