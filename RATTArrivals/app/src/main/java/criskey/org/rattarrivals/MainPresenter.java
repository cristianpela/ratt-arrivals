package criskey.org.rattarrivals;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.model.Model;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.parse.ObservableNetworkDataDownloader;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.mvp.BasePresenter;
import criskey.org.rattarrivals.parse.RemoteDBBackup;
import criskey.org.rattarrivals.repository.PreferencesRepository;
import criskey.org.rattarrivals.repository.Repository;
import criskey.org.rattarrivals.repository.RepositoryManager;
import criskey.org.rattarrivals.repository.StationRepository;
import criskey.org.rattarrivals.repository.TransporterRepository;
import criskey.org.rattarrivals.repository.TransporterSQLRepository;
import criskey.org.rattarrivals.util.Logger;
import criskey.org.rattarrivals.view.DisplayFilter;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.Exceptions;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by criskey on 28/12/2015.
 */
public class MainPresenter extends BasePresenter<MainView> implements Observer<List<?>> {

    private static final long serialVersionUID = 5961279319198741078L;

    private Set<Transporter> transporterListAccumulator;

    private String errorMessage;

    private int currentLanguageIndex;

    public MainPresenter() {
    }

    @Override
    public void onBind() {
        if (isViewAttached()) {
            getPrefsRepository().makeCurrentLanguageAvailable();
            if (getPrefsRepository().hasData()) {
                getView().onLastDisplayFilter(getLastDisplayFilter());
            } else if (hasError()) {
                getView().onShowError(errorMessage);
            } else {
                downloadData();
            }
        }
    }

    private void downloadData() {
        if (isViewAttached())
            getView().onShowWait();
        ObservableNetworkDataDownloader.getObservableNetworkData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(this);
    }

    public String[] getAvailableLanguages() {
        return getPrefsRepository().getAvailableLanguages();
    }

    public void setCurrentLanguage(int languageIndex) {
        currentLanguageIndex = languageIndex;
    }

    public void saveCurrentLanguage() {
        if (currentLanguageIndex != -1) {
            getPrefsRepository().setCurrentLanguage(currentLanguageIndex);
            getPrefsRepository().makeCurrentLanguageAvailable();
        }
    }

    public int getCurrentLanguageIndex() {
        currentLanguageIndex = getPrefsRepository().getCurrentLanguage();
        return currentLanguageIndex;
    }

    private DisplayFilter getLastDisplayFilter() {
        String lastDisplayFilter = getPrefsRepository().getLastDisplayFilter();
        DisplayFilter displayFilter = DisplayFilter.ALL;
        if (lastDisplayFilter != null)
            displayFilter = DisplayFilter.valueOf(lastDisplayFilter);
        return displayFilter;
    }

    public void reset() {
        final Collection<Repository> all = getRepositoryManager().getAll();
        for (Repository repository : all) {
            repository.deleteAll();
        }
        onBind();
    }

    @Override
    public void onCompleted() {
        if (!hasError()) {
            //TODO : Make createMany to support Collections NOT jut Lists
            getTransporterRepository().createMany(new LinkedList<>(transporterListAccumulator));
            getPrefsRepository().validateData(true);
            if (isViewAttached()) {
                getView().onHideWait();
            }
        }
    }

    @Override
    public void onError(Throwable e) {
        errorMessage = e.getMessage();
        if (isViewAttached())
            getView().onShowError(errorMessage);
    }

    @Override
    public void onNext(List<?> parsedData) {
        Class elemClass = parsedData.get(0).getClass();
        if (elemClass.equals(Transporter.class)) {
            if (transporterListAccumulator == null) {
                transporterListAccumulator = new HashSet<>();
            }
            getView().onShowProgress(R.string.saving_transporter_data);
            transporterListAccumulator.addAll((List<Transporter>) parsedData);
        } else if (elemClass.equals(Station.class)) {
            getStationRepository().createMany((List<Station>) parsedData);
            createManyOnRemote((List<Station>) parsedData);
            getView().onShowProgress(R.string.saving_station_data);
        } else {
            getView().onShowProgress(R.string.create_rmtdb);
        }
    }

    private void createManyOnRemote(final List<Station> entities) {

//        ObservableNetworkDataDownloader.Callee<Void> callee = new ObservableNetworkDataDownloader.Callee<Void>() {
//            @Override
//            public Void called() {
//                final RemoteDBBackup remoteDb = new RemoteDBBackup();
//                remoteDb.execute(new RemoteDBBackup.Query() {
//                    @Override
//                    public void exec() throws IOException, JSONException {
//                        Logger.log("Check for remote database...");
//                        if (remoteDb.isDatabaseEmpty()) {
//                            Logger.log("Remote database is empty! Populating remote database...");
//                            remoteDb.insertBulkLocations(entities);
//                        }
//                    }
//
//                    @Override
//                    public void onError(Exception e) {
//                        Exceptions.propagate(e);
//                        e.printStackTrace();
//                    }
//                });
//                return null;
//            }
//        };
//
//        ObservableNetworkDataDownloader.runInParallel(callee);
    }

    @Override
    public void retryOnError() {
        downloadData();
    }

    private boolean hasError() {
        return errorMessage != null;
    }


    private TransporterRepository getTransporterRepository() {
        return (TransporterRepository) getRepositoryManager().getRepository(RepositoryManager.TRANSPORTER_REPOSITORY);
    }

    private PreferencesRepository getPrefsRepository() {
        return (PreferencesRepository) getRepositoryManager().getRepository(RepositoryManager.PREFERENCES_REPOSITORY);
    }

    private StationRepository getStationRepository() {
        return (StationRepository) getRepositoryManager().getRepository(RepositoryManager.STATION_REPOSITORY);
    }

}