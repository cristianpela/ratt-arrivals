package criskey.org.rattarrivals.mvp;

import android.support.annotation.StringRes;

/**
 * Created by criskey on 28/3/2016.
 */
public interface Wait {

    public void onShowWait();

    public void onHideWait();

    public void onShowProgress(@StringRes int res);

    public void onShowError(String errorMessage);
}
