package criskey.org.rattarrivals.view.station;

import java.util.List;

import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.mvp.IView;

/**
 * Created by criskey on 2/3/2016.
 */
public interface StationAliasView extends IView {


    public void onShowStationsUnderAlias(List<Station> stations);

    public void onShowStationFilteredStations(List<Station> stations);

}
