package criskey.org.rattarrivals.parse;

import android.net.Uri;
import android.support.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collection;

import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.util.Logger;

/**
 * Service to save and load station locations remotely. Right now it using mlabs api directly.
 * In the future should use an rest server as middleware<br/>
 * <p/>
 * https://api.mlab.com/api/1/databases/my-db/collections/my-coll?apiKey=myAPIKey <br>
 * https://api.mlab.com/api/1/databases/my-db/collections/my-coll?q={"active": true}&apiKey=myAPIKey <br>
 * <p/>
 * <a href="http://docs.mlab.com/data-api">Documentation for API</a><br>
 * Created by criskey on 20/6/2016.
 */
public class RemoteDBBackup {

    private static final String API_KEY = "AmZ1n8J7zFa8Tqrv4REqLp-318aps9vQ";
    /**
     * q=<query> - restrict results by the specified JSON query <br>
     * Example:  'https://api.mlab.com/api/1/databases/my-db/collections/my-coll?apiKey=myAPIKey&q={"_id":1234}'
     */
    private static final String QUERY_PARAM = "q";

    /**
     * f=<set of fields> - specify the set of fields to include or exclude in each document (1 - include; 0 - exclude)<br>
     * Example: https://api.mlab.com/api/1/databases/my-db/collections/my-coll?f={"notes": 0}&apiKey=myAPIKey
     */
    private static final String FILTER_PARAM = "f";

    /**
     * "c" example - return the count of documents with "active" of true:
     * https://api.mlab.com/api/1/databases/my-db/collections/my-coll?q={"active": true}&c=true&apiKey=myAPIKey
     */
    private static final String COUNT_PARAM = "c";

    /**
     * u=true - “upsert”: insert the document defined in the request body if none match the specified query<br>
     * So, you can think of PUT like “update”; with u=true added, it becomes “update or insert”, or “upsert” for short.
     */
    private static final String UPSERT_FLAG_PARAM = "u";

    /**
     * s=<sort order> - specify the order in which to sort each specified field (1- ascending; -1 - descending)<br>
     * "s" example - return all documents sorted by "priority" ascending and "difficulty" descending:
     * https://api.mlab.com/api/1/databases/my-db/collections/my-coll?s={"priority": 1, "difficulty": -1}&apiKey=myAPIKey
     */
    private static final String SORT_PARAM = "s";

    /**
     * sk=<num results to skip> - specify the number of results to skip in the result set; useful for paging<br>
     */
    private static final String PAGE_SKIP_PARAM = "sk";

    /**
     * l=<limit> - specify the limit for the number of results (default is 1000)
     */
    private static final String PAGE_ITEMS_PARAM = "l";


    private static final String POST = "POST";
    private static final String PUT = "PUT";
    private static final String GET = "GET";

    private Uri baseUri;

    private HttpURLConnection conn;
    private OutputStream os;
    private BufferedWriter writer;
    private InputStream in;
    private BufferedReader reader;

    public RemoteDBBackup(String baseUrl, String apiKey) {
        baseUri = Uri.parse(baseUrl).buildUpon().appendQueryParameter("apiKey", apiKey).build();
        //baseUri = Uri.parse("https://api.bitbucket.org/2.0/repositories/cristianpela");
    }

    public RemoteDBBackup() {
        this("https://api.mlab.com/api/1/databases/rattarrivals/collections/locations", API_KEY);
    }

    /**
     * Check if database is empty. Sends a "count" query
     *
     * @return if number of records is 0
     * @throws IOException
     * @throws JSONException
     */
    public boolean isDatabaseEmpty() throws IOException, JSONException {
        Uri countUri = baseUri.buildUpon().appendQueryParameter(COUNT_PARAM, "true").build();
        String response = sendHttpRequest(countUri, GET, null);
        return Integer.parseInt(response) == 0;
    }

    /**
     * Insert multiple station location. Use this when application is running for the first time and
     * make sure that db is empty.
     *
     * @param stations
     * @throws IOException
     * @throws JSONException
     */
    public void insertBulkLocations(Collection<Station> stations) throws IOException, JSONException {
        JSONArray jsonData = new JSONArray();
        for (Station station : stations) {
            JSONObject stationJSON = createStationJSONObject(station);
            jsonData.put(stationJSON);
        }
        String response = sendHttpRequest(baseUri, POST, jsonData.toString());
    }

    /**
     * Upsert method for remote api mongo db
     *
     * @param station
     * @throws IOException
     */
    public JSONObject upsertLocation(Station station) throws IOException, JSONException {
        String id = station.getId();
        JSONObject jsonData = new JSONObject();
        JSONObject $setObject = createStationJSONObject(station);
        jsonData.put("$set", $setObject);
        Uri putUrl = baseUri
                .buildUpon()
                .appendQueryParameter(QUERY_PARAM, whereId(id))
                .appendQueryParameter(UPSERT_FLAG_PARAM, "true")
                .build();
        String response = sendHttpRequest(putUrl, PUT, jsonData.toString());
        return new JSONObject(response);
    }

    private JSONObject createStationJSONObject(Station station) throws JSONException {
        Location location = (station.getLocation() == null) ? Location.EMPTY_LOCATION : station.getLocation();
        return new JSONObject()
                .put("station_id", station.getId())
                .put("name", station.getName())
                .put("latitude", location.getLatitude())
                .put("longitude", location.getLongitude());
    }

    /**
     * Returns a saved station location from remote mongo db
     *
     * @param stationId
     * @return
     * @throws IOException
     */
    public JSONObject getLocation(String stationId) throws IOException, JSONException {
        //exclude document id field
        JSONObject filterObject = new JSONObject()
                .put("_id", 0);
        Uri getUri = baseUri
                .buildUpon()
                .appendQueryParameter(QUERY_PARAM, whereId(stationId))
                .appendQueryParameter(FILTER_PARAM, filterObject.toString())
                .appendQueryParameter(UPSERT_FLAG_PARAM, "true")
                .build();
        JSONArray responseArray = new JSONArray(sendHttpRequest(getUri, GET, null));
        if (responseArray.length() == 0)
            throw new JSONException("No location was found for station id : " + stationId);
        return responseArray.getJSONObject(0);
    }

    @NonNull
    private String whereId(String id) {
        return "{\"station_id\":\"" + id + "\"}";
    }

    private String sendHttpRequest(Uri uri, String method, String jsonData) throws IOException, JSONException {
        //set http-headers and open connection
        conn = (HttpURLConnection) ((new URL(uri.toString()).openConnection()));
        conn.setDoInput(true);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestProperty("Accept-Charset", "UTF-8");
        conn.setRequestMethod(method);
        //write body request
        if (method.equals(POST) || method.equals(PUT)) {
            conn.setDoOutput(true);
            os = conn.getOutputStream();
            writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(jsonData);
            writer.close();
            os.close();
        }
        //response
        int code = conn.getResponseCode();
        if (code != HttpURLConnection.HTTP_OK) {
            in = conn.getErrorStream();
        } else
            in = conn.getInputStream();
        reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder response = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            response.append(line);
        }
        return response.toString();
    }


    public void close() throws IOException {
        if (conn != null)
            conn.disconnect();
        if (in != null) {
            in.close();
        }
        if (reader != null) {
            reader.close();
        }
    }

    public void execute(Query query) {
        try {
            query.exec();
        } catch (Exception e) {
            query.onError(e);
            e.printStackTrace();
        } finally {
            try {
                close();
            } catch (IOException e) {
                query.onError(e);
                e.printStackTrace();
            }
        }
    }

    public static interface Query {
        public void exec() throws IOException, JSONException;

        public void onError(Exception e);
    }
}
