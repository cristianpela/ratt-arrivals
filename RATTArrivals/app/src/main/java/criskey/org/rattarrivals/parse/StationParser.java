package criskey.org.rattarrivals.parse;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashSet;
import java.util.Set;

import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.model.Transporter;

/**
 * Created by criskey on 12/2/2016.
 */
public class StationParser extends Parser {

    private Station station;

    public StationParser(AbstractNetworkDataDownloader downloader, Station bareStation) throws ParsingException {
        super(downloader);
        this.station = bareStation;
    }

    /**
     * Bare minimum station is a station with an id and name only;
     *
     * @param bareStation
     */
    public StationParser(Station bareStation) throws ParsingException {
        super(NetworkUtils.getSingleStationLink(bareStation.getId()));
        this.station = bareStation;
    }

    public Station getStation() throws ParsingException {
        Elements elements = document.select("option");
        Set<Transporter> transporters = null;
        for (Element e : elements) {
            if (transporters == null)
                transporters = new HashSet<>();
            transporters.add(new Transporter(Long.parseLong(e.attr("value"))));
        }
        if (transporters == null)
            throw new ParsingException(
                    "There are no transporter routes associated with this station! [" + document.text()+"]");
        station.getTransporters().addAll(transporters);
        return station;
    }
}
