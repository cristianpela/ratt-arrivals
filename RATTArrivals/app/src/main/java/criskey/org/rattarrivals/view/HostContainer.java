package criskey.org.rattarrivals.view;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;

/**
 * Created by criskey on 25/1/2016.
 */
@IntDef({MainDisplayFragmentView.LANDSCAPE_CONTAINER, MainDisplayFragmentView.PORTRAIT_CONTAINER})
public @interface HostContainer {
}
