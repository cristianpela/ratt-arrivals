package criskey.org.rattarrivals.view.map;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaOptions;
import com.google.android.gms.maps.StreetViewPanoramaView;
import com.google.android.gms.maps.model.LatLng;

import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.util.AppDialog;
import criskey.org.rattarrivals.view.station.StationArrivalsFragment;

import android.view.ViewGroup.LayoutParams;

/**
 * Created by criskey on 8/3/2016.
 */
public class StationStreetViewDialog extends AppDialog implements OnStreetViewPanoramaReadyCallback {

    public static final String TAG = StationStreetViewDialog.class.getSimpleName();
    private static final String LOCATION_KEY = "LOCATION_KEY";
    private StreetViewPanoramaView mPanorama;

    public static StationStreetViewDialog newInstance(LatLng location){
        Bundle args = new Bundle();
        args.putParcelable(LOCATION_KEY, location);
        StationStreetViewDialog instance = new StationStreetViewDialog();
        instance.setArguments(args);
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.station_frag_street_view, container, false);
        FrameLayout frame = (FrameLayout) view.findViewById(R.id.street_view_container);
        frame.addView(mPanorama, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        return view;
    }


    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        //streetViewPanorama.setPosition(new LatLng(-33.87365, 151.20689));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        StreetViewPanoramaOptions options = new StreetViewPanoramaOptions();
        if (savedInstanceState == null) {
            LatLng location = (LatLng) getArguments().get(LOCATION_KEY);
            options.position(location);
        }
        mPanorama = new StreetViewPanoramaView(getActivity(), options);
        mPanorama.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, android.R.style.Theme_Holo_DialogWhenLarge);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        mPanorama.onPause();
        super.onPause();
    }

    @Override
    public void onLowMemory() {
        mPanorama.onLowMemory();
        super.onLowMemory();
    }

    @Override
    public void onResume() {
        mPanorama.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        mPanorama.onDestroy();
        mPanorama = null;
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mPanorama.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    public void setLocation(final LatLng latLng) {
        mPanorama.getStreetViewPanoramaAsync(new OnStreetViewPanoramaReadyCallback() {
            @Override
            public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
                streetViewPanorama.setPosition(latLng);
            }
        });
    }
}
