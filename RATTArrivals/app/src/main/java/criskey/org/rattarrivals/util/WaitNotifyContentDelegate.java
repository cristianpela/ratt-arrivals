package criskey.org.rattarrivals.util;

import android.app.Activity;
import android.content.res.Resources;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.mvp.Wait;

/**
 * Utility class to show/hide a panel while data is processed on a background thread<br>
 * Ids to provide:
 * <ul>
 * <li>Main panel id, the one that is hidden while processing(default is R.id.contentMain).<br>
 * See {@link ResIdConfig}
 * </li>
 * <li>
 * In XML include the wait panel R.layout.wait_notify_layout. Then provide for the "include"
 * tag an id (default is expected  R.id.waitNotifyLayout)
 * </li>
 * </ul>
 * Created by criskey on 25/1/2016.
 */
public class WaitNotifyContentDelegate implements Wait {

    private ProgressBar mProgressBar;
    private TextView mTextMessage;
    private Button mButtonRetry;
    private Button mButtonCancel;

    private View mWaitContainer;
    private View mMainContainer;

    private Adapter baseViewAdapter;

    public WaitNotifyContentDelegate(Adapter baseViewAdapter, ResIdConfig config) {

        this.baseViewAdapter = baseViewAdapter;
        mWaitContainer = baseViewAdapter.adaptFindViewById(config.waitNotifyLayoutLayoutId);
        mMainContainer = baseViewAdapter.adaptFindViewById(config.mainContentLayoutId);

        mProgressBar = (ProgressBar) baseViewAdapter.adaptFindViewById(config.progressBarId);
        mTextMessage = (TextView) baseViewAdapter.adaptFindViewById(config.messageId);
        mTextMessage.setMovementMethod(LinkMovementMethod.getInstance());
        mButtonRetry = (Button) baseViewAdapter.adaptFindViewById(config.retryButtonId);
        mButtonCancel = (Button) baseViewAdapter.adaptFindViewById(config.cancelButtonId);

        setActionButtonsListeners();
        onHideWait();
    }

    public WaitNotifyContentDelegate(Adapter baseViewAdapter) {
        this(baseViewAdapter, ResIdConfig.getDefaultConfig());
    }



    @Override
    public void onShowWait() {
        mMainContainer.setVisibility(View.INVISIBLE);
        mWaitContainer.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
        setActionButtonsVisibility(View.GONE);
        setMessage(baseViewAdapter.adaptResources().getString(R.string.message_wait));
    }


    @Override
    public void onHideWait() {
        mMainContainer.setVisibility(View.VISIBLE);
        mWaitContainer.setVisibility(View.GONE);
    }

    @Override
    public void onShowError(String message) {
        mMainContainer.setVisibility(View.INVISIBLE);
        mWaitContainer.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        setMessage(message);
        setActionButtonsVisibility(View.VISIBLE);
    }

    @Override
    public void onShowProgress(@StringRes int res) {
        setMessageStringRes(res);
    }

    public void setMessage(String message) {
        mTextMessage.setText(Html.fromHtml(message));
    }

    public void setMessageStringRes(@StringRes int message) {
        mTextMessage.setText(message);
    }

    private void setActionButtonsListeners() {
        mButtonRetry.setOnClickListener(baseViewAdapter.adaptOnRetryListener());
        mButtonCancel.setOnClickListener(baseViewAdapter.adaptOnCancelListener());

    }

    private void setActionButtonsVisibility(int visibility) {
        mButtonRetry.setVisibility(visibility);
        mButtonCancel.setVisibility(visibility);
    }

    public void destroy() {
        mProgressBar = null;
        mTextMessage = null;
        mButtonRetry = null;
        mButtonCancel = null;
        mWaitContainer = null;
        mMainContainer = null;
        baseViewAdapter = null;
    }


    public static interface Adapter {

        public View adaptFindViewById(@IdRes int id);

        public Resources adaptResources();

        public View.OnClickListener adaptOnRetryListener();

        public View.OnClickListener adaptOnCancelListener();

    }

    public static abstract class AdapterStrategy implements Adapter {

        private AdapterStrategy() {
        }

        public static Adapter activityAdapter(final Activity activity,
                                              final View.OnClickListener retryListener,
                                              final View.OnClickListener cancelListener) {
            return new AdapterStrategy() {
                @Override
                public View adaptFindViewById(@IdRes int id) {
                    return activity.findViewById(id);
                }

                @Override
                public Resources adaptResources() {
                    return activity.getResources();
                }

                @Override
                public View.OnClickListener adaptOnRetryListener() {
                    return retryListener;
                }

                @Override
                public View.OnClickListener adaptOnCancelListener() {
                    return cancelListener;
                }
            };
        }

        public static Adapter fragmentAdapter(Fragment fragment,
                                              View.OnClickListener retryListener,
                                              View.OnClickListener cancelListener) {
            return activityAdapter(fragment.getActivity(), retryListener, cancelListener);
        }
    }

    public static final class ResIdConfig {
        final int mainContentLayoutId;
        final int waitNotifyLayoutLayoutId;
        final int progressBarId;
        final int messageId;
        final int retryButtonId;
        final int cancelButtonId;

        public ResIdConfig(@IdRes int mainContentLayoutId,
                           @IdRes int waitNotifyLayoutLayoutId,
                           @IdRes int progressBarId,
                           @IdRes int messageId,
                           @IdRes int retryButtonId,
                           @IdRes int cancelButtonId) {
            this.mainContentLayoutId = mainContentLayoutId;
            this.waitNotifyLayoutLayoutId = waitNotifyLayoutLayoutId;
            this.messageId = messageId;
            this.progressBarId = progressBarId;
            this.retryButtonId = retryButtonId;
            this.cancelButtonId = cancelButtonId;
        }

        public static ResIdConfig getDefaultConfig() {
            return new ResIdConfig(R.id.contentMain, R.id.waitNotifyLayout,
                    R.id.progressDownloadData, R.id.textMessage, R.id.buttonRetry, R.id.buttonCancel);
        }

        public static final class Builder {
            private int mainContentLayoutId;
            private int waitNotifyLayoutId;
            private int progressBarId;
            private int messageId;
            private int retryButtonId;
            private int cancelButtonId;

            public Builder() {
                ResIdConfig defaultConfig = ResIdConfig.getDefaultConfig();
                mainContentLayoutId = defaultConfig.mainContentLayoutId;
                waitNotifyLayoutId = defaultConfig.waitNotifyLayoutLayoutId;
                progressBarId = defaultConfig.progressBarId;
                messageId = defaultConfig.messageId;
                retryButtonId = defaultConfig.retryButtonId;
                cancelButtonId = defaultConfig.cancelButtonId;
            }

            /**
             * Default is R.id.contentMain
             * @param mainContentLayoutId
             * @return
             */
            public Builder mainContentLayoutId(@IdRes int mainContentLayoutId) {
                this.mainContentLayoutId = mainContentLayoutId;
                return this;
            }

            public Builder waitNotifyLayoutId(@IdRes int waitNotifyLayoutLayoutId) {
                this.mainContentLayoutId = waitNotifyLayoutLayoutId;
                return this;
            }

            public Builder progressBarId(@IdRes int progressBarId) {
                this.progressBarId = progressBarId;
                return this;
            }

            public Builder messageId(@IdRes int messageId) {
                this.messageId = messageId;
                return this;
            }

            public Builder retryButtonId(@IdRes int retryButtonId) {
                this.retryButtonId = retryButtonId;
                return this;
            }

            public Builder cancelButtonId(@IdRes int cancelButtonId) {
                this.cancelButtonId = cancelButtonId;
                return this;
            }

            public ResIdConfig build() {
                return new ResIdConfig(mainContentLayoutId, waitNotifyLayoutId, progressBarId,
                        messageId, retryButtonId, cancelButtonId);
            }
        }
    }

}
