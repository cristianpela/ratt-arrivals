package criskey.org.rattarrivals.util;

import android.content.Context;
import android.support.annotation.StringRes;

/**
 * Utility class to process exceptions carrying resource id messages. Useful for app localization
 * Created by criskey on 29/3/2016.
 */
public final class ExceptionsResID {

    private ExceptionsResID() {

    }

    /**
     * Creates a throwable which wraps a resource ID message instead of a plain String.
     * @param resId
     * @return
     */
    public static Throwable throwID(@StringRes int resId) {
        return new Throwable("" + resId);
    }

    /**
     * Extracts a string resource id from a throwable message
     * @param e
     * @return
     * @throws NumberFormatException
     */
    public static int catchID(Throwable e) throws NumberFormatException {
        String resId = e.getMessage().split(":")[1].trim();
        return Integer.parseInt(resId);
    }
}
