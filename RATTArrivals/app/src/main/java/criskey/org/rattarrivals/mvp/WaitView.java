package criskey.org.rattarrivals.mvp;

import android.support.annotation.StringRes;

import criskey.org.rattarrivals.mvp.IView;

/**Just a simple composite interface
 * Created by criskey on 25/2/2016.
 */
public interface WaitView extends IView, Wait {

}
