package criskey.org.rattarrivals.parse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by criskey on 19/1/2016.
 */
public class ResourceStreamDataDownloader  implements AbstractNetworkDataDownloader {

    // Since base Uri is required, provide a placeholder instead;
    private static final String BASE_URI_PLACEHOLDER = "http://localhost/";

    private InputStream in;

    public ResourceStreamDataDownloader(InputStream in) {
        this.in = in;
    }

    @Override
    public Document load() throws ParsingException {
        try {
            return Jsoup.parse(in, "UTF-8", BASE_URI_PLACEHOLDER);
        } catch (IOException e) {
            throw new ParsingException(e);
        }
    }
}
