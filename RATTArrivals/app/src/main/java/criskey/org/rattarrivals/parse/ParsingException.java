package criskey.org.rattarrivals.parse;

/**
 * Created by criskey on 17/1/2016.
 */
public class ParsingException extends RuntimeException {

    public ParsingException() {
    }

    public ParsingException(String detailMessage) {
        super(detailMessage);
    }

    public ParsingException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ParsingException(Throwable throwable) {
        super(throwable);
    }
}
