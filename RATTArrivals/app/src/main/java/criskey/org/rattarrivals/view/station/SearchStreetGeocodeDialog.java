package criskey.org.rattarrivals.view.station;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.util.AppDialog;
import criskey.org.rattarrivals.util.SpeechPrompter;

/**
 * Created by criskey on 7/3/2016.
 */
public class SearchStreetGeocodeDialog extends AppDialog<StationArrivalsPresenter> {

    public static final String DIALOG_TITLE_KEY = "DIALOG_TITLE_KEY";

    private TextView mTextFoundGeocode;
    private EditText mEditSearch;

    public static void hideKeyboardFrom(Context context, View view) {
        //InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        // imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        //imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    public static SearchStreetGeocodeDialog newInstance(String title) {
        Bundle bundle = new Bundle();
        bundle.putString(DIALOG_TITLE_KEY, title);
        SearchStreetGeocodeDialog dialog = new SearchStreetGeocodeDialog();
        dialog.setArguments(bundle);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.street_search_geocode_dialog, container, false);

        mTextFoundGeocode = (TextView) view.findViewById(R.id.textFoundGeocode);
        mEditSearch = (EditText) view.findViewById(R.id.editSearchGeocode);
        ImageButton buttonSearch = (ImageButton) view.findViewById(R.id.imageButtonSearchGeocode);
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String street = mEditSearch.getText().toString();
                if (TextUtils.isEmpty(street)) {
                    mEditSearch.setError(getResources().getString(R.string.search_street_empty_error));
                } else {
                    pvPresenter.searchGeocode(street);
                }

            }
        });

        ImageButton buttonSpeech = (ImageButton) view.findViewById(R.id.imageButtonSpeech);
        buttonSpeech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SpeechPrompter.promptSpeechInputFromFragment(SearchStreetGeocodeDialog.this, R.string.search_street_voice_prompt_title);
            }
        });

        Button buttonConfirm = (Button) view.findViewById(R.id.buttonOK);
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String street = mEditSearch.getText().toString();
                String location = mTextFoundGeocode.getText().toString();
                if (TextUtils.isEmpty(street)) {
                    mEditSearch.setError(getResources().getString(R.string.search_street_empty_error));
                } else if (TextUtils.isEmpty(location)) {
                    Toast.makeText(v.getContext(), R.string.search_geocode_empty_error, Toast.LENGTH_SHORT).show();
                } else {
                    hideKeyboardFrom(getActivity(), mEditSearch);
                    pvPresenter.openMapActivity(mTextFoundGeocode.getText().toString());
                    pvPresenter = null;
                }
            }
        });

        enableWaitLayer();
        String street = getArguments().getString(DIALOG_TITLE_KEY);
        getDialog().setTitle(street);
        mEditSearch.setText(street);

        return view;
    }

    public void onFoundLocation(String formattedLocation) {
        mTextFoundGeocode.setText(formattedLocation);
        onHideWait();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == SpeechPrompter.SPEECH_REQUEST_CODE && data != null) {
            String street = SpeechPrompter.getConvertedSpeechTopResult(data);
            mEditSearch.setText(street);
        }
    }

    @Override
    public View.OnClickListener adaptOnRetryListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pvPresenter.retrySearchGeocode();
            }
        };
    }
}
