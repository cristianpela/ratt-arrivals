package criskey.org.rattarrivals.repository;

import java.util.List;

import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.model.Transporter;

/**
 * Created by criskey on 12/2/2016.
 */
public interface StationRepository extends Repository<String, Station> {

    /**
     * Find a station/or stations by name. Unfortunately there are stations with different ID but same name;
     * @param name
     * @return list of stations
     */
    public List<Station> findAllByNameOrAlias(String name);

    /**
     * Find stations by "LIKE" name or alias. The stations must be "joined"
     *(have transporters associated with them)
     * <br>
     *
     * Using LIKE ?% will generate a bind or column index out of range
     * <br>
     * See this bug report.
     * <a href="https://code.google.com/p/android/issues/detail?id=3153">Issue 3135</a><br>
     * See more:
     * <a href="https://www.sqlite.org/c3ref/bind_blob.html">SQLite doc</a>
     * @param hint for station name or alias
     * @return found stations
     */
    public List<Station> findAllStationsLike(String hint);

    /**
     * Finds all distinct aliases in station table
     * @return list(set) of aliases
     */
    public List<String> findAllAliases();

    /**
     * Find a station in the many-to-many table transporter_stations.
     * The Query itself will return a station with all transporters in that station. with no additional data about station
     * So the given station parameter must pe "filled" with these transporters.<br>
     * Do not create a new station instance to return
     * @param station
     * @return
     */
    public Station findJoinedStation(Station station);

    /**
     * Create an entry in the  many - to - many table between Station and Transpoter table
     * <br> See {@link criskey.org.rattarrivals.repository.sqlite.Contract}
     *
     * @param station
     * @param transporter
     * @return
     */
    public boolean createJoinTransporterStation(Station station, Transporter transporter);

    /**
     * Check if the station has transporters associated with it
     * @param station
     * @return
     */
    public boolean isJoined(Station station);

    /**
     * Updates station's location
     * @param station
     * @param location
     * @return
     */
    public boolean updateLocation(Station station, Location location);


    /**
     * Updates station's alias (user's given name)
     * @param station
     * @param alias
     * @return
     */
    public boolean updateAlias(Station station, String alias);
}
