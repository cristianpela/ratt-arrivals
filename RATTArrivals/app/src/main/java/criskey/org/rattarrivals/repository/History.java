package criskey.org.rattarrivals.repository;

import java.util.List;
import java.util.Set;

/**
 * Created by criskey on 1/2/2016.
 */
public interface History<E> {

    public static final int DEFAULT_CAPACITY = 5;

    public void push(E entry);

    public int getCapacity();

    public void setCapacity(int capacity);

    public List<E> getEntries();
}
