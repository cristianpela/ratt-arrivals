package criskey.org.rattarrivals.view;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.mvp.BasePresenter;
import criskey.org.rattarrivals.model.Route;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.parse.RouteParser;
import criskey.org.rattarrivals.repository.RepositoryManager;
import criskey.org.rattarrivals.repository.TransporterRepository;
import criskey.org.rattarrivals.util.Logger;
import criskey.org.rattarrivals.util.Utils;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.Exceptions;
import rx.schedulers.Schedulers;

/**
 * Created by criskey on 8/1/2016.
 */
public class MainDisplayFragmentPresenter extends BasePresenter<MainDisplayFragmentView> implements LastCheckTimer.TimerCallback {

    private static final long serialVersionUID = 2597913772717307719L;

    private Route route;

    private boolean wayToggle;

    private transient AtomicBoolean isDownloading;

    private transient Subscription routeSubscription;

    private String errorMessage;

    private Transporter transporterRetry;

    /**
     * See MainDisplayFragmentView
     */
    private int hostContainer;

    private transient LastCheckTimer lastCheckTimer;

    public MainDisplayFragmentPresenter() {
        isDownloading = new AtomicBoolean();
        // lastCheckTimer = new LastCheckTimer(this, 0);
    }

    @Override
    public void onBind() {
        if (isDownloading == null)
            isDownloading = new AtomicBoolean();
        if (isViewAttached()) {
            if (isDownloading.get()) {
                getView().onShowWait();
            } else if (hasError()) {
                getView().onShowError(errorMessage);
            } else if (route != null) {
                displayRoute();
                String line = route.getTransporter().getLine();
                getView().onSettingCurrentLine(line);
                //getView().onLastRefreshCheck(lastCheckTimer.getTimeElapsedMessage());
                getView().onShowHistory(getTransporterRepository().getTransporterHistory());
            } else {
                getView().onNothingToShow();
            }
        }
        //lastCheckTimer.startCounting();
    }

    private void downloadRoute(final Transporter transporter, final InputStream rawResourceInputStream) {
        route = null;
        errorMessage = null;
        routeSubscription = Observable
                .create(new Observable.OnSubscribe<Route>() {
                    @Override
                    public void call(Subscriber<? super Route> subscriber) {
                        if (!subscriber.isUnsubscribed()) {
                            try {
                                isDownloading.compareAndSet(false, true);
                                // Thread.sleep(5000);
                                RouteParser routeParser = RouteParser.newInstance(transporter, rawResourceInputStream);
                                subscriber.onNext(routeParser.getRoute());
                            } catch (Exception ex) {
                                Exceptions.propagate(ex);
                                ex.printStackTrace();
                            }
                            subscriber.onCompleted();
                        }
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new RouteDownloadSubscriber());
    }

    private void displayRoute() {
        if (route == null) {
            getView().onNothingToShow();
            return;
        }

        List<Station> currentStationsWay;
        String from, to;
        if (wayToggle) {
            from = route.getToName();
            to = route.getFromName();
            currentStationsWay = route.getToFromStations();
        } else {
            from = route.getFromName();
            to = route.getToName();
            currentStationsWay = route.getFromToStations();
        }
        getView().onDisplayRoute(currentStationsWay, Utils.prettifyStationName(from), Utils.prettifyStationName(to));
    }

    public void changeWay() {
        wayToggle = !wayToggle;
        displayRoute();
    }

    private boolean hasError() {
        return errorMessage != null;
    }

    public void refresh(InputStream rawResourceInputStream) {
        if (route != null)//ensure the route is already saved
            setTransporter(route.getTransporter(), rawResourceInputStream);
    }

    public int getHostContainer() {
        return hostContainer;
    }

    public void setHostContainer(int hostContainer) {
        this.hostContainer = hostContainer;
    }

    public boolean isEmpty() {
        return route == null && errorMessage == null && isDownloading.get() == false;
    }

    private TransporterRepository getTransporterRepository() {
        return (TransporterRepository) getRepositoryManager().getRepository(RepositoryManager.TRANSPORTER_REPOSITORY);
    }

    @Override
    public void unbindView() {
        //lastCheckTimer.stopCounting();
        super.unbindView();
    }

    @Override
    public void onDestroy() {
        if (routeSubscription != null) {
            routeSubscription.unsubscribe();
        }
        getTransporterRepository().commitHistory();
    }

    @Override
    public void retryOnError() {
        setTransporter(transporterRetry);
    }

    public Transporter getTransporter() {
        return route.getTransporter();
    }

    public void setTransporter(Transporter newTransporter) {
        setTransporter(newTransporter, null);
    }

    /**
     * This method is used only for debugging/ offline test mode. See {@link criskey.org.rattarrivals.parse.RouteParser.DebugRouteParser}<br>
     * In product use {@link MainDisplayFragmentPresenter#setTransporter(Transporter)}
     *
     * @param newTransporter
     * @param rawResourceInputStream
     */
    public void setTransporter(Transporter newTransporter, InputStream rawResourceInputStream) {
        if (newTransporter != null) {
            transporterRetry = newTransporter;
            if (isViewAttached()) {
                getView().onShowWait();
                getView().onSettingCurrentLine(newTransporter.getLine());
            }
            downloadRoute(newTransporter, rawResourceInputStream);
        } else if (isViewAttached()) {
            if (isDownloading.get()) {
                getView().onShowWait();
            } else {
                displayRoute();
            }
        }
    }

    @Override
    public void onAsyncElapsedTimeComputed(String elapsedMessage) {
        if (isViewAttached())
            getView().onLastRefreshCheck(elapsedMessage);
    }


    @Override
    public String toString() {
        return "MainDisplayFragmentPresenter{" +
                "route=" + route +
                "retry=" + transporterRetry +
                '}';
    }

    /**
     * ATTENTION: these stations don't have location
     *
     * @return
     */
    public ArrayList<Station> getWholeRouteWithLocations() {
        List<Station> fromToStations = route.getFromToStations();
        List<Station> toFromStations = route.getToFromStations();
        ArrayList<Station> wholeRouteWithLocations = new ArrayList<>();
        for (int i = 0, s = fromToStations.size(); i < s; i++) {
            Station station = fromToStations.get(i);
            wholeRouteWithLocations.add(station);
        }
        for (int i = 0, s = toFromStations.size(); i < s; i++) {
            Station station = toFromStations.get(i);
            if (!wholeRouteWithLocations.contains(station))
                wholeRouteWithLocations.add(station);
        }
        return wholeRouteWithLocations;
    }

    private class RouteDownloadSubscriber extends Subscriber<Route> {

        Route nexRoute;

        @Override
        public void onCompleted() {
            errorMessage = null;
            transporterRetry = null;
            route = nexRoute;
            isDownloading.compareAndSet(true, false);
            if (isViewAttached()) {
                getView().onHideWait();
                displayRoute();
                getView().onSettingCurrentLine(route.getTransporter().getLine());
                getTransporterRepository().addToHistory(route.getTransporter());
                getView().onShowHistory(getTransporterRepository().getTransporterHistory());
                // lastCheckTimer.reset();
            }
        }

        @Override
        public void onError(Throwable e) {
            errorMessage = e.getMessage();
            isDownloading.compareAndSet(true, false);
            if (isViewAttached())
                getView().onShowError(errorMessage);
        }

        @Override
        public void onNext(Route newRoute) {
            nexRoute = newRoute;
        }

    }


}
