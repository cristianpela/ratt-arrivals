package criskey.org.rattarrivals.mvp;

import java.io.Serializable;
import java.util.Locale;

import criskey.org.rattarrivals.repository.Repository;
import criskey.org.rattarrivals.repository.RepositoryManager;
import criskey.org.rattarrivals.repository.TransporterRepository;

/**
 * Naming a presenter should follow this idiom steps:<br>
 * If the activity presenter is named Foo, then presenters of the FooActivity's fragments should start with "Foo"
 * (something like Foo<SomeNome>Presenter).<br>
 * When the activity presenter is removed from cache, the cached fragment presenters will removed also.
 * So any key starting with "Foo" will be removed.
 * <p/>
 * Created by criskey on 28/12/2015.
 */
public interface Presenter<IV extends IView> extends Serializable {

    public void bindView(IV view);

    public void unbindView();

    public boolean isViewAttached();

    public IV getView();

    public void onBind();

    public void onUnbind();

    public void onDestroy();

    public void retryOnError();

    public Locale getCurrentLanguage();

    public RepositoryManager getRepositoryManager();

}
