package criskey.org.rattarrivals.view.station;

import java.util.List;

import criskey.org.rattarrivals.model.Arrival;
import criskey.org.rattarrivals.mvp.IView;

/**
 * Created by criskey on 15/2/2016.
 */
public interface StationView extends IView {

    public void onFirstTimeIntent();

}
