package criskey.org.rattarrivals.repository;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.model.TransporterBuilder;

/**
 * Created by criskey on 10/2/2016.
 */
public abstract class AbstractTransporterRepository implements TransporterRepository {

    private static final String HISTORY_KEY_SET = "history_key_set";

    private History<Transporter> transporterHistory;

    private SharedPreferences historyStorage;

    public AbstractTransporterRepository(Context context) {
        historyStorage = context.getSharedPreferences("history_repo", Context.MODE_PRIVATE);
    }

    @Override
    public boolean deleteAll() {
        historyStorage.edit().clear().apply();
        deleteAllTransporters();
        return true;
    }

    @Override
    public void addToHistory(Transporter transporter) {
        lazyInitHistory();
        transporterHistory.push(transporter);
    }

    @Override
    public List<Transporter> getTransporterHistory() {
        lazyInitHistory();
        return transporterHistory.getEntries();
    }

    @Override
    public void commitHistory() {
        if (transporterHistory == null)
            return;
        List<Transporter> historyEntries = transporterHistory.getEntries();
        Set<String> storeSet = new LinkedHashSet<>();
        for (int i = 0; i < historyEntries.size(); i++) {
            storeSet.add(""+historyEntries.get(i).getId());
        }
        historyStorage
                .edit()
                .clear()
                .putStringSet(HISTORY_KEY_SET, storeSet)
                .apply();
    }

    @Override
    public boolean update(Transporter transporter){
        throw new UnsupportedOperationException("Not implemented");
    }

    public abstract boolean deleteAllTransporters();

    private void lazyInitHistory() {
        if (transporterHistory != null)
            return;
        // these are transporter ids
        Set<String> historyKeys = historyStorage.getStringSet(HISTORY_KEY_SET, null);
        if (historyKeys == null) {
            transporterHistory = new HistoryImpl<>();// empty history
            return;
        }
         /*
         an inner join between history and all transporters. joined by transporter_id
         */
        innerJoin:
        {
            final Map<String, Transporter> joinMap = new HashMap<>();
            for (String key : historyKeys) {
                joinMap.put(key, TransporterBuilder.dummyTransporter());
            }

            List<Transporter> allTransporters = findAll();
            for (Transporter transporter : allTransporters) {
                Transporter fromMapTransporter = joinMap.get(transporter.getId());
                if (fromMapTransporter != null)
                    joinMap.put(""+transporter.getId(), transporter);
            }

            transporterHistory = new HistoryImpl<>(joinMap.values());
        }
    }
};
