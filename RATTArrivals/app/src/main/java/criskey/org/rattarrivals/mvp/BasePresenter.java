package criskey.org.rattarrivals.mvp;

import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.Locale;

import criskey.org.rattarrivals.repository.PreferencesRepository;
import criskey.org.rattarrivals.repository.RepositoryManager;
import criskey.org.rattarrivals.util.ExceptionsResID;
import criskey.org.rattarrivals.util.Logger;

/**
 * Created by criskey on 28/12/2015.
 */
//TODO Rx support
public abstract class BasePresenter<IV extends IView> implements Presenter<IV> {

    private transient WeakReference<IV> viewReference;

    private transient boolean serializable;

    public BasePresenter() {
        serializable = true;
    }

    protected void forbidSerialization() {
        serializable = false;
    }

    public boolean isSerializable() {
        return serializable;
    }

    @Override
    public IV getView() {
        return viewReference.get();
    }

    @Override
    public void bindView(IV view) {
        viewReference = new WeakReference<>(view);
        debugSys("Binding  with " + view + " completed");
    }

    @Override
    public void unbindView() {
        debugSys("Unbinding  " + viewReference.get() + " completed");
        viewReference.clear();
    }

    @Override
    public void onUnbind() {

    }

    @Override
    public void onDestroy() {
    }

    @Override
    public RepositoryManager getRepositoryManager() {
        return RepositoryManager.getInstance();
    }

    @Override
    public boolean isViewAttached() {
        return getView() != null;
    }

    protected void showErrorOnView(Throwable e) {
        if (isViewAttached()) {
            try {
                getView().onShowInfo(ExceptionsResID.catchID(e));
            } catch (Exception ex) {
                getView().onShowInfo(e.getMessage());
            }
        }
    }

    @Override
    public void retryOnError() {
    }

    @Override
    public Locale getCurrentLanguage() {
        PreferencesRepository prefRepo = (PreferencesRepository) getRepositoryManager().getRepository(RepositoryManager.PREFERENCES_REPOSITORY);
        int currentLangIndex = prefRepo.getCurrentLanguage();
        String langSymbol = prefRepo.getAvailableLanguages()[currentLangIndex];
        return new Locale(langSymbol);
    }

    //Logging helper methods;
    protected void debugSys(String message) {
        Logger.log(this, message);
    }

    protected void debugLog(String message) {
        Log.d(this.getClass().getSimpleName(), message);
    }


}
