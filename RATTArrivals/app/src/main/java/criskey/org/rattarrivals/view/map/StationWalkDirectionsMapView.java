package criskey.org.rattarrivals.view.map;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.mvp.WaitView;

/**
 * Created by criskey on 24/3/2016.
 */
public interface StationWalkDirectionsMapView extends WaitView {

    public void onDecodedMapPath(Location from, Location to, List<Location> path);

    public void onWalkingDirections(List<String> directions);
}
