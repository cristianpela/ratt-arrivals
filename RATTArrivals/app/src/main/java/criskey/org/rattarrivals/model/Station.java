package criskey.org.rattarrivals.model;

import android.support.v4.util.Pools;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import criskey.org.rattarrivals.util.BreakPoint;

/**
 * This class models <br><code>www.ratt.ro/txt/select_traseu.php?id_statie=</code></><br>
 * Each station has 1+ {@link Transporter}<br>
 * Created by criskey on 14/1/2016.
 */
public class Station implements Serializable {

    private static final long serialVersionUID = 1264871550742991982L;

    private String id;

    private String name;

    private String alias;

    private List<Transporter> transporters;

    private List<Arrival> arrivals;

    private Location location;

    /**
     * The ID is not primary because initialy there is not any knowledge about station's ID. The ID will be revelead and persisted when the
     * user visits the station. See {@link #setId(String)}
     *
     * @param name         primary key
     * @param transporters transporters which stop in this station; should havae at least one
     */

    public Station(String name, List<Transporter> transporters) {
        this.name = name;
        this.transporters = transporters;
    }

    public Station(String name, Transporter transporter) {
        this(name, Arrays.asList(new Transporter[]{transporter}));
    }

    public Station(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public Station() {
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public boolean hasLocation(){
        return location != null && !location.equals(Location.EMPTY_LOCATION);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Arrival> getArrivals() {
        if (arrivals == null) {
            arrivals = new ArrayList<>(5);
        }
        return arrivals;
    }

    public void setArrivals(List<Arrival> arrivals) {
        this.arrivals = arrivals;
    }

    public boolean hasAlias() {
        return alias != null;
    }

    public List<Transporter> getTransporters() {
        if (transporters == null) {
            transporters = new ArrayList<>(5);
        }
        return transporters;
    }

    public void setTransporters(List<Transporter> transporters) {
        this.transporters = transporters;
    }

    @Override
    public String toString() {
        return name + " " + arrivals + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Station station = (Station) o;

        if (id != null ? !id.equals(station.id) : station.id != null) return false;
        return name.equals(station.name);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + name.hashCode();
        return result;
    }

    // TODO: 13/4/2016 Make a generalized PoolFactory ?
    public static final class Pool {

        private static final int MAX = 50;
        private static final Pools.SimplePool<Station> sPool =
                new Pools.SimplePool<>(MAX);
        //   public static AtomicInteger objectCounter = new AtomicInteger(0);

        private static int objectCounter = 0;

        private static volatile boolean filled = false;

        /**
         * The algorithm works like this:<br>
         * In current "session" (ex: creating objects inside a loop)
         * if the pool is not filled, it will produce objects and release them to the pool. If we're reaching MAX, objects will be not released.<br>
         * Similarly for emptying, acquiring objects until drained. When drained, it will create new objects.</br>
         * After the "session" is done (for example exiting the loop), you MUST call {@link Pool#end()} !!
         * @return
         */
        public synchronized static Station obtain() {
            Station instance;
            if (filled) {
                instance = sPool.acquire(); // drain the pool
                if (instance == null) {
                    instance = new Station();
                    objectCounter = 0; // reset
                }
            } else {
                if (objectCounter < MAX) { // fill the pool
                    instance = new Station();
                    sPool.release(instance);
                    objectCounter++;
                } else {
                    instance = new Station();
                }
            }
            return instance;

        }

        /**
         * Saves the state of the pool after the "session" has ended
         */
        public static void end() {
            filled = objectCounter >= MAX;
        }

    }
}
