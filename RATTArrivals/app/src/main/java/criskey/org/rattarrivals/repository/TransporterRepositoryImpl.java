package criskey.org.rattarrivals.repository;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.model.TransporterBuilder;
import criskey.org.rattarrivals.model.TransporterType;

/**
 * Created by criskey on 4/1/2016.
 */
@Deprecated
public class TransporterRepositoryImpl extends AbstractTransporterRepository {

    private SharedPreferences transporterStorage;

    public TransporterRepositoryImpl(Context context) {
        super(context);
        transporterStorage = context.getSharedPreferences("transporter_repo", Context.MODE_PRIVATE);
    }

    @Override
    public boolean createOne(Transporter entity) {
        transporterStorage.edit().putString(""+entity.getId(), entity.toString()).apply();
        return true;
    }

    @Override
    public boolean createMany(List<Transporter> entities) {
        SharedPreferences.Editor editor = transporterStorage.edit();
        for (int i = 0, len = entities.size(); i < len; i++) {
            Transporter transporter = entities.get(i);
            String key = ""+transporter.getId();
            editor.putString(key, TransporterBuilder.asString(transporter));
        }
        editor.apply();
        return true;
    }

    @Override
    public Transporter findOne(final Long id) {
        return getTransporters(new TransporterFilter() {
            @Override
            public boolean filter(Transporter transporter) {
                return transporter.getId() == id;
            }
        }).get(0);
    }

    @Override
    public List<Transporter> findAll() {
        return getTransporters(null);
    }

    @Override
    public List<Transporter> getTransportersByType(final TransporterType transporterType) {
        return getTransporters(new TransporterFilter() {
            @Override
            public boolean filter(Transporter transporter) {
                return transporter.getTransporterType() == transporterType;
            }
        });
    }

    @Override
    public List<Transporter> getTransportersByFavorite() {
        return getTransporters(new TransporterFilter() {
            @Override
            public boolean filter(Transporter transporter) {
                return transporter.isFavorite();
            }
        });
    }

    @Override
    public void changeFavorite(Transporter transporter) {
        transporter.setFavorite(!transporter.isFavorite());
        String key = ""+transporter.getId();
        transporterStorage.edit().putString(key, TransporterBuilder.asString(transporter)).apply();
    }


    @Override
    public boolean deleteAllTransporters() {
        transporterStorage.edit().clear().apply();
        return true;
    }

    private List<Transporter> getTransporters(TransporterFilter filter) {
        List<Transporter> transporterList = new ArrayList<>();
        Map<String, ?> savedData = transporterStorage.getAll();
        for (String key : savedData.keySet()) {
            String prefData = (String) savedData.get(key);
            Transporter transporter = TransporterBuilder.buildFromString(prefData);
            if (filter == null || filter.filter(transporter))
                transporterList.add(transporter);
        }
        return transporterList;
    }

}
