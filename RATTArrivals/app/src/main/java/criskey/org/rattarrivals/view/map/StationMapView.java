package criskey.org.rattarrivals.view.map;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.mvp.IView;

/**
 * Created by criskey on 9/3/2016.
 */
public interface StationMapView extends IView {

    public void onShowStreetView(Location location);

    public void onPlaceMarkers(List<Station> stations);

    public void onShowMarkerDetail(String title, String snippet, String formatedCoordinates);

    public void onShowButtonCommit(boolean show);

}
