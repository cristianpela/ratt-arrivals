package criskey.org.rattarrivals.view.station;

import java.util.ArrayList;
import java.util.List;

import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.mvp.WaitView;

/**
 * Created by criskey on 25/2/2016.
 */
public interface StationArrivalsView extends WaitView {

    public void onShowStationArrivals(String stationNameOrAlias,
                                      boolean hasAlias,
                                      List<String> stationsUnderAlias,
                                      List<Station> stations);

    public void onShowWaitGeocode();

    public void onShowErrorGeocode(String error);

    public void onFoundGeocode(String formattedLocation);

    /**
     * Callback returned after a call with remote database.
     * @param location can be {@link Location#EMPTY_LOCATION}
     */
    public void onRemoteLocationResult(Location location);

    public void onOpenMapActivity(double latitude, double longitude, ArrayList<Station> stations);

    public void onFirstTimeIntent();

}
