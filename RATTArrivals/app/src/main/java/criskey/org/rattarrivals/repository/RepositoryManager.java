package criskey.org.rattarrivals.repository;

import android.content.Context;
import android.support.annotation.IntDef;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by criskey on 28/1/2016.
 */
public class RepositoryManager {

    public static final int PREFERENCES_REPOSITORY = 0;
    public static final int TRANSPORTER_REPOSITORY = 1;
    public static final int STATION_REPOSITORY = 2;

    private SparseArray<Repository> repositoryCache;
    private Context context;

    private static RepositoryManager INSTANCE;

    private RepositoryManager() {
        repositoryCache = new SparseArray<>();
    }

    /**
     * @param context must be applicationContext!
     * @return singleton of RepositoryManager
     */
    public static RepositoryManager getInstance(Context context) {
        if(INSTANCE == null)
            INSTANCE = new RepositoryManager();
        INSTANCE.context = context;
        return INSTANCE;
    }

    public static RepositoryManager getInstance(){
        if(INSTANCE == null){
            throw new IllegalArgumentException("Please instantiate the manager in your root Application#onCreate with Application's Context");
        }
        return INSTANCE;
    }

    public Repository getRepository(@RepoID int repositoryId) {
        Repository repository = repositoryCache.get(repositoryId);
        if (repository == null)
            repository = createRepository(repositoryId);
        return repository;
    }

    private Repository createRepository(int repositoryId) {
        Repository repository = null;
        switch (repositoryId) {
            case TRANSPORTER_REPOSITORY: {
                repository = new TransporterSQLRepository(context);
                break;
            }
            case PREFERENCES_REPOSITORY: {
                repository = new PreferencesRepositoryImpl(context);
                break;
            }
            case STATION_REPOSITORY: {
                repository = new StationRepositoryImpl(context);
                break;
            }
        }
        repositoryCache.put(repositoryId, repository);
        return repository;
    }

    public void removeCachedRepositories(){
        repositoryCache.clear();
    }

    public Collection<Repository> getAll(){
        return Arrays.asList(getRepository(PREFERENCES_REPOSITORY), getRepository(STATION_REPOSITORY), getRepository(PREFERENCES_REPOSITORY));
    }

    /**
     * Created by criskey on 28/1/2016.
     */
    @IntDef({PREFERENCES_REPOSITORY,
            STATION_REPOSITORY,
            TRANSPORTER_REPOSITORY})
    public static @interface RepoID {
    }

}
