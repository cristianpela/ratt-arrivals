package criskey.org.rattarrivals.util;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.View;

import criskey.org.rattarrivals.mvp.Presenter;
import criskey.org.rattarrivals.mvp.WaitView;

/**
 * Created by criskey on 7/3/2016.
 */
public class AppDialog<P extends Presenter> extends AppCompatDialogFragment implements WaitView, WaitNotifyContentDelegate.Adapter {

    private static final String IS_WAIT_SHOWING = "IS_WAIT_SHOWING";
    private static final String HAS_WAIT_LAYER_KEY = "HAS_WAIT_LAYER_KEY";
    private static final String LAST_ERROR_MESSAGE_KEY = "LAST_ERROR_MESSAGE_KEY";

    protected P pvPresenter;

    private boolean hasWaitLayer;

    private String lastErrorMessage;

    private WaitNotifyContentDelegate waitDelegate;

    private WaitNotifyContentDelegate.ResIdConfig config;

    /**
     * Attach the presenter of the view that has this dialog attached
     *
     * @param pvPresenter
     */
    public void attachPresenter(P pvPresenter) {
        this.pvPresenter = pvPresenter;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        hasWaitLayer = (savedInstanceState != null) ? savedInstanceState.getBoolean(HAS_WAIT_LAYER_KEY) : hasWaitLayer;
        if (hasWaitLayer) {
            waitDelegate = new WaitNotifyContentDelegate(this, config);
            if (savedInstanceState != null) {
                lastErrorMessage = savedInstanceState.getString(LAST_ERROR_MESSAGE_KEY);
                if (lastErrorMessage != null)
                    waitDelegate.onShowError(lastErrorMessage);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(HAS_WAIT_LAYER_KEY, hasWaitLayer);
        outState.putString(LAST_ERROR_MESSAGE_KEY, lastErrorMessage);
        super.onSaveInstanceState(outState);
    }

    /**
     * in order to boot the waitLayerm, this must be called in onCreateView
     */
    public void enableWaitLayer() {
        enableWaitLayer(null);
    }

    /**
     * In order to boot the waitLayer, this must be called in onCreateView
     *
     * @param config
     */
    public void enableWaitLayer(WaitNotifyContentDelegate.ResIdConfig config) {
        hasWaitLayer = true;
        this.config = (config == null) ? WaitNotifyContentDelegate.ResIdConfig.getDefaultConfig() : config;
    }


    @Override
    public void onShowWait() {
        if (!hasWaitLayer)
            throw new UnsupportedOperationException("Please enable the wait layer!");
        waitDelegate.onShowWait();
    }

    @Override
    public void onHideWait() {
        if (!hasWaitLayer)
            throw new UnsupportedOperationException("Please enable the wait layer!");
        waitDelegate.onHideWait();
    }

    @Override
    public void onShowProgress(@StringRes int res) {
        if (!hasWaitLayer)
            throw new UnsupportedOperationException("Please enable the wait layer!");
        waitDelegate.setMessageStringRes(res);
    }

    @Override
    public void onShowError(String errorMessage) {
        if (!hasWaitLayer)
            throw new UnsupportedOperationException("Please enable the wait layer!");
        lastErrorMessage = errorMessage;
        waitDelegate.onShowError(errorMessage);
    }

    @Override
    public View adaptFindViewById(@IdRes int id) {
        if (!hasWaitLayer)
            throw new UnsupportedOperationException("Please enable the wait layer!");
        return getView().findViewById(id);
    }

    @Override
    public Resources adaptResources() {
        if (!hasWaitLayer)
            throw new UnsupportedOperationException("Please enable the wait layer!");
        return getResources();
    }

    @Override
    public void onDestroy() {
        if(hasWaitLayer)
            waitDelegate.destroy();
        super.onDestroy();
    }

    /**
     * This MUST be overriden by any extension of the this class
     *
     * @return
     */
    @Override
    public View.OnClickListener adaptOnRetryListener() {
        if (!hasWaitLayer)
            throw new UnsupportedOperationException("Please enable the wait layer!");
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: find a way to reattach the presenter after a screen rotation
                if (pvPresenter != null)
                    pvPresenter.retryOnError();
                else
                    new IllegalAccessException("In order to retry the operation you must attach the parent view presenter. Call " +
                            " attachPresenter()");
            }
        };
    }

    @Override
    public View.OnClickListener adaptOnCancelListener() {
        if (!hasWaitLayer)
            throw new UnsupportedOperationException("Please enable the wait layer!");
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                waitDelegate.onHideWait();
            }
        };
    }

    @Override
    public void onShowInfo(String infoMessage) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onShowInfo(int resid) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onPresenterAttached() {

    }

}
