package criskey.org.rattarrivals.mvp;

import android.content.Context;

import java.lang.reflect.ParameterizedType;

import criskey.org.rattarrivals.repository.RepositoryManager;
import criskey.org.rattarrivals.util.Logger;

/**
 * Created by criskey on 5/1/2016.
 */
public class VPBinder<V extends IView, P extends Presenter> {

    private V view;
    private P presenter;
    private PresenterManager<P> presenterManager;

    public VPBinder(V view) {
        this.view = view;
        presenterManager = PresenterManager.getInstance();
        presenter = presenterManager.getPresenter(extractPresenterSignatureClass());
    }

    public void bindView() {
        logBindingState("bind");
        presenter.bindView(view);
        presenter.onBind();
    }

    public void unbindView() {
        logBindingState("unbind");
        presenter.onUnbind();
        presenter.unbindView();
        presenterManager.putPresenter(presenter);
    }

    public P getPresenter() {
        return presenter;
    }

    public void destroyPresenter() {
        presenterManager.removePresenter(presenter);
    }

    private Class<P> extractPresenterSignatureClass() {
        ParameterizedType genericSuperclass = (ParameterizedType) view.getClass()
                .getGenericSuperclass();
        Class<P> presenterClass = (Class<P>) genericSuperclass
                .getActualTypeArguments()[0];
        return presenterClass;
    }

    private void logBindingState(String state) {
        Logger.log(this, state + " " + view.getClass().getSimpleName() + " <-> " + presenter.getClass().getSimpleName());
    }
}
