package criskey.org.rattarrivals.mvp;

import android.support.annotation.StringRes;

/**
 * Created by criskey on 28/12/2015.
 */
public interface IView {

    /**
     * Simple toast message
     *
     * @param infoMessage
     */
    public void onShowInfo(String infoMessage);

    /**
     * Simple toast message
     *
     * @param resId
     */
    public void onShowInfo(@StringRes int resId);

    /**
     * Callback for when the presenter is attached to the View.
     * In this method is better to register listeners for widgets
     * Best case scenario:<br>
     * When recreated after a process kill, if the IView implementor implements a {@code SeekBar.OnChangeListener} and the listener is registered on
     * {@code onCreate} and if in the {@code onProgressChanged} is called the presenter, this will result a NPE, because
     * the presenter is bound on {@code onPause} lifecycle. So the best way to do is this:<br>
     * {@code
     * public void onPresenterAttached() {
     * mSeekBar.setOnSeekBarChangeListener(this)
     * }
     * }
     */
    public void onPresenterAttached();
}
