package criskey.org.rattarrivals.view.station;

import android.support.v4.util.Pair;
import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.mvp.BasePresenter;
import criskey.org.rattarrivals.parse.ObservableNetworkDataDownloader;
import criskey.org.rattarrivals.repository.RepositoryManager;
import criskey.org.rattarrivals.repository.StationRepository;
import criskey.org.rattarrivals.util.BreakPoint;
import criskey.org.rattarrivals.util.ExceptionsResID;
import rx.Observer;
import rx.Subscription;
import rx.exceptions.Exceptions;

/**
 * Created by criskey on 15/3/2016.
 */

//TODO solve the state of newAddress newLocation newSearch
public class StationNearPresenter extends BasePresenter<StationNearView> implements
        Observer<List<StationNearPresenter.DistanceStation>>,
        ObservableNetworkDataDownloader.Callee<List<StationNearPresenter.DistanceStation>> {

    private static final long serialVersionUID = -5467694756748200128L;

    private static final int MAX_RADIUS = 700;

    private int radius;

    private String address;

    private Location origin;

    private List<DistanceStation> stations;

    private transient StationRepository repository;

    private transient Subscription subscription;

    private transient volatile boolean isNewSearch;

    private transient String searchAddress;

    public StationNearPresenter() {
        this.stations = new ArrayList<>();
    }

    @Override
    public void onBind() {
        if (stations == null)
            this.stations = new ArrayList<>();
        repository = (StationRepository) getRepositoryManager().getRepository(RepositoryManager.STATION_REPOSITORY);
        if (isViewAttached()) {
            getView().onSliderInitialState(radius, MAX_RADIUS);
            if (!stations.isEmpty()) {
                getView().onShowStationsInRadius(stations, address);
            }
        }
    }

    @Override
    public void unbindView() {
        if (subscription != null)
            subscription.unsubscribe();
        super.unbindView();
    }

    public void searchAround(String newAddress) {
        BreakPoint.breakInTheNameOfLove();
        if (isViewAttached() && TextUtils.isEmpty(newAddress) && TextUtils.isEmpty(address)) {
            getView().onShowInfo("Address is empty!");
            return;
        }
        if (TextUtils.isEmpty(address) || !address.equals(newAddress)) {
            searchAddress = newAddress;
            isNewSearch = true;
        }
        subscription = ObservableNetworkDataDownloader.runInParallel(this, this);
    }

    public void setRadius(int radius) {
        this.radius = radius;
        if (isViewAttached())
            getView().onShowRadius(radius);
    }

    @Override
    public void onCompleted() {
        isNewSearch = false;
    }

    @Override
    public void onError(Throwable e) {
        showErrorOnView(e);
        isNewSearch = false;
        searchAddress = address;
    }

    @Override
    public void onNext(List<DistanceStation> stations) {
        this.stations.clear();
        DistanceStation firstDistance = stations.get(0);
        if (firstDistance.station != null) {
            origin = firstDistance.from;
            address = firstDistance.address;
            this.stations.addAll(stations);
        }
        if (isViewAttached()) {
            getView().onShowStationsInRadius(this.stations, address);
        }
    }

    /**
     * Should not called in the main thread!
     *
     * @return
     */
    @Override
    public List<DistanceStation> called() {
        Location newOrigin = origin;
        if (isNewSearch) {
            newOrigin = ObservableNetworkDataDownloader.getLocationService(searchAddress);
            if (newOrigin.equals(Location.EMPTY_LOCATION))
                Exceptions.propagate(ExceptionsResID.throwID(R.string.search_geocode_bad_location));
        }
        LatLng convertedOrigin = new LatLng(newOrigin.getLatitude(), newOrigin.getLongitude());
        List<DistanceStation> foundStations = new ArrayList<>();

        List<Station> all = repository.findAll();
        for (Station station : all) {
            Location location = station.getLocation();
            if (!location.isEmpty()) {
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                double distance = SphericalUtil.computeDistanceBetween(convertedOrigin, latLng);
                if (distance <= radius) {
                    if (repository.isJoined(station))
                        repository.findJoinedStation(station);
                    foundStations.add(new DistanceStation(distance, newOrigin, station, searchAddress));
                }
            }
        }
        if (foundStations.isEmpty()) {
            foundStations.add(new DistanceStation(0, newOrigin, null, searchAddress));
        }
        return foundStations;
    }


    public static class DistanceStation implements Serializable {

        public final Station station;

        public final int distance;

        public final Location from;

        private final String address;

        private DistanceStation(double distance, Location origin, Station station, String address) {
            this.station = station;
            this.distance = (int) Math.floor(distance);
            from = origin;
            this.address = address;
        }
    }
}
