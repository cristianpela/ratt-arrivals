package criskey.org.rattarrivals.model;

import android.support.v4.util.Pools;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class models <br>
 * <code>www.ratt.ro/txt/afis_msg.php?id_traseu=?&id_statie=?</code><br>
 * Created by criskey on 14/1/2016.
 */
public class Arrival implements Serializable {

    private static final String NO_TIME = "--:--";
    private static final long serialVersionUID = -5586808480213329012L;

    private Transporter transporter;
    private String time;

    private boolean locked;

    public Arrival() {

    }

    public Arrival(String time, Transporter transporter) {
        this.time = time;
        this.transporter = transporter;
    }

    public static Arrival emptyArrival(Transporter transporter) {
        return new Arrival(NO_TIME, transporter);
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Transporter getTransporter() {
        return transporter;
    }

    public void setTransporter(Transporter transporter) {
        this.transporter = transporter;
    }

    @Override
    public String toString() {
        return time + " # " + transporter.getLine();
    }

    public static final class Pool {

        private static int MAX = 50;
        private static final Pools.SimplePool<Arrival> sPool =
                new Pools.SimplePool<>(MAX);
        //   public static AtomicInteger objectCounter = new AtomicInteger(0);

        private static int objectCounter = 0;

        private static volatile boolean filled = false;

        public static Arrival obtain(int poolSize) {
            MAX = poolSize;
            return obtain();
        }

        /**
         * The algorithm works like this:<br>
         * In current "session" (ex: creating objects inside a loop)
         * if the pool is not filled, it will produce objects and release them to the pool. If we're reaching MAX, objects will be not released.<br>
         * Similarly for emptying, acquiring objects until drained. When drained, it will create new objects.</br>
         * After the "session" is done (for example exiting the loop), you MUST call {@link Pool#end()} !!
         *
         * @return
         */
        public synchronized static Arrival obtain() {
            Arrival instance;
            if (filled) {
                instance = sPool.acquire(); // drain the pool
                if (instance == null) {
                    instance = new Arrival();
                    objectCounter = 0; // reset
                }
            } else {
                if (objectCounter < MAX) { // fill the pool
                    instance = new Arrival();
                    sPool.release(instance);
                    objectCounter++;
                } else {
                    instance = new Arrival();
                }
            }
            return instance;

        }

        /**
         * Saves the state of the pool after the "session" has ended
         */
        public static void end() {
            filled = objectCounter >= MAX;
        }

    }
}
