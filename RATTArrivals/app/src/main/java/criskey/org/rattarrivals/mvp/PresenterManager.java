package criskey.org.rattarrivals.mvp;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import criskey.org.rattarrivals.util.Logger;

/**
 * Created by criskey on 28/12/2015.
 */
public final class PresenterManager<P extends Presenter> {

    private static PresenterManager INSTANCE;
    private Map<String, P> presenterCache;
    private PresenterSerializer serializer;
    private PresenterSerializerQueue serializerQueue;

    private PresenterManager() {
        presenterCache = new HashMap<>();
        serializer = new PresenterSerializer();
        safeStartQueue();
    }

    public static <P extends Presenter> PresenterManager<P> getInstance() {
        if (INSTANCE == null)
            INSTANCE = new PresenterManager<P>();
        if (!INSTANCE.serializer.isDirectorySet())
            throw new IllegalArgumentException("Please set serializing directory first by calling getInstance(dir)");
        return INSTANCE;
    }

    public static <P extends Presenter> PresenterManager<P> getInstance(File rootDirectory) {
        if (INSTANCE == null)
            INSTANCE = new PresenterManager<P>();
        INSTANCE.serializer.setDirectory(rootDirectory);
        return INSTANCE;
    }

    /**
     * this method ensure that only one PresenterSerializerQueue is spawned
     */
    private void safeStartQueue() {
        Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
        PresenterSerializerQueue existingSerializerQueue = null;
        for (Thread thread : threadSet) {
            if (thread.getName().equals(PresenterSerializerQueue.PRESENTER_QUEUE_THREAD_NAME)) {
                existingSerializerQueue = (PresenterSerializerQueue) thread;
                Logger.log(existingSerializerQueue.getClass(),"There is already a queue running... No need to create a new one");
                break;
            }
        }
        if (existingSerializerQueue == null) { // start a new queue if is not already running
            serializerQueue = new PresenterSerializerQueue(serializer);
            serializerQueue.startAndServe();
        }else
            serializerQueue = existingSerializerQueue;
    }

    public P getPresenter(Class<P> clazz) {
        String className = clazz.getSimpleName();
        P presenter = presenterCache.get(className);
        if (presenter == null) {
            // get from it from disk
            presenter = serializer.deserialize(clazz.getSimpleName());
            if (presenter == null) {
                presenter = createPresenter(clazz);
                Logger.log(className + " was new created");
            }
            return presenter;
        }
        Logger.log(className + " was retrieved from cache");
        return presenter;
    }

    /**
     * Caching a presenter attached to an activity, or attached to a fragment of an activity
     *
     * @param presenter
     */
    public void putPresenter(P presenter) {
        String key = createKey(presenter);
        presenterCache.put(key, presenter);
        serializerQueue.serialize(key, presenter);
    }

    @NonNull
    private String createKey(Presenter presenter) {
        return presenter.getClass().getSimpleName();
    }

    /**
     * This will perform a "deep" removal. Meaning that not only the activity presenter will be removed but also the activity
     * fragments presenters. See the naming convention in {@link Presenter}
     *
     * @param presenter attached to an activity/fragment
     */
    //TODO think for a better way to do this. Right now, by doing this I'm losing the O(1) key access
    public void removePresenter(P presenter) {
        String key = createKey(presenter);
        String suffix = "Presenter";
        boolean hasSuffix = key.endsWith(suffix);

        String searchStem = (hasSuffix) ? key.split(suffix)[0] : key;

        presenter.onDestroy();
        presenterCache.remove(key);
        // serializer.serialize(key, presenter);
        serializerQueue.serialize(key, presenter);

        Logger.log("Main Presenter Manager CLEAR: " + key);
        Set<String> keysToRemove = new HashSet<>(presenterCache.keySet());
        for (String k : keysToRemove) {
            if (k.startsWith(searchStem)) {
                P childPresenter = presenterCache.get(k);
                childPresenter.onDestroy();
                presenterCache.remove(k);
                // serializer.serialize(k, childPresenter);
                serializerQueue.serialize(k, childPresenter);
                Logger.log("Main Presenter Manager CLEAR: " + k);
            }
        }
    }

    /**
     * this should be called on low memory
     */
    public void removeCachedPresenters() {
        presenterCache.clear();
    }

    public int size() {
        return presenterCache.size();
    }

    @NonNull
    private P createPresenter(Class<P> clazz) {
        P presenter = null;
        try {
            presenter = clazz.asSubclass(clazz).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return presenter;
    }

    @Override
    public String toString() {
        return "PresenterManager{" +
                presenterCache +
                '}';
    }

    private static final class Creator<P extends Presenter> {
        public static final PresenterManager INSTANCE = new PresenterManager<>();
    }

    private static class PresenterSerializerQueue extends HandlerThread {

        public static final String PRESENTER_QUEUE_THREAD_NAME = "PRESENTER_QUEUE_THREAD";

        private Handler handler;
        private PresenterManager.PresenterSerializer serializer;

        public PresenterSerializerQueue(PresenterManager.PresenterSerializer serializer) {
            super(PRESENTER_QUEUE_THREAD_NAME);
            this.serializer = serializer;
        }

        public void serve() {
            handler = new Handler(getLooper());
        }

        public <P extends Presenter> void serialize(final String key, final
        P presenter) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (getLooper() == Looper.getMainLooper())
                        throw new IllegalAccessError("The serializing code must run not on the main thread!");
                    Logger.log(PresenterSerializerQueue.this, "queuing for serialization!" );
                    serializer.serialize(key, presenter);
                }
            });
        }

        public <P extends Presenter> P deserialize(final String key) {
            // TODO implement this
            throw new UnsupportedOperationException("//TODO");
        }

        public void startAndServe() {
            start();
            serve();
            Logger.log(this, "Queue has started and is serving...");
        }
    }

    class PresenterSerializer {

        static final String EXTENSION = ".prs";

        static final String DIR_NAME = "ratt-presenters";

        private File directory;

        void setDirectory(File directory) {
            File dir = new File(directory, DIR_NAME);
            if (!dir.exists()) {
                dir.mkdir();
            }
            this.directory = dir;
        }

        boolean isDirectorySet() {
            return directory != null;
        }

        void serialize(String key, P p) {
            FileOutputStream fos = null;
            ObjectOutputStream out = null;
            try {
                Logger.log(this, "Serializing presenter " + key);
                File file = new File(directory, key + EXTENSION);
                fos = new FileOutputStream(file);
                out = new ObjectOutputStream(fos);
                out.writeObject(p);
                out.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        public P deserialize(String key) {
            FileInputStream fis = null;
            ObjectInputStream in = null;
            P p = null;
            File file = new File(directory, key + EXTENSION);
            if (!file.exists()) {
                Logger.log(file.getPath() + " doesn't exists!");
                return null;
            }
            try {
                fis = new FileInputStream(file);
                in = new ObjectInputStream(fis);
                p = (P) in.readObject();
                Logger.log(this, "Deserializing presenter " + key + " -- " + p.toString());
                in.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return p;
        }

    }
}
