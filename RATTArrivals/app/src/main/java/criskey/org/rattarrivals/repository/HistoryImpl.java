package criskey.org.rattarrivals.repository;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by criskey on 1/2/2016.
 */
public class HistoryImpl<E> implements History<E> {

    private int capacity;

    private Map<E, HistoryEntry> entriesMap;

    private HistoryEntry head, tail; // sentinel entries;

    public HistoryImpl(int capacity, Collection<E> savedEntries) {
        entriesMap = new HashMap<>();
        initCapacity(capacity, savedEntries);
        initMap(savedEntries);
    }

    public HistoryImpl(int capacity) {
        this(capacity, null);
    }

    public HistoryImpl(Collection<E> savedEntries) {
        this(DEFAULT_CAPACITY, savedEntries);
    }

    public HistoryImpl() {
        this(DEFAULT_CAPACITY);
    }


    @Override
    public void push(E value) {
        // if value is already on top, do nothing
        if (!head.next.isNull() && head.next.value.equals(value)) {
            return;
        }

        // add the entry to the top of stack;
        HistoryEntry entry = getHistoryEntry(value);
        head.next.previous = entry;
        entry.next = head.next;
        head.next = entry;
        entry.previous = head;

        //trim the stack by removing the oldest entry
        if (entriesMap.size() > capacity)
            evict();
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public void setCapacity(int newCapacity) {
        if (newCapacity < DEFAULT_CAPACITY) {
            throw new IllegalArgumentException("The capacity must be at least " + DEFAULT_CAPACITY);
        }
        // evict any overflowing entries over the new capacity
        // Ex oldcap = 8 and newcap=6 then we must evict the last 2 oldest entries;
        int over = this.capacity - newCapacity;
        while (over > 0) {
            evict();
            over--;
        }
        this.capacity = newCapacity;
    }

    private void initCapacity(int capacity, Collection<E> savedEntries) {
        if (savedEntries == null) {
            this.capacity = capacity;
        } else {
            int targetCapacity = savedEntries.size();
            if (targetCapacity < DEFAULT_CAPACITY)
                targetCapacity = DEFAULT_CAPACITY;
            this.capacity = targetCapacity;
        }
    }

    @Override
    public List<E> getEntries() {
        //TODO: make this a plain array, since the size is fixed?
        List<E> output = new ArrayList<>(entriesMap.size());
        HistoryEntry current = head.next;
        while (!current.isNull()) {
            output.add(current.value);
            current = current.next;
        }
        return output;
    }

    private void initMap(Collection<E> savedEntries) {
        head = new HistoryEntry(null);
        tail = new HistoryEntry(null);
        head.next = tail;
        tail.previous = head;
        if (savedEntries != null) {
            for (E se : savedEntries) {
                push(se);
            }
        }
    }

    @NonNull
    private HistoryEntry getHistoryEntry(E value) {
        HistoryEntry entry = entriesMap.get(value);
        if (entry != null) {
            entry.remove();
        } else {
            entry = new HistoryEntry(value);
            entriesMap.put(value, entry);
        }
        return entry;
    }

    private void evict() {
        HistoryEntry entry = tail.previous;
        entriesMap.remove(entry.value.toString());
        tail.previous.remove();
    }

    private class HistoryEntry {
        E value;
        HistoryEntry next;
        HistoryEntry previous;

        public HistoryEntry(E value) {
            this.value = value;
        }

        public void remove() {
            previous.next = next;
            next.previous = previous;
            next = null;
            previous = null;
        }

        boolean isNull() {
            return value == null;
        }
    }
}
