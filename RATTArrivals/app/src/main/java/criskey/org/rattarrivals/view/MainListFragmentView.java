package criskey.org.rattarrivals.view;

import java.util.List;

import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.mvp.IView;

/**
 * Created by criskey on 6/1/2016.
 */
public interface MainListFragmentView extends IView {

    public void displayTransporters(List<Transporter> transporterList);
}
