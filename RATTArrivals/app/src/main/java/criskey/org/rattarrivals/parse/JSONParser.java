package criskey.org.rattarrivals.parse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by criskey on 24/3/2016.
 */
public abstract class JSONParser extends Parser {

    protected JSONObject jsonDocument;

    public JSONParser(AbstractNetworkDataDownloader downloader) throws ParsingException, JSONException {
        super(downloader);
        jsonDocument = new JSONObject(document.text());
    }

    public JSONParser(String link) throws ParsingException, JSONException {
        super(new NetworkDataDownloader(link));
        jsonDocument = new JSONObject(document.text());
    }
}
