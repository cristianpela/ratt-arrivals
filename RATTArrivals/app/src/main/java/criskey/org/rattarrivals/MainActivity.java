package criskey.org.rattarrivals;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.mvp.BaseActivity;
import criskey.org.rattarrivals.util.Logger;
import criskey.org.rattarrivals.util.WaitNotifyContentDelegate;
import criskey.org.rattarrivals.view.DisplayFilter;
import criskey.org.rattarrivals.view.MainDisplayFragment;
import criskey.org.rattarrivals.view.MainListFragment;
import criskey.org.rattarrivals.view.OnTransporterItemClick;
import criskey.org.rattarrivals.view.map.StationMapsActivity;
import criskey.org.rattarrivals.view.station.StationActivity;

import static criskey.org.rattarrivals.util.WaitNotifyContentDelegate.Adapter;
import static criskey.org.rattarrivals.util.WaitNotifyContentDelegate.AdapterStrategy;
import static criskey.org.rattarrivals.util.WaitNotifyContentDelegate.ResIdConfig;


public class MainActivity extends BaseActivity<MainPresenter> implements
        MainView,
        OnTransporterItemClick,
        MainDisplayFragment.OnWaitMainDisplayFragment {

    public static final String MAIN_DISPLAY_BACK_ENTRY = "MAIN_DISPLAY_BACK_ENTRY";

    private DrawerLayout mDrawerLayout;

    private Toolbar mToolbar;

    private CollapsingToolbarLayout mLayoutCollapsingToolbar;

    private View mRouteToolbar;

    private WaitNotifyContentDelegate mWaitNotifyViewDelegate;

    private FragmentManager mFragmentManager;

    private ImageButton mImageButtonRouteRefresh;

    private MainDisplayFragment mMainDisplayFragment;

    private MainListFragment mMainListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_routes);
        mFragmentManager = getSupportFragmentManager();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (isInLandscape()) {
            landscapeInitWidgets();
        } else {
            portraitInitWidgets();
        }
        initFragments(savedInstanceState);
        createWaitNotifyViewDelegate();
    }


    private void portraitInitWidgets() {
        mLayoutCollapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);
        mRouteToolbar = findViewById(R.id.routeToolbar);
        mImageButtonRouteRefresh = (ImageButton) findViewById(R.id.imageButtonRouteRefresh);
        mImageButtonRouteRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMainDisplayFragment.refresh();
            }
        });
        ImageButton imageButtonRouteMap = (ImageButton) findViewById(R.id.imageButtonRouteMap);
        imageButtonRouteMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMainDisplayFragment.startMapRouteActivity();
            }
        });
    }

    private void landscapeInitWidgets() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.slidingPane);
    }

    private void initFragments(Bundle savedInstanceState) {
        if (isInLandscape()) {//landscape
            if (savedInstanceState == null) {
                mMainListFragment = new MainListFragment();
                mMainDisplayFragment = MainDisplayFragment.createFragment(null);
                mFragmentManager.beginTransaction()
                        .add(R.id.drawerContainer, mMainListFragment, MainListFragment.FRAGMENT_TAG)
                        .add(R.id.mainContainer, mMainDisplayFragment, MainDisplayFragment.FRAGMENT_TAG)
                        .commit();
            } else {
                // if we're coming from portrait pop the back stack first, then add to landscape container
                // also we need to pop "twice" in case we're coming after a kill;
                mFragmentManager.popBackStackImmediate();
                mMainListFragment = (MainListFragment) mFragmentManager.findFragmentByTag(MainListFragment.FRAGMENT_TAG);
                mMainDisplayFragment = (MainDisplayFragment) mFragmentManager.findFragmentByTag(MainDisplayFragment.FRAGMENT_TAG);
                if (mMainDisplayFragment == null) {
                    mMainDisplayFragment = MainDisplayFragment.createFragment(null);
                    mFragmentManager.beginTransaction()
                            .add(R.id.mainContainer, mMainDisplayFragment, MainDisplayFragment.FRAGMENT_TAG)
                            .commit();
                } else if (!mMainDisplayFragment.isAdded() && !mMainListFragment.isAdded()) {
                    //this happens when fragments are recreated after an activity kill during pause (triggered by low memory)
                    mFragmentManager.popBackStackImmediate();
                    mFragmentManager.beginTransaction()
                            .remove(mMainDisplayFragment).commit();
                    mFragmentManager.executePendingTransactions();
                    mFragmentManager.beginTransaction()
                            .replace(R.id.drawerContainer, mMainListFragment, MainListFragment.FRAGMENT_TAG)
                            .replace(R.id.mainContainer, mMainDisplayFragment, MainDisplayFragment.FRAGMENT_TAG)
                            .commit();
                    mFragmentManager.executePendingTransactions();
                }
            }
        } else {//portrait
            if (savedInstanceState == null) {
                mMainListFragment = new MainListFragment();
                mFragmentManager.beginTransaction()
                        .add(R.id.drawerContainer, mMainListFragment, MainListFragment.FRAGMENT_TAG)
                        .commit();
            } else {
                mMainListFragment = (MainListFragment) mFragmentManager.findFragmentByTag(MainListFragment.FRAGMENT_TAG);
                mMainDisplayFragment = (MainDisplayFragment) mFragmentManager.findFragmentByTag(MainDisplayFragment.FRAGMENT_TAG);
                if (mMainDisplayFragment == null)//this happens when the app was killed when mMainListFragment was in foreground
                    return;

                //if we're coming from landscape, remove it first from landscape container
                mFragmentManager.beginTransaction()
                        .remove(mMainDisplayFragment).commit();
                mFragmentManager.executePendingTransactions();

                FragmentTransaction transaction = mFragmentManager.beginTransaction();
                transaction
                        .replace(R.id.drawerContainer, mMainDisplayFragment, MainDisplayFragment.FRAGMENT_TAG);
                if (mFragmentManager.getBackStackEntryCount() == 0) // if the fragment is not reconstructed after a kill (we're coming from land)
                    transaction.addToBackStack(MAIN_DISPLAY_BACK_ENTRY);
                transaction.commit();
                mFragmentManager.executePendingTransactions();
            }
        }
    }

    private void popFragmentBackStackTwice() {
        // TODO: 13/4/2016  Why back stack has duplicate records instead of one when recreating after a kill
        //mFragmentManager.popBackStackImmediate(MAIN_DISPLAY_BACK_ENTRY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        mFragmentManager.popBackStackImmediate();
        mFragmentManager.popBackStackImmediate();
    }

    @Override
    public void onPresenterAttached() {
    }

    private void createWaitNotifyViewDelegate() {
        Adapter adapter = AdapterStrategy.activityAdapter(this,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getPresenter().retryOnError();
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mWaitNotifyViewDelegate.onHideWait();
                    }
                });
        WaitNotifyContentDelegate.ResIdConfig config = new ResIdConfig.Builder().mainContentLayoutId(R.id.drawerContainer).build();
        mWaitNotifyViewDelegate = new WaitNotifyContentDelegate(adapter, config);
    }

    public void actionOpenKMLRoute(View button) {
        startActivity(StationMapsActivity.createKMLRouteIntent(this, R.raw.bus40kml));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        DisplayFilter displayFilter = null;
        if (!isInLandscape()) {
            portraitBringMainListFragmentToFront();
        }
        switch (id) {
            case R.id.action_all:
                displayFilter = DisplayFilter.ALL;
                mMainListFragment.setSelectedDisplayFilter(displayFilter);
                landscapeOpenDrawer();
                break;
            case R.id.action_menu_bus:
                displayFilter = DisplayFilter.BUS;
                mMainListFragment.setSelectedDisplayFilter(displayFilter);
                landscapeOpenDrawer();
                break;
            case R.id.action_menu_tram:
                displayFilter = DisplayFilter.TRAM;
                mMainListFragment.setSelectedDisplayFilter(displayFilter);
                landscapeOpenDrawer();
                break;
            case R.id.action_menu_trolley:
                displayFilter = DisplayFilter.TROLLEY;
                mMainListFragment.setSelectedDisplayFilter(displayFilter);
                landscapeOpenDrawer();
                break;
            case R.id.action_menu_favorite:
                displayFilter = DisplayFilter.FAVORITE;
                mMainListFragment.setSelectedDisplayFilter(displayFilter);
                landscapeOpenDrawer();
                break;
            case R.id.action_language:
                showLanguageDialog();
                break;
            case R.id.action_stations:
                startActivity(new Intent(this, StationActivity.class));
                break;
            case R.id.action_reset:
                getPresenter().reset();
                break;
        }
        changeSubtitle(displayFilter);
        return super.onOptionsItemSelected(item);
    }

    public void portraitBringMainListFragmentToFront() {
        // TODO: 13/4/2016 forced to pop twice in case of a system kill
        popFragmentBackStackTwice();
        if (!mMainListFragment.isAdded()) {
            mFragmentManager.beginTransaction().replace(R.id.drawerContainer, mMainListFragment, MainListFragment.FRAGMENT_TAG).commit();
            mFragmentManager.executePendingTransactions();
            portraitDisableScrimActions();
        }
        mLayoutCollapsingToolbar.setTitleEnabled(false);
        mRouteToolbar.setVisibility(View.GONE);
    }

    private void showLanguageDialog() {
        String languages[] = getPresenter().getAvailableLanguages();
        int currLangIdx = getPresenter().getCurrentLanguageIndex();
        final AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.lang_dialog_title)
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getPresenter().saveCurrentLanguage();
                        finish();
                        startActivity(getIntent())/*.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));*/;
                    }
                })
                .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getPresenter().setCurrentLanguage(-1);// no language is set
                    }
                })
                .setSingleChoiceItems(languages, currLangIdx, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        getPresenter().setCurrentLanguage(item);
                    }
                })
                .create();
        alertDialog.show();
    }

    public void landscapeOpenDrawer() {
        if (isInLandscape()) {
            mDrawerLayout.openDrawer(Gravity.LEFT);
        }
    }

    @Override
    public void onLastDisplayFilter(DisplayFilter displayFilter) {
        changeSubtitle(displayFilter);
    }

    @Override
    public void onShowWait() {
        mWaitNotifyViewDelegate.onShowWait();
    }

    @Override
    public void onHideWait() {
        mWaitNotifyViewDelegate.onHideWait();
        mMainListFragment.setSelectedDisplayFilter(DisplayFilter.ALL);
    }

    @Override
    public void onShowProgress(@StringRes int res) {
        mWaitNotifyViewDelegate.onShowProgress(res);
    }

    @Override
    public void onShowError(String errorMessage) {
        mWaitNotifyViewDelegate.onShowError(errorMessage);
    }

    @Override
    public void onDestroy() {
        mWaitNotifyViewDelegate.destroy();
        super.onDestroy();
    }

    public void portraitEnableScrimActions(String routeLine) {
        mLayoutCollapsingToolbar.setTitleEnabled(true);
        mLayoutCollapsingToolbar.setTitle(routeLine);
        mRouteToolbar.setVisibility(View.VISIBLE);
    }

    public void portraitDisableScrimActions() {
        mRouteToolbar.setVisibility(View.GONE);
        mLayoutCollapsingToolbar.setTitleEnabled(false);

    }

    private void changeSubtitle(DisplayFilter displayFilter) {
        if (displayFilter != null) {
            getSupportActionBar().setSubtitle(getResources().getString(displayFilter.getDisplayResId()));
        }
    }

    public boolean isInLandscape() {
        return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        // to pass robolectric tests
        //return findViewById(R.id.rightContainer) != null;
    }

    @Override
    public void onBackPressed() {
        if (!isInLandscape())
            portraitDisableScrimActions();
        super.onBackPressed();
    }

    /*
     * ###################Fragment Callbacks###################
     */

    @Override
    public void onTransporterClickFromFragment(Transporter transporter) {
        if (!isInLandscape()) {
            mMainDisplayFragment = (MainDisplayFragment) mFragmentManager.findFragmentByTag(MainDisplayFragment.FRAGMENT_TAG);
            if (mMainDisplayFragment == null) {
                mMainDisplayFragment = MainDisplayFragment.createFragment(transporter);
            } else {
                mMainDisplayFragment.setTransporter(transporter);
            }
            mFragmentManager.beginTransaction()
                    .replace(R.id.drawerContainer, mMainDisplayFragment, MainDisplayFragment.FRAGMENT_TAG)
                    .addToBackStack(MAIN_DISPLAY_BACK_ENTRY)
                    .commit();
            mFragmentManager.executePendingTransactions();
        } else {
            mMainDisplayFragment.setTransporter(transporter);
        }
    }

    @Override
    public void onWaitToLoadMainDisplayInPortrait() {
        portraitDisableScrimActions();
    }

    @Override
    public void onFinishToLoadMainDisplayInPortrait(String lineTitle) {
        portraitEnableScrimActions(lineTitle);
    }
}
