package criskey.org.rattarrivals.view.station;

import android.media.Image;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.model.Arrival;
import criskey.org.rattarrivals.model.Station;

/**
 * Created by criskey on 1/3/2016.
 */
public class ArrivalRecyclerAdapter extends RecyclerView.Adapter<ArrivalRecyclerAdapter.ArrivalHolder> {

    private List<IndexedArrival> indexedArrivals;

    private WeakReference<Callback> callbackRef;

    public ArrivalRecyclerAdapter(List<Station> stations, Callback callback) {
        indexedArrivals = new ArrayList<>();
        for (Station station : stations) {
            String stationName = station.getName();
            for (Arrival arrival : station.getArrivals()) {
                indexedArrivals.add(new IndexedArrival(stationName, arrival));
            }
        }
        moveLockedArrivalsTop();
        this.callbackRef = new WeakReference<Callback>(callback);
    }

    @Override
    public int getItemCount() {
        return indexedArrivals.size();
    }

    @Override
    public ArrivalHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_display_arrival, parent, false);
        return new ArrivalHolder(view);
    }

    @Override
    public void onBindViewHolder(ArrivalHolder holder, int position) {
        holder.bind(position);
    }

    private void moveLockedArrivalsTop() {
        Collections.sort(indexedArrivals, new Comparator<IndexedArrival>() {
            @Override
            public int compare(IndexedArrival lhs, IndexedArrival rhs) {
                boolean llocked = lhs.arrival.isLocked();
                boolean rlocked = rhs.arrival.isLocked();
                return llocked == rlocked ? 0 : llocked ? -1 : 1;
            }
        });
    }

    public static interface Callback {

        public void selectRouteArrival(int index);

        public void lockToggleArrival(int position);

    }

    class ArrivalHolder extends RecyclerView.ViewHolder {
        private TextView mTextStationName;
        private TextView mTextLine;
        private TextView mTextArrival;
        private ImageButton mImageLock;
        private ImageView mImageTransporter;

        public ArrivalHolder(View itemView) {
            super(itemView);
            mTextStationName = (TextView) itemView.findViewById(R.id.textStationName);
            mTextLine = (TextView) itemView.findViewById(R.id.textLine);
            mTextArrival = (TextView) itemView.findViewById(R.id.textArrivalTime);
            mImageLock = (ImageButton) itemView.findViewById(R.id.imageButtonLock);
            mImageTransporter = (ImageView) itemView.findViewById(R.id.imageTransporter);
        }

        private void bind(final int position) {
            final IndexedArrival iarr = indexedArrivals.get(position);
            mTextStationName.setText(iarr.stationName);
            mTextLine.setText(iarr.arrival.getTransporter().getLine());
            mTextArrival.setText(iarr.arrival.getTime());
            mImageLock.setImageResource((iarr.arrival.isLocked())
                    ? R.drawable.ic_lock_outline_black_24dp
                    : R.drawable.ic_lock_outline_white_24dp);
            mImageTransporter.setImageResource(iarr.arrival
                    .getTransporter()
                    .getTransporterType()
                    .getImageResId());
            mImageLock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Callback callback = callbackRef.get();
                    if (callback != null) {
                        iarr.arrival.setLocked(!iarr.arrival.isLocked());
                        moveLockedArrivalsTop();
                        notifyDataSetChanged();
                        callback.lockToggleArrival(position);
                    }
                }
            });
        }
    }

    private class IndexedArrival {
        String stationName;
        Arrival arrival;

        public IndexedArrival(String stationName, Arrival arrival) {
            this.arrival = arrival;
            this.stationName = stationName;
        }
    }

}
