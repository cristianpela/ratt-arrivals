package criskey.org.rattarrivals.parse;

import org.jsoup.nodes.Document;

/**
 * Created by criskey on 12/2/2016.
 */
abstract class Parser {

    protected Document document;

    public Parser(AbstractNetworkDataDownloader downloader) throws ParsingException {
        this.document = downloader.load();
    }

    public Parser(String link) throws ParsingException {
        this(new NetworkDataDownloader(link));
    }

}
