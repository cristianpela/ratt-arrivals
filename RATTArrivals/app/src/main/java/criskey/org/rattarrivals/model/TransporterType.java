package criskey.org.rattarrivals.model;

import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;

import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.parse.NetworkUtils;

/**
 * Created by criskey on 26/12/2015.
 */
public enum TransporterType {

    BUS("auto", R.drawable.ic_directions_bus_black_24dp),
    TRAM("tram", R.drawable.ic_directions_railway_black_24dp ),
    TROLLEY("trol",  R.drawable.ic_directions_transit_black_24dp);
    private String location;
    private int resId;
    TransporterType(String location, @DrawableRes int resId) {
        this.location = location + ".php";
        this.resId = resId;
    }

    public Uri getUri() {
        return NetworkUtils.TRANSPORTER_BASE_URI.buildUpon().appendPath(location).build();
    }

    public int getImageResId(){
        return resId;
    }

}
