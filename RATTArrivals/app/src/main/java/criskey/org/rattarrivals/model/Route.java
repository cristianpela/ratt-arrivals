package criskey.org.rattarrivals.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by criskey on 14/1/2016.
 */
public class Route implements Serializable {

    private static final long serialVersionUID = 5788894227341231402L;

    private Transporter transporter;

    private List<Station> fromToStations, toFromStations;

    public Route(Transporter transporter, List<Station> fromToStations, List<Station> toFromStations) {
        this.transporter = transporter;
        this.toFromStations = toFromStations;
        this.fromToStations = fromToStations;
    }

    public String getFromName() {
        return fromToStations.get(0).getName();
    }

    public String getToName() {
        return toFromStations.get(0).getName();
    }

    public List<Station> getFromToStations() {
        return fromToStations;
    }

    public List<Station> getToFromStations() {
        return toFromStations;
    }

    public Transporter getTransporter() {
        return transporter;
    }

    @Override
    public String toString() {
        return getTransporter().getLine() + " "
                + getFromName() + " " + getToName();
    }
}
