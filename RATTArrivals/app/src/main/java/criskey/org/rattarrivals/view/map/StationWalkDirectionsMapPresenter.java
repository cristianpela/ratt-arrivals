package criskey.org.rattarrivals.view.map;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.mvp.BasePresenter;
import criskey.org.rattarrivals.parse.ObservableNetworkDataDownloader;
import criskey.org.rattarrivals.parse.WalkingDirectionsParser;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.exceptions.Exceptions;

/**
 * Created by criskey on 24/3/2016.
 */

public class StationWalkDirectionsMapPresenter extends BasePresenter<StationWalkDirectionsMapView>
        implements ObservableNetworkDataDownloader.Callee<WalkingDirectionsParser.WalkingDirections>,
        Observer<WalkingDirectionsParser.WalkingDirections> {

    private static final long serialVersionUID = -25402924328879759L;

    private List<Location> pathNodes;

    private Location from, to;

    private List<String> directions;

    private String errorMessage;

    private transient Subscription subscription;

    /**
     * testing constructor;
     *
     * @param pathNodes
     */
    public StationWalkDirectionsMapPresenter(double[] pathNodes) {
        // this.pathNodes = pathNodes;
    }

    /**
     * Production constructor;
     */
    public StationWalkDirectionsMapPresenter() {
    }

    @Override
    public void onBind() {
        if (isViewAttached()) {
            if (errorMessage != null)
                getView().onShowError(errorMessage);
            else if (pathNodes != null && directions != null) {
                showPathNodesAndDirections(pathNodes, directions);
            }
        }
    }

    private void showPathNodesAndDirections(List<Location> path, List<String> dirs) {
        getView().onDecodedMapPath(from, to, path);
        getView().onWalkingDirections(dirs);
    }

    public void obtainWalkingDirections(final Location from, final Location to) {
        if (!isViewAttached() || isSameSearch(from, to))
            return;
        this.from = from;
        this.to = to;
        getView().onShowWait();
        subscription = ObservableNetworkDataDownloader.runInParallel(this/*callee impl*/, this/*observer impl*/);
    }

    private boolean isSameSearch(Location newFrom, Location newTo) {
        return (from != null && to != null)  && (newFrom.equals(from) && newTo.equals(to));
    }

    @Override
    public void retryOnError() {
        obtainWalkingDirections(from, to);
    }

    @Override
    public void onUnbind() {
        if (subscription != null)
            subscription.unsubscribe();
    }

    @Override
    public WalkingDirectionsParser.WalkingDirections called() {
        try {
            WalkingDirectionsParser parser = new WalkingDirectionsParser(from, to, "ro");
            return parser.getWalkingDirections();
        } catch (Exception ex) {
            Exceptions.propagate(ex);
        }
        return null; // unreachable
    }

    @Override
    public void onCompleted() {
        if (isViewAttached()) {
            errorMessage = null; // we're good; any prev error is removed
            getView().onHideWait();
        }
    }

    @Override
    public void onError(Throwable e) {
        if (isViewAttached()) {
            errorMessage = e.getMessage();
            getView().onShowError(errorMessage);
        }
    }

    @Override
    public void onNext(WalkingDirectionsParser.WalkingDirections walkingDirections) {
        if (isViewAttached()) {
            pathNodes = walkingDirections.getPathNodes();
            directions = walkingDirections.getDirections();
            showPathNodesAndDirections(pathNodes, directions);
        }
    }
}
