package criskey.org.rattarrivals.view.map;

import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.mvp.BasePresenter;
import criskey.org.rattarrivals.parse.ObservableNetworkDataDownloader;
import criskey.org.rattarrivals.parse.RemoteDBBackup;
import criskey.org.rattarrivals.repository.RepositoryManager;
import criskey.org.rattarrivals.repository.StationRepository;
import criskey.org.rattarrivals.util.Logger;
import rx.exceptions.Exceptions;
import rx.functions.Action1;

/**
 * Created by criskey on 9/3/2016.
 */
public class StationMapPresenter extends BasePresenter<StationMapView> {

    private static final long serialVersionUID = 5L;

    private static final double PROXIMITY = 0.0001;

    private ArrayList<Station> stations;

    private transient boolean[] updatedStationLocIndexes;

    private Location referenceLocation;

    private double offset = PROXIMITY;

    private transient StationRepository stationRepository;

    private transient DecimalFormat decimalFormat;

    private transient boolean isLocationUpdated;

    private transient boolean isLocationSaved;

    public StationMapPresenter() {
        initDecimalFormat();
        referenceLocation = new Location(45.774122, 21.244131);// default location;
    }

    private void initDecimalFormat() {
        decimalFormat = new DecimalFormat();
        decimalFormat.setMaximumFractionDigits(6);
        decimalFormat.setMinimumFractionDigits(0);
        decimalFormat.setGroupingUsed(false);
    }


    @Override
    public void unbindView() {
        updatedStationLocIndexes = null;
        super.unbindView();
    }

    @Override
    public void onBind() {
        if (decimalFormat == null)
            initDecimalFormat();
        stationRepository = (StationRepository) getRepositoryManager().getRepository(RepositoryManager.STATION_REPOSITORY);
    }

    public void setup(ArrayList<Station> stations, Location referenceLocation) {
        if (stations == null && referenceLocation == null) {
            initStations();
        } else {
            this.stations = stations;
            this.referenceLocation = referenceLocation;
        }
        updatedStationLocIndexes = new boolean[stations.size()];
    }

    /**
     * This method is for isolated testing purposes only
     */
    private void initStations() {
        stations = (ArrayList) stationRepository.findAllByNameOrAlias("Scari");
        for (Station station : stations) {
            stationRepository.findJoinedStation(station);
        }
        Location firstLocation = stations.get(0).getLocation();
        if (!firstLocation.equals(Location.EMPTY_LOCATION))
            referenceLocation = firstLocation;
    }

    public void placeMarkers() {
        if (!isViewAttached())
            return;
        List<Station> sendStations = new ArrayList<>(stations);
        for (int i = 0; i < sendStations.size(); i++) {
            Station station = sendStations.get(i);
            Location location = station.getLocation();
            if (location == null) { // we're coming from mainRouteDisplay [those stations have no id or location
                Station dbStation = stationRepository.findAllByNameOrAlias(station.getName()).get(0);
                station.setId(dbStation.getId());
                location = dbStation.getLocation();
            }
            if (location.equals(Location.EMPTY_LOCATION)) {
                station.setLocation(getProximityLatLng(i));
            } else {
                station.setLocation(location);
            }
        }
        getView().onPlaceMarkers(sendStations);
    }

    @NonNull
    private Location getProximityLatLng(int i) {
        int sign = (i % 2 == 0) ? -1 : 1;
        offset = offset * sign + PROXIMITY;
        double lat = referenceLocation.getLatitude() + offset;
        double longi = referenceLocation.getLongitude() + offset;
        Location loc = new Location(lat, longi);
        referenceLocation = loc;
        return loc;
    }


    public void updateCurrentMarker(Location location, String title, String snippet) {
        addUpdatedStationLocIndex(location, title);
        showCurrentMarkerDetails(location, title, snippet);
        if (isViewAttached()) {
            getView().onShowButtonCommit(true);
        }
    }

    public void showCurrentMarkerDetails(Location location, String title, String snippet) {
        referenceLocation = location;
        if (isViewAttached()) {
            String formatCoordinate = "("
                    + decimalFormat.format(location.getLatitude()) + " "
                    + decimalFormat.format(location.getLongitude()) + ")";
            snippet = snippet.replaceAll("\\n", "<br>");
            getView().onShowMarkerDetail(title, snippet, formatCoordinate);
        }
    }

    @NonNull
    private void addUpdatedStationLocIndex(Location location, String title) {
        int index = -1;
        for (int i = 0; i < stations.size(); i++) {
            Station station = stations.get(i);
            if (station.getName().equals(title)) {
                station.setLocation(location);
                index = i;
                break;
            }
        }
        assert (index > -1);
        updatedStationLocIndexes[index] = true;
        isLocationUpdated = true;
    }

    public void triggerStreetView() {
        if (isViewAttached())
            getView().onShowStreetView(referenceLocation);
    }

    public boolean isLocationSaved() {
        return isLocationSaved;
    }

    public boolean isLocationUpdated() {
        return isLocationUpdated;
    }

    public String getTransportersSnippet(Station station) {
        StringBuilder builder = new StringBuilder();
        for (Transporter transporter : station.getTransporters()) {
            builder.append(transporter.getLine()).append("\n");
        }
        return builder.toString();
    }

    public void commitUpdates() {
        for (int i = 0; i < updatedStationLocIndexes.length; i++) {
            if (updatedStationLocIndexes[i]) {
                Station station = stations.get(i);
                stationRepository.updateLocation(station, station.getLocation());
                commitRemoteUpdate(station);
            }
        }
        getView().onShowButtonCommit(false);
        isLocationSaved = true;
    }


    private void commitRemoteUpdate(final Station station) {
        ObservableNetworkDataDownloader.Callee<Void> callee = new ObservableNetworkDataDownloader.Callee<Void>() {
            @Override
            public Void called() {
                final RemoteDBBackup rmtDb = new RemoteDBBackup();
                rmtDb.execute(new RemoteDBBackup.Query() {
                    @Override
                    public void exec() throws IOException, JSONException {
                        Logger.log("Saving location on remote database for station: " + station.getName());
                        rmtDb.upsertLocation(station);
                    }

                    @Override
                    public void onError(Exception e) {
                        Exceptions.propagate(e);
                    }
                });
                return null;
            }
        };
        ObservableNetworkDataDownloader.runInParallel(callee);
    }

    public ArrayList<Location> getUpdatedLocations() {
        ArrayList<Location> locations = new ArrayList<>(stations.size());
        for (int i = 0; i < stations.size(); i++) {
            Station station = stations.get(i);
            locations.add(station.getLocation());
        }
        return locations;
    }


}
