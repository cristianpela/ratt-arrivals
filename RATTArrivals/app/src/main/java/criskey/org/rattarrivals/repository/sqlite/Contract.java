package criskey.org.rattarrivals.repository.sqlite;

import android.content.UriMatcher;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;

/**
 * Contract for sqlite db tables
 * Created by criskey on 8/2/2016.
 */
public class Contract {

    public static final String AUTHORITY = "org.criskey.rattarivals.auth";

    public static final Uri BASE_CONTENT_URI = new Uri.Builder().scheme("content").authority(AUTHORITY).build();

    public static final Uri STATION_URI = BASE_CONTENT_URI.buildUpon().appendPath(Contract.Station.TITLE).build();
    public static final Uri STATION_URI_DISTINCT = STATION_URI.buildUpon().appendPath("distinct").build();
    public static final Uri TRANSPORTER_URI = BASE_CONTENT_URI.buildUpon().appendPath(Contract.Transporter.TITLE).build();
    public static final Uri JOIN_URI = BASE_CONTENT_URI.buildUpon().appendPath(Contract.JoinStationTransporter.TITLE).build();

    public static final int STATION_MATCH_ID = 100;
    public static final int STATION_MATCH_DISTINCT_ID = 101;
    public static final int TRANSPORTER_MATCH_ID = 200;
    public static final int JOIN_MATCH_ID = 300;
    public static final int SINGLE_JOIN_MATCH_ID = 301;
    public static final String NAME = "name";
    static final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        matcher.addURI(AUTHORITY, Contract.Station.TITLE, STATION_MATCH_ID);
        matcher.addURI(AUTHORITY, Contract.Station.TITLE + "/distinct", STATION_MATCH_DISTINCT_ID);
        matcher.addURI(AUTHORITY, Contract.Transporter.TITLE, TRANSPORTER_MATCH_ID);
        matcher.addURI(AUTHORITY, Contract.JoinStationTransporter.TITLE, JOIN_MATCH_ID);
        matcher.addURI(AUTHORITY, Contract.JoinStationTransporter.TITLE + "/#", SINGLE_JOIN_MATCH_ID);
    }


    private Contract() {
    }

    public static final class Station extends Contract implements BaseColumns {
        public static final String ALIAS = "alias";
        public static final String TITLE = "station";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
    }

    public static final class Transporter extends Contract {
        //TODO rename transporter table id column to 'tr_id';
        public static final String _ID = "st_id";
        public static final String FAVORITE = "favorite";
        public static final String TYPE = "type";
        public static final String TITLE
                = "transporter";
        public static final String LINK
                = "link";
    }

    public static final class JoinStationTransporter extends Contract implements BaseColumns {
        public static final String FK_STATION = "fk_" + Station.TITLE;
        public static final String FK_TRANSPORTER = "fk_" + Transporter.TITLE;
        public static final String
                TITLE = Station.TITLE + "_" + Transporter.TITLE;
    }
}
