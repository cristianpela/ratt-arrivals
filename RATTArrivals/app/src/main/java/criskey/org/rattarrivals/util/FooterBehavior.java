package criskey.org.rattarrivals.util;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

/**
 * Created by criskey on 5/2/2016.
 */
public class FooterBehavior extends CoordinatorLayout.Behavior<LinearLayout> {

    public FooterBehavior() {
    }

    public FooterBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, LinearLayout child, View dependency) {

        return dependency instanceof LinearLayout;
    }

    @Override
    public void onNestedScroll(CoordinatorLayout coordinatorLayout,
                               LinearLayout child,
                               View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {

    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, LinearLayout child, View dependency) {
       // criskey.org.rattarrivals.util.Logger.log(this,"Main Changed " + dependency);
        return true;
    }

    @Override
    public boolean onNestedFling(CoordinatorLayout coordinatorLayout,
                                 LinearLayout child, View target, float velocityX, float velocityY, boolean consumed) {

        return true;
    }

    @Override
    public boolean onTouchEvent(CoordinatorLayout parent, LinearLayout child, MotionEvent ev) {

        return true;
    }
}
