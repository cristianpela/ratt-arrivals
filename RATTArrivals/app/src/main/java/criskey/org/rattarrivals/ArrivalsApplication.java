package criskey.org.rattarrivals;

import android.app.Application;
import android.app.NotificationManager;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v4.app.NotificationCompat;


import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.model.TransporterType;
import criskey.org.rattarrivals.repository.PreferencesRepository;
import criskey.org.rattarrivals.repository.Repository;
import criskey.org.rattarrivals.repository.RepositoryManager;
import criskey.org.rattarrivals.repository.TransporterRepository;
import criskey.org.rattarrivals.mvp.Presenter;
import criskey.org.rattarrivals.mvp.PresenterManager;
import io.fabric.sdk.android.Fabric;

/**
 * Created by criskey on 28/12/2015.
 */
public class ArrivalsApplication extends Application {


    private RepositoryManager mRepositoryManager;
    private PresenterManager mPresenterManager;

    /*
    private RefWatcher refWatcher;
    public static RefWatcher getRefWatcher(Context context) {
        ArrivalsApplication application = (ArrivalsApplication) context.getApplicationContext();
        return application.refWatcher;
    }*/

    @Override
    public void onCreate() {
        super.onCreate();
        //uncomment this for emulator/real device test
        //setUpDatabaseForTesting();
        //setLanguage(null);

        //refWatcher = LeakCanary.install(this);
        mRepositoryManager = RepositoryManager.getInstance(getApplicationContext());
        mPresenterManager = PresenterManager.getInstance(getFilesDir());
        // setCurrentLanguage();

        Fabric.with(this, new Crashlytics());
    }

    /**
     * 3 Buses<br>
     * 5 Trams<br>
     * 4 Trolleys<br>
     * 5 Favorites<br>
     */
    public void setUpDatabaseForTesting() {
        TransporterRepository transporterRepository = (TransporterRepository) getRepository(RepositoryManager.TRANSPORTER_REPOSITORY);
        PreferencesRepository preferencesRepository = (PreferencesRepository) getRepository(RepositoryManager.PREFERENCES_REPOSITORY);
        if (!preferencesRepository.hasData()) {
            transporterRepository.createMany(dummyInitDatabase());
            preferencesRepository.validateData(true);
        }
    }

    private List<Transporter> dummyInitDatabase() {
        List<Transporter> transporterList = new ArrayList<>();
        Transporter line40 = new Transporter(TransporterType.BUS, "","Linia 40", 40, true);
        transporterList.add(line40);
        transporterList.add(new Transporter(TransporterType.BUS,"", "Linia 33", 33, false));
        transporterList.add(new Transporter(TransporterType.BUS, "","Linia 46", 46, false));

        transporterList.add(new Transporter(TransporterType.TRAM,"", "Linia 1", 1, true));
        transporterList.add(new Transporter(TransporterType.TRAM, "","Linia 2", 2, false));
        transporterList.add(new Transporter(TransporterType.TRAM,"", "Linia 4", 4, false));
        transporterList.add(new Transporter(TransporterType.TRAM,"", "Linia 5", 5, true));
        transporterList.add(new Transporter(TransporterType.TRAM, "","Linia 9", 9, false));

        transporterList.add(new Transporter(TransporterType.TROLLEY,"", "Linia 14", 14, true));
        transporterList.add(new Transporter(TransporterType.TROLLEY,"", "Linia 11", 11, false));
        transporterList.add(new Transporter(TransporterType.TROLLEY,"", "Linia 13", 13, true));
        transporterList.add(new Transporter(TransporterType.TROLLEY,"", "Linia 18", 18, false));
        return transporterList;
    }

    public <P extends Presenter> PresenterManager<P> getPresenterManager() {
        return mPresenterManager;
    }

    public Repository getRepository(@RepositoryManager.RepoID int repositoryId) {
        return mRepositoryManager.getRepository(repositoryId);
    }

    public RepositoryManager getRepositoryManager() {
        return mRepositoryManager;
    }

    public SharedPreferences getArrivalsPreferences() {
        return getSharedPreferences("transporter_repo", Context.MODE_PRIVATE);
    }

    public void setLanguage(String language) {
        Configuration configuration = getResources().getConfiguration();
        configuration.locale = new Locale("ro");
        getResources().updateConfiguration(configuration, null);
    }

    private void setCurrentLanguage() {
        PreferencesRepository preferencesRepository = (PreferencesRepository) mRepositoryManager.getRepository(RepositoryManager.PREFERENCES_REPOSITORY);
        preferencesRepository.makeCurrentLanguageAvailable();
    }

    @Override
    public void onTrimMemory(int level) {
        if (level >= ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW) {}
        super.onTrimMemory(level);
    }

    @Override
    public void onLowMemory() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setContentTitle("R.A.T.T Memory Notification")
                .setContentText("Clearing cached presenters and repositories")
                .setSmallIcon(R.drawable.ic_directions_bus_white_24dp);
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(001, builder.build());
        mPresenterManager.removeCachedPresenters();
        mRepositoryManager.removeCachedRepositories();
        super.onLowMemory();
    }
}
