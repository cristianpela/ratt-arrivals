package criskey.org.rattarrivals.parse;

import android.net.Uri;

/**
 * Created by criskey on 26/12/2015.
 */
public final class NetworkUtils {

    public static final Uri BASE_URI = Uri.parse("http://86.122.170.105:61978");

    public static final Uri TRANSPORTER_BASE_URI = BASE_URI.buildUpon().appendPath("html").appendPath("timpi").build();

    private static final Uri BASE_URI_TXT = Uri.parse("http://ratt.ro/txt");

    public static final String STATIONS_LINK = BASE_URI_TXT.buildUpon().appendPath("select_statie.php").build().toString();
    //"http://ratt.ro/txt/select_statie.php";

    private NetworkUtils() {
    }

    public static final String getSingleStationLink(String stationId) {
        return BASE_URI_TXT.buildUpon().appendPath("select_traseu.php").appendQueryParameter("id_statie", stationId).build().toString();
        //return "http://ratt.ro/txt/select_traseu.php?id_statie=" + stationId;
    }

    public static final String getArrivalsTimeLink(String stationId, String transporterId) {
        return BASE_URI_TXT.buildUpon().appendPath("afis_msg.php")
                .appendQueryParameter("id_traseu", transporterId)
                .appendQueryParameter("id_statie", stationId)
                .build().toString();
        //return "http://ratt.ro/txt/afis_msg.php?id_traseu=" + transporterId + "&id_statie=" + stationId;
    }

    /**
     * It changes often...
     *
     * @param trailHref
     * @return
     */
    public static final String getTransporterLink(String trailHref) {
        return TRANSPORTER_BASE_URI.toString() + "/" + trailHref;
    }

}
