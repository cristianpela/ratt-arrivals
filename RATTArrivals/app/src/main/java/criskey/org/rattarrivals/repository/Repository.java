package criskey.org.rattarrivals.repository;

import java.util.List;

/**
 * Base generic interface for all repositories. Has basic CRUD operations
 * Takes types for primary key (Pk) and Entity (E)
 * Created by criskey on 28/1/2016.
 */
public interface Repository<Pk, E> {

    public E findOne(Pk primaryKey);

    public List<E> findAll();

    public boolean createOne(E entity);

    public boolean createMany(List<E> entities);

    public boolean deleteAll();

    public boolean update(E entity);

}
