package criskey.org.rattarrivals.view.station;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.mvp.BaseFragment;
import criskey.org.rattarrivals.util.Utils;
import criskey.org.rattarrivals.util.WaitNotifyContentDelegate;
import criskey.org.rattarrivals.view.map.StationMapsActivity;
import criskey.org.rattarrivals.view.map.StationStreetViewDialog;

/**
 * Created by criskey on 25/2/2016.
 */
public class StationArrivalsFragment extends BaseFragment<StationArrivalsPresenter>
        implements StationArrivalsView,
        UnderAliasRecyclerAdapter.Callback,
        ArrivalRecyclerAdapter.Callback {

    public static final String TAG = StationArrivalsFragment.class.getSimpleName();
    public static final String ARG_STATION_NAME = "ARG_STATION_NAME";
    private static final String SEARCH_STREET_GEOCODE_TAG = "SEARCH_STREET_GEOCODE_TAG";
    private static final int ACT_MAP_REQUEST_CODE = 1066;

    private WaitNotifyContentDelegate mWaitNotifyViewDelegate;
    private TextView mTextSearchStationName;
    private RecyclerView mRecyclerUnderAlias;
    private RecyclerView mRecyclerArrivals;
    private ImageButton mButtonExpand;
    private SearchStreetGeocodeDialog mStreetGeocodeDialog;

    public static StationArrivalsFragment create(String stationName) {
        StationArrivalsFragment arrivalsFragment = new StationArrivalsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_STATION_NAME, stationName);
        arrivalsFragment.setArguments(bundle);
        return arrivalsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.station_frag_arrivals, container, false);
        mTextSearchStationName = (TextView) view.findViewById(R.id.textSearchStationName);

        Toolbar bottomToolbar = (Toolbar) view.findViewById(R.id.toolbarStationActions);
        bottomToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return onBottomToolbarMenuActionClick(item);
            }
        });
        bottomToolbar.inflateMenu(R.menu.menu_action_station);

        mRecyclerUnderAlias = (RecyclerView) view.findViewById(R.id.recyclerViewUnderAlias);
        mRecyclerUnderAlias.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        mRecyclerArrivals = (RecyclerView) view.findViewById(R.id.recyclerStationArrival);
        mRecyclerArrivals.setLayoutManager(new LinearLayoutManager(getActivity()));

        mButtonExpand = (ImageButton) view.findViewById(R.id.buttonExpandAlias);
        mButtonExpand.setOnClickListener(new View.OnClickListener() {
            boolean expanded;

            @Override
            public void onClick(View v) {
                expanded = !expanded;
                ImageButton thisButton = (ImageButton) v;
                thisButton.setImageResource((expanded) ? R.drawable.ic_keyboard_arrow_up_black_36dp
                        : R.drawable.ic_keyboard_arrow_down_black_36dp);
                Utils.toggleVisibilityGone(mRecyclerUnderAlias);
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        WaitNotifyContentDelegate.Adapter adapter = WaitNotifyContentDelegate.AdapterStrategy.activityAdapter(getActivity(),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getPresenter().retryOnError();
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mWaitNotifyViewDelegate.onHideWait();
                    }
                });
        mWaitNotifyViewDelegate = new WaitNotifyContentDelegate(adapter);
    }

    @Override
    protected void onArgumentsAttached(Bundle args) {
        String stationName = args.getString(ARG_STATION_NAME);
        assert (stationName != null) : "passed argument to fragment 'station name' must not be null";
        searchArrivalsByStationNameOrAlias(stationName);
    }

    public void searchArrivalsByStationNameOrAlias(String stationName) {
        getPresenter().searchArrivalsByStationNameOrAlias(stationName);
        mTextSearchStationName.setText(stationName);
    }

    @Override
    public void onShowWait() {
        mWaitNotifyViewDelegate.onShowWait();
    }

    @Override
    public void onShowError(String errorMessage) {
        mWaitNotifyViewDelegate.onShowError(errorMessage);
    }

    @Override
    public void onShowProgress(@StringRes int res) {
        mWaitNotifyViewDelegate.setMessageStringRes(res);
    }

    @Override
    public void onHideWait() {
        mWaitNotifyViewDelegate.onHideWait();
    }

    @Override
    public void onPresenterAttached() {

    }

    @Override
    public void onShowStationArrivals(String stationNameOrAlias,
                                      boolean hasAlias,
                                      List<String> stationsUnderAlias,
                                      List<Station> stations) {
        mTextSearchStationName.setText(stationNameOrAlias);
        if (hasAlias) {
            mRecyclerUnderAlias.setAdapter(new UnderAliasRecyclerAdapter(stationsUnderAlias, this));
            mButtonExpand.setVisibility(View.VISIBLE);
        } else {
            mButtonExpand.setVisibility(View.GONE);
            mRecyclerUnderAlias.setVisibility(View.GONE);
        }
        mRecyclerArrivals.setAdapter(new ArrivalRecyclerAdapter(stations, this));
    }

    @Override
    public void onShowWaitGeocode() {
        if (mStreetGeocodeDialog != null)
            mStreetGeocodeDialog.onShowWait();
    }

    @Override
    public void onShowErrorGeocode(String error) {
        if (mStreetGeocodeDialog != null)
            mStreetGeocodeDialog.onShowError(error);
    }

    @Override
    public void onFoundGeocode(String formattedLocation) {
        if (mStreetGeocodeDialog != null)
            mStreetGeocodeDialog.onFoundLocation(formattedLocation);
    }

    @Override
    public void onRemoteLocationResult(Location location) {
        if(location.equals(Location.EMPTY_LOCATION)){
            Toast.makeText(getContext(), R.string.search_street_view_empty_error, Toast.LENGTH_SHORT).show();
            FragmentManager fragmentManager = getFragmentManager();
            mStreetGeocodeDialog = (SearchStreetGeocodeDialog) fragmentManager.findFragmentByTag(SEARCH_STREET_GEOCODE_TAG);
            if (mStreetGeocodeDialog == null)
                mStreetGeocodeDialog = SearchStreetGeocodeDialog.newInstance(getPresenter().getStationsNames()[0]);
            mStreetGeocodeDialog.attachPresenter(getPresenter());
            mStreetGeocodeDialog.show(fragmentManager, SEARCH_STREET_GEOCODE_TAG);
        }else{
            getPresenter().openMapActivity(null);
        }

    }

    @Override
    public void onOpenMapActivity(double latitude, double longitude, ArrayList<Station> stations) {
        if (mStreetGeocodeDialog != null && mStreetGeocodeDialog.isVisible())
            mStreetGeocodeDialog.dismiss();
        Intent intent = StationMapsActivity.createStationLocationIntent(getContext(), new LatLng(latitude, longitude), stations);
        startActivityForResult(intent, ACT_MAP_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACT_MAP_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                ArrayList<LatLng> updatedLatLngs = data.getParcelableArrayListExtra(StationMapsActivity.COM_NEW_LOCATION);
                getPresenter().updateStationLocation(updatedLatLngs);
            }
        }
    }

    @Override
    public void onFirstTimeIntent() {

    }

    @Override
    public void lockToggleArrival(int position) {
        mRecyclerArrivals.scrollToPosition(0);
    }

    @Override
    public void selectRouteArrival(int index) {

    }

    @Override
    public void onDestroy() {
        if (mWaitNotifyViewDelegate != null)
            mWaitNotifyViewDelegate.destroy();
        super.onDestroy();
    }

    public boolean onBottomToolbarMenuActionClick(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_station_refresh: {
                getPresenter().refresh();
                break;
            }
            case R.id.action_station_alias: {
                buildEnterableAliasDialog(-1);// update alias for all
                break;
            }
            case R.id.action_station_map: {
                showStationOnMap();
                break;
            }
            case R.id.action_station_street_view: {
                showStationOnStreetView();
                break;
            }
        }

        return true;
    }

    private void showStationOnStreetView() {
        if (getPresenter().hasLocation()) {
            String[] stationNames = getPresenter().getStationsNames();
            if (stationNames.length == 1) {
                openStationStreetViewFragment(getPresenter().getLocationByStationIndex(0));
            } else {
                final AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                        .setCancelable(true)
                        .setSingleChoiceItems(stationNames, 0, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                openStationStreetViewFragment(getPresenter().getLocationByStationIndex(which));
                            }
                        }).create();
                alertDialog.show();
            }
        } else {
            getPresenter().checkRemoteDBForLocation();
        }
    }


    private void openStationStreetViewFragment(Location location) {
        if (location.equals(Location.EMPTY_LOCATION)) {
            Toast.makeText(getContext(), R.string.search_street_view_empty_error, Toast.LENGTH_SHORT).show();
            return;
        }

        StationStreetViewDialog streetViewDialog = (StationStreetViewDialog) getFragmentManager().findFragmentByTag(StationStreetViewDialog.TAG);
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        if (streetViewDialog == null) {
            streetViewDialog = StationStreetViewDialog.newInstance(latLng);
        } else
            streetViewDialog.setLocation(latLng);
        if (!streetViewDialog.isAdded())
            streetViewDialog.show(getFragmentManager(), StationStreetViewDialog.TAG);
    }

    private void buildEnterableAliasDialog(final int index) {
        final AutoCompleteTextView editAlias = new AutoCompleteTextView(getContext());
        editAlias.setAdapter(new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line,
                getPresenter().getAllAliases()));
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        editAlias.setLayoutParams(lp);
        editAlias.requestFocus();
        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                .setTitle("Enter an alias (a group name)")
                .setView(editAlias)
                .setCancelable(true)
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String alias = editAlias.getText().toString();
                        getPresenter().updateAlias(alias, index);
                        mTextSearchStationName.setText(alias);
                    }
                }).create();
        alertDialog.show();
    }


    private void showStationOnMap() {
        if (getPresenter().hasLocation()) {
            getPresenter().openMapActivity(null);
        } else {
           getPresenter().checkRemoteDBForLocation();
        }
    }

    @Override
    public void removeAlias(int position) {
        getPresenter().removeStationFromAlias(position);
    }

    @Override
    public void editAlias(int position) {
        buildEnterableAliasDialog(position);
    }
}
