package criskey.org.rattarrivals.view.map;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.mvp.BaseActivity;
import criskey.org.rattarrivals.util.MapUtil;
import criskey.org.rattarrivals.util.Utils;
import criskey.org.rattarrivals.util.WaitNotifyContentDelegate;


/**
 * Created by criskey on 24/3/2016.
 */
public class StationWalkDirectionsMapActivity extends BaseActivity<StationWalkDirectionsMapPresenter> implements
        StationWalkDirectionsMapView,
        OnMapReadyCallback {

    private static final int ZOOM = 16;

    private static final String KEY_FROM = "KEY_FROM";
    private static final String KEY_TO = "KEY_TO";
    private GoogleMap mMap;
    private WaitNotifyContentDelegate mWaitNotifyContentDelegate;
    private TextView mTextDirections;
    private View mScrollDirections;
    private LatLng mFrom, mTo;
    private List<LatLng> mPath;

    public static Intent createIntent(Context context, LatLng from, LatLng to) {
        return new Intent(context, StationWalkDirectionsMapActivity.class)
                .putExtra(KEY_FROM, from)
                .putExtra(KEY_TO, to);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // force this mTo be portrait only
        setContentView(R.layout.activity_direction_maps);

        mFrom = getIntent().getParcelableExtra(KEY_FROM);
        mTo = getIntent().getParcelableExtra(KEY_TO);

        mTextDirections = (TextView) findViewById(R.id.textDirections);
        mScrollDirections = findViewById(R.id.scrollWalkingDirections);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        initWaitNotifyContentDelegate();
    }

    public void actionSlide(View button) {
        Utils.toggleVisibilityGone(mScrollDirections);

        float rotationWise = (mScrollDirections.getVisibility() == View.VISIBLE) ? 1 : 2;

        button.animate().rotation(180 * rotationWise).setDuration(500).start();
    }

    @Override
    public void onPresenterAttached() {
        getPresenter().obtainWalkingDirections(MapUtil.toLocation(mFrom), MapUtil.toLocation(mTo));
    }

    @Override
    public void onDecodedMapPath(Location from, Location to, List<Location> path) {
        if (mMap != null) {
            drawPolyline(MapUtil.toLatLng(from), MapUtil.toLatLng(to), MapUtil.toLatLngList(path));
            this.mPath = null;
        } else {
            this.mPath = MapUtil.toLatLngList(path); // post pone mTo draw when map is initialized;
        }
    }

    private void drawPolyline(LatLng from, LatLng to, List<LatLng> path) {
        mMap.clear();
        mMap.addMarker(new MarkerOptions()
                .position(from)
                .title(getResources().getString(R.string.map_from)));
        mMap.addMarker(new MarkerOptions()
                .position(to)
                .title(getResources().getString(R.string.map_to)));
        mMap.addPolyline(new PolylineOptions().addAll(path).width(5)
                .color(Color.RED));
    }

    @Override
    public void onWalkingDirections(List<String> directions) {
        //TODO upgrade textDirection to a ListView?
        StringBuilder dirBuilder = new StringBuilder();
        for (int i = 0, s = directions.size(); i < s; i++) {
            String dir = directions.get(i);
            dirBuilder.append(dir + ((i != s - 1) ? "<br><br>" : ""));
        }
        mTextDirections.setText(Html.fromHtml(dirBuilder.toString()));
    }

    @Override
    public void onShowWait() {
        mWaitNotifyContentDelegate.onShowWait();
    }

    @Override
    public void onHideWait() {
        mWaitNotifyContentDelegate.onHideWait();
    }

    @Override
    public void onShowError(String errorMessage) {
        mWaitNotifyContentDelegate.onShowError(errorMessage);
    }

    @Override
    public void onShowProgress(@StringRes int res) {

    }

    @Override
    protected void onDestroy() {
        mWaitNotifyContentDelegate.destroy();
        super.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        LatLng center = MapUtil.middlePoint(mFrom, mTo);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(mFrom)
                .zoom(ZOOM).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        if (mPath != null) {
            drawPolyline(mFrom, mTo, mPath);
        }
    }

    private void initWaitNotifyContentDelegate() {
        WaitNotifyContentDelegate.Adapter adapter = WaitNotifyContentDelegate.AdapterStrategy.activityAdapter(this,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mWaitNotifyContentDelegate.onHideWait();
                    }
                });
        mWaitNotifyContentDelegate = new WaitNotifyContentDelegate(adapter);
    }
}
