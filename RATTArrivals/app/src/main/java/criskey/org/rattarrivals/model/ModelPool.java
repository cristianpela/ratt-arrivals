package criskey.org.rattarrivals.model;

import android.support.v4.util.Pools;

/**
 * Created by criskey on 23/2/2016.
 */
public class ModelPool {

    private static final Pools.SynchronizedPool sPool =
            new Pools.SynchronizedPool(15);

    public static Model acquire() {
        return (Model) sPool.acquire();
    }


}
