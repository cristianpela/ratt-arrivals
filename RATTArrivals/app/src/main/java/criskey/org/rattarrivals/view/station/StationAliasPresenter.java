package criskey.org.rattarrivals.view.station;

import java.util.List;

import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.mvp.BasePresenter;
import criskey.org.rattarrivals.repository.RepositoryManager;
import criskey.org.rattarrivals.repository.StationRepository;

/**
 * Created by criskey on 2/3/2016.
 */
public class StationAliasPresenter extends BasePresenter<StationAliasView> {

    private static final long serialVersionUID = 7L;

    private List<Station> stationsInAlias;

    private Station candidate;

    private String alias;

    private StationRepository repository;

    @Override
    public void onBind() {
        repository = (StationRepository) getRepositoryManager().getRepository(RepositoryManager.STATION_REPOSITORY);
    }

    public void setAlias(String alias){
        this.alias = alias;
    }

    public void initStationForAlias(String stationID){
        candidate = repository.findOne(stationID);
    }

    public void findStationsInAlias(String alias){
        stationsInAlias = repository.findAllByNameOrAlias(alias);
        stationsInAlias.add(candidate);
    }

    public void deleteStationUnderAlias(int index){
        Station toRemove = stationsInAlias.get(index);
        stationsInAlias.remove(toRemove);
        repository.updateAlias(toRemove, null);
    }

    public void addStationUnderAlias(Station station){
        stationsInAlias.add(station);
        repository.updateAlias(station, alias);
    }
}
