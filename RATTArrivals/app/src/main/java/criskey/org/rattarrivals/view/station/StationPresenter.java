package criskey.org.rattarrivals.view.station;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import criskey.org.rattarrivals.model.Arrival;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.mvp.BasePresenter;
import criskey.org.rattarrivals.parse.ArrivalTimeParser;
import criskey.org.rattarrivals.parse.ObservableNetworkDataDownloader;
import criskey.org.rattarrivals.parse.ParsingException;
import criskey.org.rattarrivals.parse.StationParser;
import criskey.org.rattarrivals.repository.RepositoryManager;
import criskey.org.rattarrivals.repository.StationRepositoryImpl;
import criskey.org.rattarrivals.repository.TransporterRepository;
import criskey.org.rattarrivals.repository.sqlite.Contract;
import criskey.org.rattarrivals.repository.sqlite.DummyCursor;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.Exceptions;
import rx.schedulers.Schedulers;

/**
 * Use case scenario<br>
 * 1. User clicks on station<br>
 * 2. App query after the name of clicked station. It could return more than one station. For
 * each (if is the case) follow the next steps:<br>
 * 3. App check if station is joined<br>
 * 3.1 If not, a network call to get transporters that arrive in that station [StationParser invoked]<br>
 * 3.2 Create a join with each transporter(id) received over network<br>
 * 4. If so, query all the joins with that station. Each join contain a transporter. which<br>
 * will be added to station's transporters Station#getTransporters().add(...)<br>
 * 5. For each station transporter, call network for arrival time [ArrivalTimeParser invoked]<br>
 * <p/>
 * Created by criskey on 15/2/2016.
 */
public class StationPresenter extends BasePresenter<StationView> {

    private static final long serialVersionUID = -771530544053147348L;

    private transient StationRepositoryImpl repository;

    private String lastSearch;

    public StationPresenter() {
        forbidSerialization();
    }

    @Override
    public void onBind() {
        repository = (StationRepositoryImpl) getRepositoryManager().getRepository(RepositoryManager.STATION_REPOSITORY);
        if(isViewAttached()) {
            getView().onFirstTimeIntent();
        }
    }

    public CursorAdapter getSearchAdapter() {
        return new SearchStationAdapter2((Context) getView());
    }

    /**
     * Return a station name or alias. Note that is alias is preferred first.
     *
     * @param cursor
     * @return station name or alias
     */
    public String getSearchedStationNameOrAliasAt(Cursor cursor) {
        String alias = cursor.getString(cursor.getColumnIndexOrThrow(Contract.Station.ALIAS));
        if (alias != null)
            return alias;
        return cursor.getString(cursor.getColumnIndexOrThrow(Contract.Station.NAME));
    }


    private class SearchStationAdapter2 extends SimpleCursorAdapter {
        public SearchStationAdapter2(Context context) {
            super(context, android.R.layout.simple_list_item_2, null,
                    new String[]{Contract.Station.NAME, Contract.Station.ALIAS, Contract.Station._ID},
                    new int[]{android.R.id.text1, android.R.id.text2},
                    CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        }

        @Override
        public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
            Cursor c = (constraint == null) ? new DummyCursor() : repository.findAllLikeCursor(constraint.toString());
            return c;
        }

    }
}
