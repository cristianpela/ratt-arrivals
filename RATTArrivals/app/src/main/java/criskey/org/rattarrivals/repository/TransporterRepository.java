package criskey.org.rattarrivals.repository;

import java.util.List;

import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.model.TransporterType;

/**
 * Created by criskey on 4/1/2016.
 */
public interface TransporterRepository extends Repository<Long, Transporter> {


    /**
     * @param transporterType [BUS, TRAM, TROLLEY]
     * @return transporters filtered by type
     */
    public List<Transporter> getTransportersByType(TransporterType transporterType);

    /**
     * @return transporters marked as favorite
     */
    public List<Transporter> getTransportersByFavorite();

    /**
     * Mark/Unmark transporter as favorite
     *
     * @param transporter
     */
    public void changeFavorite(Transporter transporter);

    /**
     * History of visited transporter routes
     * @return
     */
    public List<Transporter> getTransporterHistory();

    /**
     * ad to history a transporter route
     * @param transporter
     */
    public void addToHistory(Transporter transporter);

    /**
     * persists the history for future useage
     */
    public void commitHistory();

    /**
     * Created by criskey on 4/1/2016.
     */
    interface TransporterFilter {

        public boolean filter(Transporter transporter);

    }
}
