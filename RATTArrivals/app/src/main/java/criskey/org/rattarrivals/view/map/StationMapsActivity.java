package criskey.org.rattarrivals.view.map;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.RawRes;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.kml.KmlLayer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.mvp.BaseActivity;
import criskey.org.rattarrivals.util.MapUtil;

public class StationMapsActivity extends BaseActivity<StationMapPresenter> implements
        StationMapView,
        OnMapReadyCallback,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMapClickListener {

    public static final String COM_NEW_LOCATION = "COM_NEW_LOCATION";
    private static final String LOCATION_KEY = "LOCATION_KEY";
    private static final String STATIONS_KEY = "STATIONS_KEY";
    private static final String KML_LAYER_KEY = "KML_LAYER_KEY";
    private static final String ZOOM_KEY = "ZOOM_KEY";
    private static final int DEFAULT_ZOOM = 18;


    private GoogleMap mMap;
    private TextView mTextCurrentStationMarker;
    private View mCardCurrentStationMarker;

    private FloatingActionButton mImageButtonLocationSave;

    private Animation mAnimationFadeIn;
    private Animation mAnimationFadeOut;


    private Set<Marker> mMovedMarkers;

    public static Intent createRouteLocationIntent(Context context, ArrayList<Station> stations) {
        return getBaseIntent(context, 12)
                .putExtra(LOCATION_KEY, MapUtil.TIMISOARA_DEFAULT_LAT_LNG)
                .putExtra(STATIONS_KEY, stations);
    }

    public static Intent createStationLocationIntent(Context context, LatLng location, ArrayList<Station> stations) {
        return getBaseIntent(context, DEFAULT_ZOOM)
                .putExtra(LOCATION_KEY, location)
                .putExtra(STATIONS_KEY, stations);
    }

    public static Intent createKMLRouteIntent(Context context, @RawRes int kmlRes) {
        return getBaseIntent(context, 14).putExtra(KML_LAYER_KEY, kmlRes);
    }

    private static Intent getBaseIntent(Context context, int zoom) {
        return new Intent(context, StationMapsActivity.class)
                .putExtra(ZOOM_KEY, zoom);
        // .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // force this to be portrait only
        setContentView(R.layout.activity_station_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mTextCurrentStationMarker = (TextView) findViewById(R.id.textCurrentStationMarker);
        mCardCurrentStationMarker = findViewById(R.id.cardCurrentStationMarker);
        mImageButtonLocationSave = (FloatingActionButton) findViewById(R.id.imageButtonLocationSave);

        mMovedMarkers = new HashSet<>();

        mAnimationFadeIn = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        mAnimationFadeOut = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);

    }

    @Override
    public void onPresenterAttached() {
        Intent intent = getIntent();
        ArrayList<Station> stations = (ArrayList<Station>) intent.getSerializableExtra(STATIONS_KEY);
        LatLng referenceLatLng = intent.getParcelableExtra(LOCATION_KEY);
        getPresenter().setup(stations, MapUtil.toLocation(referenceLatLng));
    }

    public void actionShowStreetView(View button) {
        getPresenter().triggerStreetView();
    }

    public void actionCommitUpdates(View button) {
        getPresenter().commitUpdates();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(this);

        LatLng target = getIntent().getParcelableExtra(LOCATION_KEY);
        getPresenter().placeMarkers();
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(target)
                .zoom(getIntent().getIntExtra(ZOOM_KEY, DEFAULT_ZOOM)).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }


    @Override
    public void onMarkerDragStart(Marker marker) {
        mCardCurrentStationMarker.setVisibility(View.VISIBLE);
        updateCurrentMarker(marker);
    }

    public void setMarkerWithoutPersistedLocation(Marker marker) {
        marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        updateCurrentMarker(marker);
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        setMarkerWithoutPersistedLocation(marker);
        mMovedMarkers.add(marker);
        updateCurrentMarker(marker);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        showCurrentMarkerDetails(marker);
        return false;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mCardCurrentStationMarker.startAnimation(mAnimationFadeOut);
        mCardCurrentStationMarker.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onShowStreetView(Location location) {
        StationStreetViewDialog streetViewDialog = (StationStreetViewDialog) getSupportFragmentManager().findFragmentByTag(StationStreetViewDialog.TAG);
        if (streetViewDialog == null) {
            streetViewDialog = StationStreetViewDialog.newInstance(MapUtil.toLatLng(location));
        }
        streetViewDialog.show(getSupportFragmentManager(), StationStreetViewDialog.TAG);
    }

    @Override
    public void onPlaceMarkers(List<Station> stations) {
        for (int i = 0, s = stations.size(); i < s; i++) {
            Station station = stations.get(i);
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(MapUtil.toLatLng(station.getLocation()))
                    .title(station.getName()));
            marker.setDraggable(true);
            marker.setSnippet(getPresenter().getTransportersSnippet(station));
            if (!station.hasLocation()) {
                setMarkerWithoutPersistedLocation(marker);
            }
        }
    }

    @Override
    public void onShowMarkerDetail(String title, String snippet, String frmtCoordinates) {
        if (mCardCurrentStationMarker.getVisibility() == View.INVISIBLE) {
            mCardCurrentStationMarker.startAnimation(mAnimationFadeIn);
            mCardCurrentStationMarker.setVisibility(View.VISIBLE);
        }
        mTextCurrentStationMarker.setText(Html.fromHtml(snippet + "<br>" + frmtCoordinates));
    }

    @Override
    public void onShowButtonCommit(boolean show) {
        mImageButtonLocationSave.setVisibility((show) ? View.VISIBLE : View.GONE);
      /*  if(show){
            mImageButtonLocationSave.show();
        }else
            mImageButtonLocationSave.hide();*/

        if (!show) {
            BitmapDescriptor defaultColor = BitmapDescriptorFactory.defaultMarker();
            for (Marker marker : mMovedMarkers) {
                marker.setIcon(defaultColor);
            }
        }
    }


    @Override
    public void onBackPressed() {
        final StationMapPresenter presenter = getPresenter();
        if (presenter.isLocationUpdated()) {
            if (presenter.isLocationSaved()) {
                sendLatLngResultsBack();
                super.onBackPressed();
            } else {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.map_warning_title)
                        .setMessage(R.string.map_warning_message)
                        .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                StationMapsActivity.super.onBackPressed();
                            }
                        })
                        .create().show();
            }
        } else {
            super.onBackPressed();
        }
    }

    private void sendLatLngResultsBack() {
        ArrayList<LatLng> latLngs = (ArrayList<LatLng>) MapUtil.toLatLngList(getPresenter().getUpdatedLocations());
        setResult(RESULT_OK, new Intent().putParcelableArrayListExtra(COM_NEW_LOCATION, latLngs));
    }

    private void updateCurrentMarker(Marker marker) {
        getPresenter().updateCurrentMarker(MapUtil.toLocation(marker.getPosition()),
                marker.getTitle(),
                marker.getSnippet());
    }

    private void showCurrentMarkerDetails(Marker marker) {
        getPresenter().showCurrentMarkerDetails(MapUtil.toLocation(marker.getPosition()),
                marker.getTitle(),
                marker.getSnippet());
    }

}
