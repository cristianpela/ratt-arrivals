package criskey.org.rattarrivals.view.station;


import android.content.Context;
import android.database.Cursor;
import android.support.annotation.Nullable;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.model.Arrival;
import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.mvp.BasePresenter;
import criskey.org.rattarrivals.parse.ArrivalTimeParser;
import criskey.org.rattarrivals.parse.ObservableNetworkDataDownloader;
import criskey.org.rattarrivals.parse.ParsingException;
import criskey.org.rattarrivals.parse.RemoteDBBackup;
import criskey.org.rattarrivals.parse.StationParser;
import criskey.org.rattarrivals.repository.RepositoryManager;
import criskey.org.rattarrivals.repository.StationRepositoryImpl;
import criskey.org.rattarrivals.repository.TransporterRepository;
import criskey.org.rattarrivals.repository.sqlite.Contract;
import criskey.org.rattarrivals.repository.sqlite.DummyCursor;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.Exceptions;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by criskey on 25/2/2016.
 */
//TODO this class is too big. Refactor!
public class StationArrivalsPresenter extends BasePresenter<StationArrivalsView> implements Observer<StationArrivalsPresenter.ArrivalSpot> {

    private static final long serialVersionUID = 5863455195034912051L;

    private List<Station> stations;

    private transient StationRepositoryImpl repository;

    private transient Subscription arrivalSubscription;

    private transient Subscription stationSubscription;

    private transient Subscription geocodeSubscription;

    private transient Subscription locationRemoteSubscription;

    private String errorMessageStation;

    private String errorMessageGeocode;

    private String lastSearch;

    private String lastStreetSearch;

    private int currentProgressMessage;

    private boolean isNewSearch;

   // private transient Station.Location currentFoundLocation;

    public StationArrivalsPresenter() {
        stations = new ArrayList<>();
        currentProgressMessage = -1;
        isNewSearch = true;
    }

    @Override
    public void onBind() {
        repository = (StationRepositoryImpl) getRepositoryManager().getRepository(RepositoryManager.STATION_REPOSITORY);
        if (isViewAttached()) {
            if (errorMessageStation != null)
                getView().onShowError(errorMessageStation);
            else if (currentProgressMessage != -1) {
                setCurrentProgressMessage(currentProgressMessage);
            } else if (!stations.isEmpty()) {
                showStationArrivals();
            }
        }
    }

    private void showStationArrivals() {
        getView().onShowStationArrivals(lastSearch,
                hasAlias(),
                getStationNames(),
                stations);
    }

    @Override
    public void onDestroy() {
        if (arrivalSubscription != null)
            arrivalSubscription.unsubscribe();
        if (stationSubscription != null)
            stationSubscription.unsubscribe();
        if (geocodeSubscription != null)
            geocodeSubscription.unsubscribe();
        if (locationRemoteSubscription != null)
            locationRemoteSubscription.unsubscribe();
        super.onDestroy();
    }

    @Override
    public void retryOnError() {
        refresh();
    }

    public void refresh() {
        isNewSearch = (errorMessageStation != null) ? true : false;
        if (lastSearch != null)
            search(lastSearch);
    }

    public void searchArrivalsByStationNameOrAlias(String nameOrAlias) {
        isNewSearch = true;
        search(nameOrAlias);
    }

    //TODO refactor this
    public void search(String nameOrAlias) {
        if(nameOrAlias == null) // recreated form saved data
            return;

        if (isNewSearch) {
            stations.clear();
            stations = repository.findAllByNameOrAlias(nameOrAlias);
        }

        if (!stations.isEmpty()) {

            if (isViewAttached())
                getView().onShowWait(); // TODO: could be this unattached? This is not called when comming back from search radius frag.

            if (isNewSearch) {
                // this case happens when the name is queried. Is convenient to return the alias then (if present),
                // because the same station might share its alias with other stations.
                Station firstStationInCaseOfName = stations.get(0); // usually is only one station
                if (firstStationInCaseOfName.getAlias() != null) {
                    nameOrAlias = firstStationInCaseOfName.getAlias();
                    stations.clear();
                    stations = repository.findAllByNameOrAlias(nameOrAlias);
                }
            }

            lastSearch = nameOrAlias;

            List<Station> stationsToJoins = new LinkedList<>();

            if (isNewSearch) {
                for (Station station : stations) {
                    if (!repository.isJoined(station))
                        stationsToJoins.add(station);
                }
            }

            if (stationsToJoins.isEmpty()) {// there are no stations to join
                if (isNewSearch) {
                    for (Station station : stations) {
                        repository.findJoinedStation(station);
                    }
                }
                createArrivalSubscription();
            } else { // must sync (join) the database first
                setCurrentProgressMessage(R.string.sync_db);
                Observable<Void> stationsToJoinObs = concatStationsTranspJoinObservable(stationsToJoins);
                Subscriber<Void> stationSub = new Subscriber<Void>() {
                    @Override
                    public void onCompleted() { // now that database is "joined" is safe to get arrivals
                        createArrivalSubscription();
                    }

                    @Override
                    public void onError(Throwable e) {
                        errorMessageStation = e.getMessage();
                        if (isViewAttached())
                            getView().onShowError(e.getMessage());
                    }

                    @Override
                    public void onNext(Void aVoid) {/*move along*/}

                };
                stationSubscription = stationsToJoinObs
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(stationSub);
            }
        }else{
            // TODO: 13/4/2016 use string res id for this. Or just update the db automatically
            getView().onShowError("Statia curenta nu exista in baza de date. Baza de date trebuie reactualizata");
        }
    }

    public List<String> getAllAliases() {
        return repository.findAllAliases();
    }

    private List<String> getStationNames() {
        if (!hasAlias())
            return null;
        List<String> stationNames = new ArrayList<>();
        for (int i = 0, s = stations.size(); i < s; i++) {
            stationNames.add(stations.get(i).getName());
        }
        return stationNames;
    }


    public boolean hasAlias() {
        return stations.get(0).hasAlias();
    }

    private void setCurrentProgressMessage(int progMessage) {
        currentProgressMessage = progMessage;
        if (isViewAttached())
            getView().onShowProgress(progMessage);
    }

    private void createArrivalSubscription() {
        Observable concatArrival = concatArrivalsObservable();
        if (concatArrival == null) {// there is no lock on any arrival so, we search all as fresh search
            isNewSearch = true;
            search(lastSearch);
            debugSys("Search .... ");
        } else {
            setCurrentProgressMessage(R.string.fetch_arrivals);
            arrivalSubscription = concatArrival
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this);
        }
    }

    @Nullable
    private Observable<Void> concatStationsTranspJoinObservable(List<Station> stationsToJoins) {
        Observable<Void> stationsToJoinObs = null;
        for (Station station : stationsToJoins) {
            if (stationsToJoinObs == null) {
                stationsToJoinObs = createStationTranspJoinObservable(station);
            } else {
                stationsToJoinObs.concatWith(createStationTranspJoinObservable(station));
            }
        }
        return stationsToJoinObs;
    }

    private Observable<Void> createStationTranspJoinObservable(final Station station) {
        return Observable.create(new Observable.OnSubscribe<Void>() {
            @Override
            public void call(Subscriber<? super Void> subscriber) {
                try {
                    StationParser stationParser = new StationParser(station);
                    stationParser.getStation();
                    sanitizeStation(station);
                    for (Transporter transporter : station.getTransporters())
                        repository.createJoinTransporterStation(station, transporter);
                    subscriber.onNext(null);
                    if (!subscriber.isUnsubscribed())
                        subscriber.onCompleted();
                } catch (ParsingException ex) {
                    Exceptions.propagate(ex);
                }
            }
        });
    }


    private Observable<ArrivalSpot> createArrivalObservable(final int index,
                                                            final Station station,
                                                            final Arrival existingArrival) {
        return Observable.create(new Observable.OnSubscribe<ArrivalSpot>() {
            @Override
            public void call(Subscriber<? super ArrivalSpot> subscriber) {
                try {
                    ArrivalTimeParser arrivalTimeParser = new ArrivalTimeParser(station, existingArrival.getTransporter());
                    Arrival arrivalTime = arrivalTimeParser.getArrivalTime(existingArrival);
                    subscriber.onNext(new ArrivalSpot(index, arrivalTime));
                    if (!subscriber.isUnsubscribed())
                        subscriber.onCompleted();
                } catch (ParsingException ex) {
                    Exceptions.propagate(ex);
                }
            }
        });
    }

    private Observable<ArrivalSpot> concatArrivalsObservable() {
        Observable<ArrivalSpot> observables = null;
        for (int i = 0; i < stations.size(); i++) {
            Station station = stations.get(i);
            List<Arrival> arrivals = station.getArrivals();
            for (int j = 0; j < arrivals.size(); j++) {
                Observable<ArrivalSpot> currObs = null;
                Arrival arrival = arrivals.get(j);
                if (arrival.isLocked() || isNewSearch)
                    currObs = createArrivalObservable(i, station, arrival);
                if (currObs != null) {
                    if (observables == null)
                        observables = currObs;
                    else
                        observables = observables.concatWith(currObs);
                }
            }
        }
        return observables;
    }


    /**
     * Remove any faulty transporter ID (transporter that is not in transporter table)
     *
     * @param station
     */
    private void sanitizeStation(Station station) {
        TransporterRepository transporterRepository = (TransporterRepository) getRepositoryManager()
                .getRepository(RepositoryManager.TRANSPORTER_REPOSITORY);
        List<Transporter> toRemove = new LinkedList<>();
        List<Transporter> transporters = station.getTransporters();
        for (int i = 0; i < transporters.size(); i++) {
            Transporter curr = transporters.get(i);
            Transporter fetched = transporterRepository.findOne(curr.getId());
            if (fetched == null) {
                toRemove.add(curr);
            } else {
                curr.setLine(fetched.getLine());
                curr.setTransporterType(fetched.getTransporterType());
            }
        }
        for (Transporter transporter : toRemove) {
            station.getTransporters().remove(transporter);
        }
        toRemove.clear();
    }

    @Override
    public void onCompleted() {
        errorMessageStation = null;
        currentProgressMessage = -1;
        if (isViewAttached()) {
            getView().onHideWait();
            showStationArrivals();
        }
    }

    @Override
    public void onError(Throwable e) {
        errorMessageStation = e.getMessage();
        currentProgressMessage = -1;
        if (isViewAttached())
            getView().onShowError(errorMessageStation);
    }

    @Override
    public void onNext(ArrivalSpot arrivalSpot) {
        /*if (isNewSearch) {
            stations.get(arrivalSpot.stationPositionInList).getArrivals().add(arrivalSpot.arrival);
        }*/
    }


    public void updateAlias(String alias, int index) {
        if (TextUtils.isEmpty(alias)) {
            if (isViewAttached())
                getView().onShowInfo("Alias must not be empty!");
        } else {
            if (index == -1) {// update all
                for (Station station : stations) {
                    repository.updateAlias(station, alias);
                    station.setAlias(alias);
                }
            } else { // single update means that will remove all the other stations and keep the updated one
                repository.updateAlias(stations.get(index), alias);
            }
            searchArrivalsByStationNameOrAlias(alias);// we must bring to screen other station under the same alias
        }
    }

    public void removeStationFromAlias(int stationIndex) {
        Station station = stations.get(stationIndex);
        if (stations.size() == 1) {
            lastSearch = station.getName();
            station.setAlias(null);
        } else
            stations.remove(station);
        repository.updateAlias(station, null);
        showStationArrivals();
    }

    public void retrySearchGeocode() {
        if (errorMessageGeocode != null)
            searchGeocode(lastStreetSearch);
    }

    public void searchGeocode(final String street) {
        if (!isViewAttached())
            return;
        getView().onShowWaitGeocode();
        Action1<Location> found = new Action1<Location>() {
            @Override
            public void call(Location location) {
                errorMessageGeocode = null;
                if (isViewAttached()) {
                    String formatatedLocation = "(" + location.getLatitude() +" " + location.getLongitude() +")";
                    getView().onFoundGeocode(formatatedLocation);
                }
            }
        };
        Action1<Throwable> error = new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                errorMessageGeocode = throwable.getMessage();
                lastStreetSearch = street;
                if (isViewAttached()) {
                    getView().onShowErrorGeocode(errorMessageGeocode);
                }
            }
        };
        geocodeSubscription = ObservableNetworkDataDownloader.getGeocodeSubscription(street, found, error);
    }

    public void openMapActivity(String aroundLocation) {
        if (isViewAttached()) {
            double lat, lng;
            if(aroundLocation == null) {
                Location location = stations.get(0).getLocation();
                lat  = location.getLatitude();
                lng = location.getLongitude();
            }else{
                String[] aroundLocationSplit = aroundLocation
                        .replace("(", "")
                        .replace(")", "")
                        .split(" ");
                lat = Double.parseDouble(aroundLocationSplit[0]);
                lng = Double.parseDouble(aroundLocationSplit[1]);
            }
            getView().onOpenMapActivity(lat, lng , (ArrayList<Station>) stations);
        }
    }

    public boolean hasLocation() {
        for(Station s: stations){
            if(!s.getLocation().equals(Location.EMPTY_LOCATION))
                return true;
        }
        return false;
    }

    public void updateStationLocation(ArrayList<LatLng> updatedLatLngs) {
        for (int i = 0; i < stations.size(); i++) {
            Station station = stations.get(i);
            LatLng latLng = updatedLatLngs.get(i);
            station.setLocation(new Location(latLng.latitude, latLng.longitude));
        }
    }

    @Override
    public String toString() {
        return "StationArrivalsPresenter{" +
                "stations=" + stations +
                '}';
    }

    public String[] getStationsNames() {
        String[] names = new String[stations.size()];
        for (int i = 0; i < stations.size(); i++) {
            names[i] = stations.get(i).getName();
        }
        return names;
    }

    public Location getLocationByStationIndex(int index){
        return stations.get(index).getLocation();
    }

    public void checkRemoteDBForLocation() {

        if(!isViewAttached())
            return;

        getView().onShowWait();

        ObservableNetworkDataDownloader.Callee<Location> callee = new ObservableNetworkDataDownloader.Callee<Location>() {
            Location remoteLocation;
            @Override
            public Location called() {
                final RemoteDBBackup rmtDb = new RemoteDBBackup();
                rmtDb.execute(new RemoteDBBackup.Query() {
                    @Override
                    public void exec() throws IOException, JSONException {
                        JSONObject jsonLocation  = rmtDb.getLocation(stations.get(0).getId());
                        remoteLocation = new Location(jsonLocation.getDouble("latitude"), jsonLocation.getDouble("longitude"));
                    }

                    @Override
                    public void onError(Exception e) {
                        Exceptions.propagate(e);
                    }
                });
                return remoteLocation;
            }
        };

        ObservableNetworkDataDownloader.runInParallel(callee, new Observer<Location>() {

            Location remoteLocation;

            @Override
            public void onCompleted() {
                if(!remoteLocation.equals(Location.EMPTY_LOCATION)) {
                    repository.updateLocation(stations.get(0), remoteLocation);
                    stations.get(0).setLocation(remoteLocation);
                }
                getView().onRemoteLocationResult(remoteLocation);
                getView().onHideWait();
            }

            @Override
            public void onError(Throwable e) {
                getView().onShowError(e.getMessage());
            }

            @Override
            public void onNext(Location location) {
                remoteLocation = location;
            }
        });
    }


    //TODO since arrivals are already added when searching from db as placeholder, this wrapper is redudant, I think
    public static class ArrivalSpot {
        private int stationPositionInList;
        private Arrival arrival;

        public ArrivalSpot(int stationPositionInList, Arrival arrival) {
            this.stationPositionInList = stationPositionInList;
            this.arrival = arrival;
        }
    }

//    private class SearchStationAdapter2 extends SimpleCursorAdapter {
//        public SearchStationAdapter2(Context context) {
//            super(context, android.R.layout.simple_list_item_2, null,
//                    new String[]{Contract.Station.NAME, Contract.Station.ALIAS, Contract.Station._ID}, new int[]{android.R.id.text1, android.R.id.text2},
//                    CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
//        }
//
//        @Override
//        public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
//            Cursor c = (constraint == null) ? new DummyCursor() : repository.findAllLikeCursor(constraint.toString());
//            return c;
//        }
//
//    }
}

