package criskey.org.rattarrivals.repository;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.model.TransporterType;
import criskey.org.rattarrivals.repository.sqlite.Contract;
import criskey.org.rattarrivals.repository.sqlite.DispatchContentProvider;

/**
 * Created by criskey on 10/2/2016.
 */
public class TransporterSQLRepository extends AbstractTransporterRepository {

    private ContentResolver contentResolver;

    private Uri uri = Contract.TRANSPORTER_URI;

    public TransporterSQLRepository(Context context) {
        super(context);
        contentResolver = context.getContentResolver();
    }

    @Override
    public boolean deleteAllTransporters() {
        return contentResolver.delete(uri, null, null) > 0;
    }


    @Override
    public List<Transporter> getTransportersByType(TransporterType transporterType) {
        return getTransporters(
                Contract.Transporter.TYPE + "=?", new String[]{transporterType.toString()});
    }

    @Override
    public List<Transporter> getTransportersByFavorite() {
        return getTransporters(Contract.Transporter.FAVORITE + "=?", new String[]{"1"});
    }

    @Override
    public void changeFavorite(Transporter transporter) {
        Uri singleUri = DispatchContentProvider.buildSingleItemUri(uri, ""+transporter.getId());
        ContentValues cv = new ContentValues();
        cv.put(Contract.Transporter.FAVORITE, !transporter.isFavorite());
        contentResolver.update(singleUri, cv, null, null);
        transporter.setFavorite(!transporter.isFavorite());
    }

    @Override
    public Transporter findOne(Long primaryKey) {
        List<Transporter> transporters = getTransporters(Contract.Transporter._ID + "=?", new String[]{""+primaryKey});
        return (transporters.isEmpty()) ? null : transporters.get(0);
    }

    @Override
    public List<Transporter> findAll() {
        return getTransporters(null, null);
    }

    @Override
    public boolean createOne(Transporter entity) {
        contentResolver.insert(uri, transporter2CV(entity));
        return true;
    }

    @Override
    public boolean createMany(List<Transporter> entities) {
        int size = entities.size();
        ContentValues[] cvs = new ContentValues[size];
        for (int i = 0; i < size; i++)
            cvs[i] = transporter2CV(entities.get(i));
        contentResolver.bulkInsert(uri, cvs);
        return true;
    }

    private List<Transporter> getTransporters(String selection, String[] selectionArgs) {
        Cursor cursor = contentResolver.query(uri, null, selection, selectionArgs, null);
        List<Transporter> transporters = new ArrayList<>();
        while (cursor.moveToNext()) {
            transporters.add(cursor2Transporter(cursor));
        }
        cursor.close();
        return transporters;
    }

    private ContentValues transporter2CV(Transporter transporter) {
        ContentValues cv = new ContentValues();
        cv.put(Contract.Transporter._ID, transporter.getId());
        cv.put(Contract.Transporter.NAME, transporter.getLine());
        cv.put(Contract.Transporter.TYPE, transporter.getTransporterType().toString());
        cv.put(Contract.Transporter.FAVORITE, transporter.isFavorite());
        cv.put(Contract.Transporter.LINK, transporter.getArrivalLink());
        return cv;
    }

    private Transporter cursor2Transporter(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndexOrThrow(Contract.Transporter._ID));
        String name = cursor.getString(cursor.getColumnIndexOrThrow(Contract.Transporter.NAME));
        String link = cursor.getString(cursor.getColumnIndexOrThrow(Contract.Transporter.LINK));
        TransporterType type = TransporterType.valueOf(
                cursor.getString(cursor.getColumnIndexOrThrow(Contract.Transporter.TYPE)));
        int fav = cursor.getInt(cursor.getColumnIndexOrThrow(Contract.Transporter.FAVORITE));
        return new Transporter(type, name, link, id, fav == 1);
    }


    //TODO use AsyncQueryHandler or Rx?
    private static interface AyncQueryHandlerListener {
        public void onQueryComplete(int token, Object cookie, Cursor cursor);

        public void onUpdateComplete(int token, Object cookie, int result);

        public void onDeleteComplete(int token, Object cookie, int result);
    }

    private static class TransporterAsyncQueryHandler extends AsyncQueryHandler {

        private AyncQueryHandlerListener listener;

        public TransporterAsyncQueryHandler(ContentResolver cr, AyncQueryHandlerListener listener) {
            super(cr);
            this.listener = listener;
        }

        @Override
        protected void onUpdateComplete(int token, Object cookie, int result) {
            listener.onUpdateComplete(token, cookie, result);
        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            listener.onQueryComplete(token, cookie, cursor);
        }

        @Override
        protected void onDeleteComplete(int token, Object cookie, int result) {
            listener.onDeleteComplete(token, cookie, result);
        }
    }

}
