package criskey.org.rattarrivals.parse;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import criskey.org.rattarrivals.model.Station;

/**
 * Created by criskey on 12/2/2016.
 */
public class StationsListingParser extends Parser {

    public StationsListingParser(AbstractNetworkDataDownloader downloader) throws ParsingException {
        super(downloader);
    }

    public StationsListingParser() throws ParsingException {
        super(NetworkUtils.STATIONS_LINK);
    }

    public List<Station> getStations() {
        List<Station> stations = new ArrayList<>();
        Elements optionElements = document.select("option");
        for (Element el : optionElements) {
            Station station = new Station();
            station.setId(el.attr("value"));
            station.setName(el.text());
            stations.add(station);
        }
        return stations;
    }

}
