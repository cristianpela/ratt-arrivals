package criskey.org.rattarrivals.util;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import criskey.org.rattarrivals.model.Location;

/**
 * Created by criskey on 29/3/2016.
 */
public final class MapUtil {

    public static final LatLng TIMISOARA_DEFAULT_LAT_LNG = new LatLng(45.7488716, 21.2086793);

    private MapUtil() {
    }

    public static LatLng toLatLng(Location location) {
        return new LatLng(location.getLatitude(), location.getLongitude());
    }

    public static Location toLocation(LatLng latLng) {
        return new Location(latLng.latitude, latLng.longitude);
    }

    public static List<LatLng> toLatLngList(List<Location> locations) {
        int size = locations.size();
        List<LatLng> latLngs = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            latLngs.add(toLatLng(locations.get(i)));
        }
        return latLngs;
    }

    public static List<Location> toLocationList(List<LatLng> latLngs) {
        int size = latLngs.size();
        List<Location> locations = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            locations.add(toLocation(latLngs.get(i)));
        }
        return locations;
    }

    /**
     * http://stackoverflow.com/questions/4656802/midpoint-between-two-latitude-and-longitude
     *
     * @param latLng1
     * @param latLng2
     * @return
     */
    public static LatLng middlePoint(LatLng latLng1, LatLng latLng2) {

        double lon1 = latLng1.longitude;
        double lon2 = latLng2.longitude;

        double dLon = Math.toRadians(lon2 - lon1);

        //convert to radians
        double lat1 = Math.toRadians(latLng1.latitude);
        double lat2 = Math.toRadians(latLng2.latitude);
        lon1 = Math.toRadians(lon1);

        double Bx = Math.cos(lat2) * Math.cos(dLon);
        double By = Math.cos(lat2) * Math.sin(dLon);

        double lat3 = Math.atan2(Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + Bx) * (Math.cos(lat1) + Bx) + By * By));
        double lon3 = lon1 + Math.atan2(By, Math.cos(lat1) + Bx);

        return new LatLng(lat3, lon3);
    }

    // TODO something is wrong with this. I dont get equality when address is invalid
    public static boolean isLocationCoordinatesValid(LatLng latLng) {
        return !latLng.equals(TIMISOARA_DEFAULT_LAT_LNG);
    }

    public static boolean isLocationCoordinatesValid(Location location) {
        return isLocationCoordinatesValid(toLatLng(location));
    }
}
