package criskey.org.rattarrivals.model;

/**
 * Created by criskey on 14/1/2016.
 */
public final class TransporterBuilder {

    private static final String TO_STRING_DELIMITER = "-";

    private TransporterBuilder() {
    }

    public static String asString(Transporter tranporter) {
        return tranporter.getTransporterType()
                + TO_STRING_DELIMITER + tranporter.getLine()
                + TO_STRING_DELIMITER + tranporter.getArrivalLink()
                + TO_STRING_DELIMITER + tranporter.getId()
                + TO_STRING_DELIMITER + tranporter.isFavorite();
    }

    public static Transporter buildFromString(String data) {
        String[] splitter = data.split(TO_STRING_DELIMITER);
        TransporterType transporterType = TransporterType.valueOf(splitter[0]);
        String line = splitter[1];
        String link = splitter[2];
        long id = Long.parseLong(splitter[3]);
        boolean favorite = Boolean.parseBoolean(splitter[4]);
        return new Transporter(transporterType, line, link, id, favorite);
    }

    public static Transporter dummyTransporter(){
        return new Transporter(null, null, null, 0, false);
    }
}
