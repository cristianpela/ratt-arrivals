package criskey.org.rattarrivals;

import criskey.org.rattarrivals.mvp.IView;
import criskey.org.rattarrivals.mvp.WaitView;
import criskey.org.rattarrivals.view.DisplayFilter;

/**
 * Created by criskey on 28/12/2015.
 */
public interface MainView extends WaitView {

    public void onLastDisplayFilter(DisplayFilter displayFilter);

}
