package criskey.org.rattarrivals.view;

import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by criskey on 20/1/2016.
 */
public class LastCheckTimer {

    private AtomicLong atomicTimeStamp;
    private Subscription timerSubscription;
    private TimerCallback callback;

    public LastCheckTimer(TimerCallback callback, long timesStamp) {
        this(callback);
        setReferenceTimeStamp(timesStamp);
    }

    public LastCheckTimer(TimerCallback callback) {
        this.callback = callback;
        atomicTimeStamp = new AtomicLong();
    }

    public String getTimeElapsedMessage() {
        long now = System.currentTimeMillis();
        long deltaRaw = now - atomicTimeStamp.get();// milliseconds
        if (deltaRaw < TimeUnit.SECONDS.toMillis(60)) {
            return "Checked: " + TimeUnit.MILLISECONDS.toSeconds(deltaRaw) + " seconds ago";
        }
        return "Checked: " + TimeUnit.MILLISECONDS.toMinutes(deltaRaw) + " minutes ago";
    }

    public void startCounting() {
        timerSubscription = Observable
                .interval(30, TimeUnit.SECONDS).flatMap(new Func1<Long, Observable<String>>() {
                    @Override
                    public Observable<String> call(Long aLong) {
                        return getElapsedObservable();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String message) {
                        callback.onAsyncElapsedTimeComputed(message);
                    }
                });

    }

    @NonNull
    protected Observable<String> getElapsedObservable() {
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(getTimeElapsedMessage());
                    subscriber.onCompleted();
                }
            }
        });
    }

    public void stopCounting() {
        if (timerSubscription != null) {
            timerSubscription.unsubscribe();
        }
    }

    public void setReferenceTimeStamp(long timeStamp) {
        atomicTimeStamp.set(timeStamp);
    }

    public long getReferenceTimeStamp(){
        return atomicTimeStamp.get();
    }

    public void reset() {
        atomicTimeStamp.set(System.currentTimeMillis());
    }

    public static interface TimerCallback {
        public void onAsyncElapsedTimeComputed(String elapsedMessage);
    }
}
