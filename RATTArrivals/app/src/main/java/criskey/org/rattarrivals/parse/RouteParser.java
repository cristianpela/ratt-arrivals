package criskey.org.rattarrivals.parse;

import android.support.annotation.NonNull;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.InputStream;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import criskey.org.rattarrivals.model.Arrival;
import criskey.org.rattarrivals.model.Route;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.model.Transporter;

/**
 * Created by criskey on 14/1/2016.
 */
public class RouteParser extends Parser {

    private static Collator collator = Collator.getInstance();

    private static final int COLUMN_STATION_WIDTH = 200;
    private static final int COLUMN_ARRIVAL_TIME_WIDTH = 60;
    private static final String HEADER_TABLE_WORD_STATION = "Stația";
    // private static final String SKIP_WORD_ARRIVAL = "Sosire";

    private Transporter transporter;

    public RouteParser(AbstractNetworkDataDownloader downloader, Transporter transporter) throws ParsingException {
        super(downloader);
        this.transporter = transporter;
    }

    public RouteParser(Transporter transporter) throws ParsingException {
        this(new NetworkDataDownloader(transporter.getArrivalLink()), transporter);
    }

    public static RouteParser newInstance(Transporter transporter, InputStream inputStream) throws ParsingException {
        return (inputStream == null) ? new RouteParser(transporter) : new DebugRouteParser(inputStream, transporter);
        //throw  new ParsingException("This is a test error!");
    }

    public Route getRoute() {

        List<String> stationNames = getStationNamesBothWay();
        List<String> arrivalTimes = getArrivalTimesBothWay();

        List<Station> fromToStations = new ArrayList<>();
        List<Station> toFromStations = new ArrayList<>();

        List<Station> currentStationList = fromToStations;
        // we're staring from index one cause first "station" has the header HEADER_TABLE_WORD_STATION
        for (int i = 1, size = stationNames.size(); i < size; i++) {

            String stationName = stationNames.get(i);
            String arrivalTime = arrivalTimes.get(i);

            if (isHeaderStation(stationName)) {
                currentStationList = toFromStations;
                continue;// skip on cycle to avoid the other header HEADER_TABLE_WORD_STATION & SKIP_WORD_ARRIVAL
            }

            Station station = createStation(stationName, arrivalTime);
            currentStationList.add(station);
        }

        Station.Pool.end();
        Arrival.Pool.end();

//        Elements tables = document.select("table");
//        Element fromTable = tables.get(2);
//        Element toTable = tables.get(5);
//
//        List<Station> fromToStations = parseWay(fromTable);
//        List<Station> toFromStations = parseWay(toTable);
//
//        Station.Pool.end();
//        Arrival.Pool.end();


        return new Route(transporter, fromToStations, toFromStations);
    }

    private boolean isHeaderStation(String stationName) {
        return stationName.equals(HEADER_TABLE_WORD_STATION) || stationName.equals("Sta?ia");
    }

    @NonNull
    protected Station createStation(String stationName, String arrivalTime) {
        Station station = Station.Pool.obtain();
        station.setName(stationName);
        station.setTransporters(Arrays.asList(new Transporter[]{transporter}));
        Arrival arrival = Arrival.Pool.obtain();
        arrival.setTime(arrivalTime);
        arrival.setTransporter(transporter);
        station.setArrivals(Arrays.asList(new Arrival[]{arrival}));
        //Station.Pool.recycle(station);
        //Arrival.Pool.recycle(arrival);
        return station;
    }

    public List<String> getStationNamesBothWay() {
        return getRelevantColumn(COLUMN_STATION_WIDTH);
    }

    public List<String> getArrivalTimesBothWay() {
        return getRelevantColumn(COLUMN_ARRIVAL_TIME_WIDTH);
    }

    private List<Station> parseWay(Element table) {
        List<Station> stations = new ArrayList<>();
        Elements td = table.select("tr").select("td");
        List<TextNode> stationNames = td.get(1).child(0).textNodes();
        List<TextNode> arrivalTimes = td.get(2).child(0).textNodes();
        assert stationNames.size() == arrivalTimes.size(): "Station names size and arrival times size must match";
        for (int i = 0; i < stationNames.size(); i++) {
            Station station = createStation(stationNames.get(i).text(), arrivalTimes.get(i).text());
            stations.add(station);
        }
        return stations;
    }

    private List<String> getRelevantColumn(int columnWidth) {
        Elements tdElements = document.select("td[width=" + columnWidth + "]");
        List<String> data = new ArrayList<>();
        for (Element tdElement : tdElements) {
            String bText = tdElement.select("b").text().trim();
            // if (!bText.equals(SKIP_WORD_ARRIVAL) && !bText.equals(HEADER_TABLE_WORD_STATION))
            data.add(bText);
        }
        return data;
    }

    public static class DebugRouteParser extends RouteParser {

        private static final SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH);

        private String debugArrivalTime;

        private long prefix;

        public DebugRouteParser(InputStream resourceIn, Transporter transporter) throws ParsingException {
            super(new ResourceStreamDataDownloader(resourceIn), transporter);
            debugArrivalTime = dateFormat.format(new Date());
            prefix = transporter.getId();
        }

        @NonNull
        @Override
        protected Station createStation(String stationName, String arrivalTime) {
            return super.createStation(prefix + "_" + stationName, debugArrivalTime);
        }

    }

}
