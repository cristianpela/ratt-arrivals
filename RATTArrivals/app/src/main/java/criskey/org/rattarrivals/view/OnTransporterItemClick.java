package criskey.org.rattarrivals.view;

import criskey.org.rattarrivals.model.Transporter;

/**
 * Created by criskey on 4/4/2016.
 */
public interface OnTransporterItemClick {
    /**
     * Callback method to get the selected transporter in {@link criskey.org.rattarrivals.MainActivity}
     *
     * @param transporter
     */
    public void onTransporterClickFromFragment(Transporter transporter);
}
