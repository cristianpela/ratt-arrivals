package criskey.org.rattarrivals.repository;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import criskey.org.rattarrivals.model.Arrival;
import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.model.TransporterType;
import criskey.org.rattarrivals.repository.sqlite.Contract;
import criskey.org.rattarrivals.repository.sqlite.DispatchContentProvider;

/**
 * Created by criskey on 12/2/2016.
 */
public class StationRepositoryImpl implements StationRepository {

    private ContentResolver contentResolver;
    private Uri stationUri = Contract.STATION_URI;
    private Uri joinUri = Contract.JOIN_URI;

    public StationRepositoryImpl(Context context) {
        contentResolver = context.getContentResolver();
    }

    @Override
    public Station findOne(String primaryKey) {
        List<Station> stations = find(stationUri, Contract.Station._ID + "=?", new String[]{primaryKey}, false);
        return (stations.isEmpty()) ? null : stations.get(0);
    }

    // TODO do all the joining here?
    @Override
    public List<Station> findAllByNameOrAlias(String name) {
        return find(stationUri, Contract.Station.NAME + "=? OR " + Contract.Station.ALIAS + "=?", new String[]{name, name}, false);
    }

    @Override
    /**
     * Using LIKE ?% will generate a bind or column index out of range
     * See this bug report.
     * <a href="https://code.google.com/p/android/issues/detail?id=3153">Issue 3135</a>
     */
    public List<Station> findAllStationsLike(String hint) {
        String selection = Contract.Station.NAME + " LIKE '%' || @VVV || '%' OR " + Contract.Station.ALIAS + " LIKE '%' || @VVV || '%'";
        return find(stationUri, selection, new String[]{hint}, false);
    }

    @Override
    public List<Station> findAll() {
        return find(stationUri, null, null, false);
    }

    @Override
    public List<String> findAllAliases() {
        List<String> aliases = new ArrayList<>();
        Cursor cursor = contentResolver.query(Contract.STATION_URI_DISTINCT,
                new String[]{Contract.Station.ALIAS}, null, null, null);
        while (cursor.moveToNext()) {
            String alias = cursor.getString(cursor.getColumnIndexOrThrow(Contract.Station.ALIAS));
            if (alias != null)
                aliases.add(alias);
        }
        cursor.close();
        return aliases;
    }

    public Cursor findAllCursor() {
        return contentResolver.query(stationUri, null, null, null, null);
    }

    public Cursor findAllLikeCursor(String hint) {
        String selection = Contract.Station.NAME + " LIKE '%' || @VVV || '%' OR " + Contract.Station.ALIAS + " LIKE '%' || @VVV || '%'";
        return contentResolver.query(stationUri, null, selection, new String[]{hint}, null);
    }

    @Override
    public Station findJoinedStation(Station station) {
        //TODO: Should we preload Station with dummy ArrivalTime like ("--:--")
        Uri findUri = DispatchContentProvider.buildSingleItemUri(Contract.JOIN_URI, station.getId());
        Cursor cursor = contentResolver.query(findUri, null, null, null, null);
        while (cursor.moveToNext()) {
            Transporter transporter = cursor2Transporter(cursor);
            station.getTransporters().add(transporter);
            station.getArrivals().add(Arrival.emptyArrival(transporter));
        }
        cursor.close();
        return station;
    }

    @Override
    public boolean createOne(Station entity) {
        contentResolver.insert(stationUri, barreStation2Cv(entity));
        return false;
    }

    @Override
    public boolean createMany(List<Station> entities) {
        int size = entities.size();
        ContentValues[] cvs = new ContentValues[size];
        for (int i = 0; i < size; i++) {
            cvs[i] = barreStation2Cv(entities.get(i));
        }
        contentResolver.bulkInsert(stationUri, cvs);
        return true;
    }




    @Override
    public boolean deleteAll() {
        return contentResolver.delete(stationUri, null, null) > 0;
    }

    @Override
    public boolean createJoinTransporterStation(Station station, Transporter transporter) {
        ContentValues cv = new ContentValues();
        cv.put(Contract.JoinStationTransporter.FK_STATION, station.getId());
        cv.put(Contract.JoinStationTransporter.FK_TRANSPORTER, transporter.getId());
        contentResolver.insert(joinUri, cv);
        return true;
    }


    @Override
    public boolean isJoined(Station station) {
        Cursor cursor = contentResolver.query(Contract.JOIN_URI, new String[]{Contract.JoinStationTransporter.FK_STATION},
                Contract.JoinStationTransporter.FK_STATION + "=?", new String[]{station.getId()}, null);
        boolean joined = cursor != null && cursor.moveToNext();
        cursor.close();
        return joined;
    }

    /**
     * Should not be called from presentation layer.<br>Use specialized update methods like
     * {@link #updateAlias(Station, String)} or {@link #updateLocation(Station, Location)}
     *
     * @param entity
     * @return
     */
    @Override
    public boolean update(Station entity) {
        throw new UnsupportedOperationException("Not implemented! Consider using updateAlias or updateLocation methods instead");
    }

    @Override
    public boolean updateAlias(Station station, String alias) {
        ContentValues cv = new ContentValues();
        cv.put(Contract.Station.ALIAS, alias);
        contentResolver.update(DispatchContentProvider.buildSingleItemUri(Contract.STATION_URI, station.getId()),
                cv, null, null);
        return false;
    }

    @Override
    public boolean updateLocation(Station station, Location location) {
        ContentValues cv = new ContentValues();
        cv.put(Contract.Station.LATITUDE, location.getLatitude());
        cv.put(Contract.Station.LONGITUDE, location.getLongitude());
        contentResolver.update(DispatchContentProvider.buildSingleItemUri(Contract.STATION_URI, station.getId()),
                cv, null, null);
        return false;
    }

    // helper methods
    private ContentValues barreStation2Cv(Station station) {
        ContentValues cv = new ContentValues();
        cv.put(Contract.Station._ID, station.getId());
        cv.put(Contract.Station.NAME, station.getName());
        return cv;
    }

    public Station cursor2Station(Cursor cursor, boolean joined) {
        Station station = new Station();
        station.setId(cursor.getString(cursor.getColumnIndexOrThrow(Contract.Station._ID)));
        if (!joined) {
            station.setName(cursor.getString(cursor.getColumnIndexOrThrow(Contract.Station.NAME)));
            station.setAlias(cursor.getString(cursor.getColumnIndexOrThrow(Contract.Station.ALIAS)));
            double latitude = cursor.getDouble(cursor.getColumnIndexOrThrow(Contract.Station.LATITUDE));
            double longitude = cursor.getDouble(cursor.getColumnIndexOrThrow(Contract.Station.LONGITUDE));
            station.setLocation(new Location(latitude, longitude));
        }
        return station;
    }

    private Transporter cursor2Transporter(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndexOrThrow(Contract.Transporter._ID));
        String name = cursor.getString(cursor.getColumnIndexOrThrow(Contract.Transporter.NAME));
        String link = cursor.getString(cursor.getColumnIndexOrThrow(Contract.Transporter.LINK));
        TransporterType type = TransporterType.valueOf(
                cursor.getString(cursor.getColumnIndexOrThrow(Contract.Transporter.TYPE)));
        int fav = cursor.getInt(cursor.getColumnIndexOrThrow(Contract.Transporter.FAVORITE));
        return new Transporter(type, name, link, id, fav == 1);
    }

    private List<Station> find(Uri uri, String selection, String[] selectionArgs, boolean joined) {
        List<Station> stations = new ArrayList<>();
        Cursor cursor = contentResolver.query(uri, null, selection, selectionArgs, null);
        while (cursor.moveToNext()) {
            Station station = cursor2Station(cursor, joined);
            stations.add(station);
        }
        cursor.close();
        return stations;
    }

}

