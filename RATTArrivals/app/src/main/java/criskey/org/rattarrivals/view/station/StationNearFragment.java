package criskey.org.rattarrivals.view.station;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.List;

import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.mvp.BaseFragment;
import criskey.org.rattarrivals.util.SpeechPrompter;

/**
 * Created by criskey on 15/3/2016.
 */
public class StationNearFragment extends BaseFragment<StationNearPresenter> implements
        StationNearView,
        SeekBar.OnSeekBarChangeListener,
        View.OnClickListener {

    public static final String TAG = StationNearFragment.class.getSimpleName();

    private static final int BUTTON_SEARCH_TAG = 0;

    private static final int BUTTON_SPEECH_TAG = 1;

    private SeekBar mSeekBarRadius;

    private TextView mTextRadiusDisplay;

    private TextView mTextSliderTitle;

    private TextView mTextRadiusAddress;

    private RecyclerView mRecyclerRadiusStations;

    private EditText mEditSearchAround;

    private StationNearFragment.Callback callback;

    @Override
    public void onAttach(Context context) {
        callback = (Callback) context;
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        callback = null;
        super.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.search_station_layout, container, false);

        mSeekBarRadius = (SeekBar) view.findViewById(R.id.sliderRadius);

        mTextSliderTitle = (TextView) view.findViewById(R.id.textRadiusSliderTitle);
        mTextRadiusDisplay = (TextView) view.findViewById(R.id.textRadiusDisplay);
        mTextRadiusAddress = (TextView) view.findViewById(R.id.textRadiusAddress);

        mEditSearchAround = (EditText) view.findViewById(R.id.editSearchAround);

        Button searchButton = (Button) view.findViewById(R.id.buttonSearchRadius);
        searchButton.setTag(BUTTON_SEARCH_TAG);
        searchButton.setOnClickListener(this);
        ImageButton buttonSpeech = (ImageButton) view.findViewById(R.id.imageButtonSpeech);
        buttonSpeech.setTag(BUTTON_SPEECH_TAG);
        buttonSpeech.setOnClickListener(this);

        mRecyclerRadiusStations = (RecyclerView) view.findViewById(R.id.listRadiusStations);
        mRecyclerRadiusStations.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerRadiusStations.setAdapter(new StationNearRecyclerAdapter(callback));

        return view;
    }

    @Override
    protected void onArgumentsAttached(Bundle args) {

    }

    @Override
    public void onPresenterAttached() {
        //prevent getPresenter call in progress changed to be not null
        mSeekBarRadius.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        getPresenter().setRadius(progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mTextSliderTitle.setTextColor(getResources().getColor(R.color.colorAccent));
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mTextSliderTitle.setTextColor(Color.BLACK);
    }

    @Override
    public void onShowStationsInRadius(List<StationNearPresenter.DistanceStation> stations, String address) {
        ((StationNearRecyclerAdapter) mRecyclerRadiusStations.getAdapter()).updateStations(stations);
        if (address != null) {
            String prepend = getResources().getString(R.string.search_radius_address_prefix);
            mTextRadiusAddress.setText(prepend + " " + address);
        }
        //mEditSearchAround.setText(address);
    }

    @Override
    public void onClick(View v) {
        int tag = (int) v.getTag();
        switch (tag) {
            case BUTTON_SEARCH_TAG: {
                String address = mEditSearchAround.getText().toString();
                getPresenter().searchAround(address);
                break;
            }
            case BUTTON_SPEECH_TAG: {
                SpeechPrompter.promptSpeechInputFromFragment(this, R.string.search_street_voice_prompt_title);
                break;
            }
        }
    }

    @Override
    public void onShowRadius(int meters) {
        mTextRadiusDisplay.setText("" + meters);
    }

    @Override
    public void onSliderInitialState(int current, int max) {
        mSeekBarRadius.setMax(max);
        mSeekBarRadius.setProgress(current);
        mTextRadiusDisplay.setText("" + current);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SpeechPrompter.SPEECH_REQUEST_CODE && data != null) {
                mEditSearchAround.setText(SpeechPrompter.getConvertedSpeechTopResult(data));
            }
        }
    }

    @Override
    public void onShowWait() {

    }

    @Override
    public void onHideWait() {

    }

    @Override
    public void onShowProgress(@StringRes int res) {

    }

    @Override
    public void onShowError(String errorMessage) {

    }

    public static interface Callback {
        public void onNearStationClicked(String name);
    }
}
