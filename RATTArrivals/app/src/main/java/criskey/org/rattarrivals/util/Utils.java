package criskey.org.rattarrivals.util;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.StringRes;
import android.util.SparseArray;
import android.view.View;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by criskey on 11/1/2016.
 */
public final class Utils {

    public static final Locale LOCALE_RO = new Locale("ro");
    private static Pattern prettifyStationPattern = Pattern.compile("^(Tv|Tb|Ab_|AT)|([\\s|1-4][a|b|tb| ?s| ?p]*.?)$|_");

    private Utils() {
    }

    /**
     * Translate a phrase by replacing certain words with the corresponding resource string<br>
     * Example:<br>
     * If the corresponding string for world is "welt", the phrase
     * <code>
     * "Hello world"
     * </code>
     * will become <code>"Hello welt"</code>
     *
     * @param context
     * @param phrase
     * @param resPairs "R.string." id as key, and value the word to be replaced
     * @return the translated phrase
     */
    public static String insertTranslation(Context context, String phrase, SparseArray<String> resPairs) {
        String translatedPhrase = phrase;
        Resources resource = context.getResources();
        for (int i = 0; i < resPairs.size(); i++) {
            int resId = resPairs.keyAt(i);
            String toTranslate = resPairs.get(resId);
            translatedPhrase = translatedPhrase.replace(toTranslate, resource.getString(resId));
        }
        return translatedPhrase;
    }

    public static String insertOneTranslation(Context context, String phrase, @StringRes int resId, String wordToBeTranslated) {
        SparseArray<String> resPairs = new SparseArray<>();
        resPairs.put(resId, wordToBeTranslated);
        return insertTranslation(context, phrase, resPairs);
    }

    @SuppressWarnings("ResourceType")
    public static void toggleVisibilityGone(View view) {
        view.setVisibility(Math.abs(view.getVisibility() - View.GONE));
    }

    @SuppressWarnings("ResourceType")
    public static void toggleVisibilityInvisible(View view) {
        view.setVisibility(Math.abs(view.getVisibility() - View.INVISIBLE));
    }

    public static String prettifyStationName(String name) {
        Matcher matcher = prettifyStationPattern.matcher(name);
        return matcher.replaceAll(" ").trim();
    }
}
