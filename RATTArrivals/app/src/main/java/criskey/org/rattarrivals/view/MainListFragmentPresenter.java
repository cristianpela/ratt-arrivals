package criskey.org.rattarrivals.view;

import java.util.List;

import criskey.org.rattarrivals.MainPresenter;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.model.TransporterType;
import criskey.org.rattarrivals.mvp.Presenter;
import criskey.org.rattarrivals.repository.PreferencesRepository;
import criskey.org.rattarrivals.repository.RepositoryManager;
import criskey.org.rattarrivals.repository.TransporterRepository;
import criskey.org.rattarrivals.mvp.BasePresenter;
import criskey.org.rattarrivals.util.BreakPoint;

/**
 * Created by criskey on 6/1/2016.
 */
public class MainListFragmentPresenter extends BasePresenter<MainListFragmentView> {

    private static final long serialVersionUID = -2656562305658181672L;

    private transient PreferencesRepository preferencesRepository;
    
    private transient TransporterRepository transporterRepository;

    public MainListFragmentPresenter() {
    }

    @Override
    public void onBind() {
        preferencesRepository = getPrefsRepository();
        if (preferencesRepository == null) // guard this for now
            return;
        if (isViewAttached()) {
            String lastDisplayFilterAsString = preferencesRepository.getLastDisplayFilter();
            DisplayFilter lastDisplayFilter = (lastDisplayFilterAsString == null) ? DisplayFilter.ALL :
                    DisplayFilter.valueOf(lastDisplayFilterAsString);
            List<Transporter> transporterList = getTransportersByDisplayFilter(lastDisplayFilter);
            getView().displayTransporters(transporterList);
        }
    }

    public void setSelectedDisplayFilter(DisplayFilter displayFilter) {
        getPrefsRepository().setLastDisplayFilter(displayFilter.toString());
        if (isViewAttached()) {
            getView().displayTransporters(getTransportersByDisplayFilter(displayFilter));
        }
    }

    private List<Transporter> getTransportersByDisplayFilter(DisplayFilter displayFilter) {
        List<Transporter> transporterList = null;
        TransporterRepository repository = getTransporterRepository();
        switch (displayFilter) {
            case ALL:
                transporterList = repository.findAll();
                break;
            case FAVORITE:
                transporterList = repository.getTransportersByFavorite();
                break;
            case TRAM:
                transporterList = repository.getTransportersByType(TransporterType.TRAM);
                break;
            case BUS:
                transporterList = repository.getTransportersByType(TransporterType.BUS);
                break;
            case TROLLEY:
                transporterList = repository.getTransportersByType(TransporterType.TROLLEY);
                break;
        }
        return transporterList;
    }

    public void changeFavorite(Transporter transporter) {
        getTransporterRepository().changeFavorite(transporter);
    }

    public TransporterRepository getTransporterRepository() {
        if (transporterRepository == null) {
            transporterRepository = (TransporterRepository) getRepositoryManager().getRepository(RepositoryManager.TRANSPORTER_REPOSITORY);
        }
        return transporterRepository;
    }

    //TODO: Bug here ! Sometimes I'm getting null?
    private PreferencesRepository getPrefsRepository() {
        return (PreferencesRepository) getRepositoryManager().getRepository(RepositoryManager.PREFERENCES_REPOSITORY);
    }

}
