package criskey.org.rattarrivals.parse;

import org.jsoup.nodes.Document;

/**
 * Created by criskey on 26/12/2015.
 */
public interface  AbstractNetworkDataDownloader {

    public Document load() throws ParsingException;
}
