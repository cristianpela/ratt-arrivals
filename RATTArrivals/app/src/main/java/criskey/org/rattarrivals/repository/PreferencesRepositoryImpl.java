package criskey.org.rattarrivals.repository;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;

import java.util.List;
import java.util.Locale;

import criskey.org.rattarrivals.R;

/**
 * Created by criskey on 29/1/2016.
 */
public class PreferencesRepositoryImpl implements PreferencesRepository {

    private static final String APP_HAS_DATA = "app_db_has_data";

    private static final String APP_LAST_DISPLAY_FILTER = "app_last_display_filter";

    private static final String APP_CURRENT_LANGUAGE = "app_current_language";

    private SharedPreferences preferences;

    private Resources resources;

    public PreferencesRepositoryImpl(Context context) {
        preferences = context.getSharedPreferences("prefs_repo", Context.MODE_PRIVATE);
        this.resources = context.getResources();
    }

    @Override
    public String getLastDisplayFilter() {
        return preferences.getString(APP_LAST_DISPLAY_FILTER, null);
    }

    @Override
    public void setLastDisplayFilter(String displayFilter) {
        preferences.edit().putString(APP_LAST_DISPLAY_FILTER, displayFilter).commit();
    }

    @Override
    public boolean hasData() {
        return preferences.getBoolean(APP_HAS_DATA, false);
    }

    @Override
    public void validateData(boolean hasData) {
        preferences.edit().putBoolean(APP_HAS_DATA, hasData).commit();
    }

    @Override
    public int getCurrentLanguage() {
        int currLangIndex = preferences.getInt(APP_CURRENT_LANGUAGE, -1);
        return (currLangIndex == -1) ? 0 : currLangIndex;
    }

    @Override
    public void setCurrentLanguage(int languageIndex) {
        preferences.edit().putInt(APP_CURRENT_LANGUAGE, languageIndex).commit();
    }

    @Override
    public void makeCurrentLanguageAvailable() {
        Configuration configuration = resources.getConfiguration();
        int currLangIndex = getCurrentLanguage();
        String[] langs = getAvailableLanguageCodes();
        if(!configuration.locale.getLanguage().equals(langs[currLangIndex])) {
            configuration.locale = new Locale(langs[currLangIndex]);
            resources.updateConfiguration(configuration, null);
        }
    }

    @Override
    public String[] getAvailableLanguages() {
        return resources.getStringArray(R.array.languages);
    }

    private String[] getAvailableLanguageCodes() {
        return resources.getStringArray(R.array.language_codes);
    }

    //TODO: maken an implementor for these repository methods
    @Override
    public Void findOne(Void primaryKey) {
        throw new UnsupportedOperationException("Method not implemented");
    }

    @Override
    public List<Void> findAll() {
        throw new UnsupportedOperationException("Method not implemented");
    }

    @Override
    public boolean createOne(Void entity) {
        throw new UnsupportedOperationException("Method not implemented");
    }

    @Override
    public boolean createMany(List<Void> entities) {
        throw new UnsupportedOperationException("Method not implemented");
    }

    @Override
    public boolean deleteAll() {
        preferences.edit().clear().apply();
        return true;
    }

    @Override
    public boolean update(Void entity){
        throw new UnsupportedOperationException("Method not implemented");
    }

}
