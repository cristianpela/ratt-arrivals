package criskey.org.rattarrivals.view.station;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import criskey.org.rattarrivals.R;
import criskey.org.rattarrivals.model.Location;
import criskey.org.rattarrivals.model.Station;
import criskey.org.rattarrivals.model.Transporter;
import criskey.org.rattarrivals.view.map.StationWalkDirectionsMapActivity;

/**
 * Created by criskey on 17/3/2016.
 */
public class StationNearRecyclerAdapter extends RecyclerView.Adapter<StationNearRecyclerAdapter.StationNearHolder> {

    private List<StationNearPresenter.DistanceStation> stations;

    private StationNearFragment.Callback callback;

    public StationNearRecyclerAdapter(StationNearFragment.Callback callback) {
        this(null, callback);
    }

    public StationNearRecyclerAdapter(List<StationNearPresenter.DistanceStation> stations, StationNearFragment.Callback callback) {
        this.stations = stations;
        this.callback = callback;
        if (stations == null)
            this.stations = new ArrayList<>();
    }

    public void updateStations(List<StationNearPresenter.DistanceStation> newStations) {
        stations.clear();
        stations.addAll(newStations);
        notifyDataSetChanged();
    }

    @Override
    public StationNearHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_display_near_station, parent, false);
        return new StationNearHolder(view);
    }

    @Override
    public void onBindViewHolder(StationNearHolder holder, int position) {
        holder.bind(stations.get(position));
    }

    @Override
    public int getItemCount() {
        return stations.size();
    }

    class StationNearHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private static final int STATION_CLICK_TAG = 0;
        private static final int OPEN_POP_UP_CLICK_TAG = 1;

        private TextView mTextStationName;
        private TextView mTextTransporters;
        private TextView mTextDistance;
        private ImageButton mImageButtonNearActions;
        private String stationName;

        private StationNearPresenter.DistanceStation distanceToStation;

        public StationNearHolder(View itemView) {
            super(itemView);
            mTextStationName = (TextView) itemView.findViewById(R.id.textNearStationName);
            registerStationClickListener(mTextStationName);
            mTextTransporters = (TextView) itemView.findViewById(R.id.textNearTransporters);
            registerStationClickListener(mTextTransporters);
            mTextDistance = (TextView) itemView.findViewById(R.id.textNearDistance);
            registerStationClickListener(mTextDistance);
            mImageButtonNearActions = (ImageButton) itemView.findViewById(R.id.imageButtonNearActions);
            mImageButtonNearActions.setTag(OPEN_POP_UP_CLICK_TAG);
            mImageButtonNearActions.setOnClickListener(this);
        }

        private void registerStationClickListener(View view) {
            view.setOnClickListener(this);
            view.setTag(STATION_CLICK_TAG);
        }

        private void bind(StationNearPresenter.DistanceStation dStation) {
            this.distanceToStation = dStation;
            Station station = dStation.station;
            stationName = station.getName();
            mTextStationName.setText(stationName);
            List<Transporter> transporters = station.getTransporters();
            if (!transporters.isEmpty()) {
                StringBuilder builder = new StringBuilder();
                for (Transporter transporter : transporters) {
                    String line = transporter.getLine().replace("Linia", "").trim();
                    builder.append(line + " ");
                }
                mTextTransporters.setText(builder.toString());
            }
            mTextDistance.setText(dStation.distance + "m");
        }

        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            switch (tag) {
                case STATION_CLICK_TAG: {
                    callback.onNearStationClicked(stationName);
                    break;
                }
                case OPEN_POP_UP_CLICK_TAG: {
                    Context ctx = v.getContext();

                    Location fromLocation = distanceToStation.from;
                    LatLng from = new LatLng(fromLocation.getLatitude(), fromLocation.getLongitude());

                    Location toStationLocation = distanceToStation.station.getLocation();
                    LatLng to = new LatLng(toStationLocation.getLatitude(), toStationLocation.getLongitude());

                    ctx.startActivity(StationWalkDirectionsMapActivity.createIntent(ctx, from, to));
                    break;
                }
            }

        }
    }
}
